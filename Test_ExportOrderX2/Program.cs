﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autofac;

namespace Test_ExportOrderX2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var container = new DependencyBuilder().GetDependencyContainer();
            
            //Application.Run(new OrderX2());
            Application.Run(container.Resolve<Booking856>());
        }
    }
}
