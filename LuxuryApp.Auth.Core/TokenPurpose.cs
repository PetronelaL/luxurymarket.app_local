﻿
namespace LuxuryApp.Auth.Core
{
    public enum TokenPurpose
    {
        None = 0,

        ResetPassword = 1, //GenerateUserTokenAsync

        Confirmation = 2 //GenerateEmailConfirmationTokenAsync
    }
}
