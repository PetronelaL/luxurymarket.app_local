﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Auth.Core.Entities
{
    public class Customer
    {
        public int CustomerId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Notes { get; set; }

        public IEnumerable<Company> Companies { get; set; }
    }

    public class Company
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public CompanyType CompanyType { get; set; }

        public bool HasAcceptedTermsAndConditions { get; set; }

        public int CurrencyID { get; set; }

        public bool IsIntegrated { get; set; }
    }

    public class CustomerDenormalized
    {
        public int CustomerId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public CompanyType CompanyType { get; set; }

        public bool HasAcceptedTermsAndConditions { get; set; }
        
        public int CurrencyID { get; set; }

        public int? APISettingsID { get; set; }
    }
    /// <summary>
    /// Enum with the Company Type values
    /// </summary>
    public enum CompanyType
    {
        None,

        Buyer = 1,

        Seller = 2,

        Both = 3
    }
}
