﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;

namespace Luxury.CurrencyUpdater.Implementations
{
    public class CurrencyUpdaterWorker: IWorker
    {
        private readonly IWorkerConfig _workerConfig;
        private readonly ICurrencyUpdateAgent _currencyUpdateAgent;

        private Task _workerTask = null;

        public CurrencyUpdaterWorker(ICurrencyUpdateAgent currencyUpdateAgent, IWorkerConfig workerConfig)
        {
            _currencyUpdateAgent = currencyUpdateAgent;
            _workerConfig = workerConfig;
        }

        public void Start(CancellationToken cancellationToken)
        {
            if (_workerTask != null && _workerTask.Status == TaskStatus.Running)
            {
                return;
            }
            _workerTask = Task.Run(() => Worker(cancellationToken));
        }

        private void Worker(CancellationToken cancellationToken)
        {
            for (;;)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                _currencyUpdateAgent.UpdateCurrencies();
                Task.Delay(_workerConfig.DelaySeconds * 1000, cancellationToken).Wait();
            }
        }
    }
}
