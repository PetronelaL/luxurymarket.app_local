﻿using NLog;

namespace LuxuryApp.Core.Infrastructure.Logging
{
    public class LuxuryLogger : ILogger
    {
        private readonly Logger _logger = LogManager.GetLogger("LuxuryLogger");

        public void Information(string message)
        {
            _logger.Info(message);
        }

        public void Information(string message, params object[] args)
        {
            _logger.Info(message, args);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(string message, params object[] args)
        {
            _logger.Error(message, args);
        }

        public void Error(string message, System.Exception ex)
        {
            _logger.Error(ex,message,null);
        }

        public void Warning(string message)
        {
            _logger.Warn(message);
        }

        public void Warning(string message, params object[] args)
        {
            _logger.Warn(message, args);
        }

        public void Trace(string message)
        {
            _logger.Trace(message);
        }

        public void Trace(string message, params object[] args)
        {
            _logger.Trace(message, args);
        }
    }
}
