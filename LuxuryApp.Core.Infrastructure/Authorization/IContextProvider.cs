﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.Infrastructure.Authorization
{
    public interface IContextProvider
    {
        int GetLoggedInUserId();
    }
}
