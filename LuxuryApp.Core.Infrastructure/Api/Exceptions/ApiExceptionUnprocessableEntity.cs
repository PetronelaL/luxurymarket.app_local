using LuxuryApp.Core.Infrastructure.Api.Models;
using System.Collections.Generic;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    public class ApiExceptionUnprocessableEntity : ApiException
    {
        public IEnumerable<string> ValidationErrorMessages { get; protected set; }
        public ApiExceptionUnprocessableEntity(IEnumerable<string> validationErrorMessages)
        {
            Code = ApiResultCode.UnprocessableEntity;
            ValidationErrorMessages = validationErrorMessages;
        }
    }
}