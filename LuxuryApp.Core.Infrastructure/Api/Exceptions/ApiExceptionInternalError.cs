using LuxuryApp.Core.Infrastructure.Api.Models;
using System;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    public class ApiExceptionInternalError : ApiException
    {
        public ApiExceptionInternalError(Exception e) :
            base(e)
        {
            Code = ApiResultCode.InternalError;
        }
    }
}