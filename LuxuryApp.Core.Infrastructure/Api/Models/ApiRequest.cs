﻿namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ApiRequest // it's a class because i wanted the default constructor to be called
    {
        public int CurrentPage { get; set; }
        public int PageRows { get; set; }

        public ApiRequest(int currentPage = 1, int pageRows = 20)
        {
            CurrentPage = currentPage;
            PageRows = pageRows;
        }
    }

    public struct ApiRequest<T>  // it's a struct, to avoid heap overhead
    {        
        public T Data { get; set; }
        public int CurrentPage { get; set; }
        public int PageRows { get; set; }

        public ApiRequest(T data, int currentPage = 1, int pageRows = 20)
        {
            Data = data;
            CurrentPage = currentPage;
            PageRows = pageRows;
        }
    }
}
