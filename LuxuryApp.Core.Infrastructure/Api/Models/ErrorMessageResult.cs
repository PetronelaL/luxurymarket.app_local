﻿using System.Configuration;
using System.IdentityModel;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using System.Data.SqlClient;
using LuxuryApp.Utils.Helpers;

namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ErrorMessageResult : IHttpActionResult
    {
        public HttpRequestMessage Request { get; set; }
        public string Content { get; set; }
        public ExceptionHandlerContext Context { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var statusCode = ApiResultCode.InternalError;
            var errorCode = string.Empty;

            var apiException = Context.Exception as ApiException;
            if (apiException != null)
            {
                if (apiException.Code != 0)
                    statusCode = apiException.Code;

            }
            else if (Context.Exception is SqlException)
            {
                statusCode = ApiResultCode.BadRequest;
            }
            else if (Context.Exception is BadRequestException)
            {
                statusCode = ApiResultCode.BadRequest;
            }

            var stackTrace = "";
            var environment = ConfigurationManager.AppSettings["Environment"];
            if (!string.IsNullOrEmpty(environment) && !environment.Equals("Live", System.StringComparison.InvariantCultureIgnoreCase))
            {
                stackTrace = Context.Exception.StackTrace;
            }

            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("LuxuryApp.Resources.Errors.ErrorMessages", this.GetType().Assembly);

            var response = new HttpResponseMessage
            {
                Content = new ObjectContent<ErrorResponse>(
                     new ErrorResponse
                     {
                         ErrorCode = statusCode,
                         Message = string.IsNullOrEmpty(Content) ? "Internal server error" : rm.GetResourceValue(Content),
                         MoreInfo = stackTrace
                     },
                     new JsonMediaTypeFormatter(), "application/json"),
                RequestMessage = Request,
                StatusCode = (HttpStatusCode)statusCode
            };
            return Task.FromResult(response);
        }

    }
}
