using LuxuryApp.Core.Infrastructure.Api.Exceptions;

namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ApiResultForbidden<T> : ApiResult<T>
    {
        public ApiResultForbidden(T data, string message = "Access denied"):
            base(data, new []{message}, new ApiExceptionForbidden())
        {
            
        }
    }
}