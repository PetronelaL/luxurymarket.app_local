﻿using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ApiResult
    {
        public ApiResultCode Code { get; protected set; }

        public IEnumerable<string> BusinessRulesFailedMessages { get; set; }
        public ApiException Exception { get; set; }
        public bool IsSuccesfull { get; set; }

        public ApiResult(
            IEnumerable<string> businessRulesFailedMessages = null,
            ApiException exception = null)
        {

            Code = ApiResultCode.Successfull;

            BusinessRulesFailedMessages = businessRulesFailedMessages ?? new List<string>();
            if (BusinessRulesFailedMessages.Any())
            {
                Code = ApiResultCode.UnprocessableEntity;
            }

            if (exception != null)
            {
                Exception = exception;
                Code = exception.Code;
            }

            IsSuccesfull = Code == ApiResultCode.Successfull;
        }
    }

    public class ApiResult<TResult> : ApiResult
    {
        private TResult _data;

        public TResult Data
        {
            get
            {
                //if (Exception != null)
                //{
                //    throw Exception;
                //}
                return _data;
            }
            set { _data = value; }
        }

        public int TotalRecords { get; set; }


        public ApiResult(TResult data)
        {
#if DEBUG
            if (typeof(TResult) != typeof(string) && typeof(TResult).GetInterfaces().Contains(typeof(IEnumerable)))
            {
                throw new ArgumentException("You should call constructor ApiResult(TResult data, int totalRecords)");
            }
#endif
            Code = ApiResultCode.Successfull;
            IsSuccesfull = true;
            _data = data;
            TotalRecords = 1;
        }

        public ApiResult(TResult data, int totalRecords)
        {
            Code = ApiResultCode.Successfull;
            IsSuccesfull = true;
            _data = data;
            TotalRecords = totalRecords;
        }

        public ApiResult(TResult data,
            IEnumerable<string> businessRulesFailedMessages,
            ApiException exception = null)
        {
            if (data == null && businessRulesFailedMessages == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            _data = data;
            Code = ApiResultCode.Successfull;

            BusinessRulesFailedMessages = businessRulesFailedMessages ?? new List<string>();
            if (BusinessRulesFailedMessages.Any())
            {
                Code = ApiResultCode.UnprocessableEntity;
            }

            if (exception != null)
            {
                Exception = exception;
                Code = exception.Code;
            }

            IsSuccesfull = Code == ApiResultCode.Successfull;
        }
    }

    public enum ApiResultCode
    {
        Successfull = 200,

        NotFound = 404,

        Forbidden = 403,

        BadRequest = 400,

        UnprocessableEntity = 422,

        InternalError = 500,

        UnsupportedMediaType = 415,

        ExpectationFailed = 417
    }

    public class ApiResultUnprocessableEntity : ApiResult
    {
        public IEnumerable<string> ValidationErrorMessages { get; set; }

        public ApiResultUnprocessableEntity(ApiExceptionUnprocessableEntity ex) :
            base(ex.ValidationErrorMessages, ex)
        {
            ValidationErrorMessages = ex.ValidationErrorMessages;
        }

        public ApiResultUnprocessableEntity(string validationErrorMessage) :
            base(new[] { validationErrorMessage }, new ApiExceptionUnprocessableEntity(new[] { validationErrorMessage }))
        {
            ValidationErrorMessages = new[] { validationErrorMessage };
        }

        public ApiResultUnprocessableEntity(string[] validationErrorMessages)
            : base(validationErrorMessages, new ApiExceptionUnprocessableEntity(validationErrorMessages))
        {
            ValidationErrorMessages = validationErrorMessages;
        }
    }

    public class ApiResultForbidden : ApiResult
    {
        public ApiResultForbidden() : base(null, new ApiExceptionForbidden())
        {
        }

        public ApiResultForbidden(string message) :
            base(new List<string>() { message }, new ApiExceptionForbidden())
        {

        }
    }

    public class ApiResultNotFound : ApiResult
    {
        public ApiResultNotFound() : base(null, new ApiExceptionNotFound())
        {

        }
    }

    public class ApiResultInternalError : ApiResult
    {
        public string ErrorId { get; set; }

        public ApiResultInternalError(string errorId)
        {
            Code = ApiResultCode.InternalError;
            ErrorId = errorId;
            var message = $"Internal error. Contact support and mentiond error id:{errorId}";
            BusinessRulesFailedMessages = new[] { message };
            Exception = new ApiExceptionInternalError(new Exception(message));
        }
    }
}
