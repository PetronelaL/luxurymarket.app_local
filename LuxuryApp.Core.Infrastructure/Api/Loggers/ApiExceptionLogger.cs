﻿using System.Web.Http.ExceptionHandling;
using NLog;

namespace LuxuryApp.Core.Infrastructure.Api.Loggers
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            LogManager.GetCurrentClassLogger().Error(context.Exception);
        }
    }
}