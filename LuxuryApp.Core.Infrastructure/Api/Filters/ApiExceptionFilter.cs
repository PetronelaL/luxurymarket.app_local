﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Api.Models;
using NLog;

namespace LuxuryApp.Core.Infrastructure.Api.Filters
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext cont)
        {
            ErrorResponse errorResponse;
            var code = HttpStatusCode.InternalServerError;
            //  TODO: add handlers for each exception type
            if (cont.Exception is ApiException)
            {
                var apiException = cont.Exception as ApiException;
                code = GetHttpStatusCode(apiException.Code);
                cont.Response = new HttpResponseMessage(code);
                errorResponse = apiException.ToErrorResponse();
            }
            else if (cont.Exception is HttpResponseException)
            {
                var apiException = new ApiException(cont.Exception.Message)
                {
                };
                cont.Response = new HttpResponseMessage(HttpStatusCode.NotFound);
                errorResponse = apiException.ToErrorResponse();
            }
            else
            {
                cont.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                errorResponse = cont.Exception.ToErrorResponse();
            }

            errorResponse.MoreInfo = cont.Exception.StackTrace;

            LogManager.GetCurrentClassLogger().Error(cont.Exception);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(errorResponse);
            cont.Response.Content = new StringContent(json);
            cont.Response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        }

        private HttpStatusCode GetHttpStatusCode(ApiResultCode code)
        {
            switch (code)
            {
                case ApiResultCode.BadRequest:
                    return HttpStatusCode.BadRequest;
                case ApiResultCode.Forbidden:
                    return HttpStatusCode.Forbidden;
                case ApiResultCode.InternalError:
                    return HttpStatusCode.InternalServerError;
                case ApiResultCode.NotFound:
                    return HttpStatusCode.NotFound;
                case ApiResultCode.UnprocessableEntity:
                    return HttpStatusCode.BadRequest;
                case ApiResultCode.UnsupportedMediaType:
                    return HttpStatusCode.BadRequest;
            }
            return HttpStatusCode.InternalServerError;
        }
    }
}
