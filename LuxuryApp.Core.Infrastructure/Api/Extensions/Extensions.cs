﻿using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.Infrastructure.Api.Extensions
{
    public static class Extensions
    {
        public static ApiResult<T> ToApiResult<T>(this T data)
        {
            return new ApiResult<T>(data);
        }

        public static ApiResult<T> ToApiResult<T>(this T data, int totalRecords)
        {
            return new ApiResult<T>(data, totalRecords);
        }
    }
}

