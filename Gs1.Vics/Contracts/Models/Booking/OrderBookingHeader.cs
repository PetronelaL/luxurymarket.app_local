﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gs1.Vics.Contracts.Models.Common;

namespace Gs1.Vics.Contracts.Models.Booking
{
    public class OrderBookingHeader :
        IInterchangeHeader,
        IGsFunctionalGroupHeader
        //IBsnBeginningSegmentShipNotice
    {
        public string InterchangeSenderId { get; set; }
        public string InterchangeReceiverId { get; set; }
        public DateTimeOffset InterchangeDateTime { get; set; }
        public string InterchangeControlVersionNumber { get; set; }
        public int InterchangeControlNumber { get; set; }
        public TestIndicator TestIndicator { get; set; }

        public string DocumentId => "PO";  // not used
        public string ApplicationSenderCode { get; set; }
        public string ApplicationReceiverCode { get; set; }
        public DateTimeOffset GroupDateTime { get; set; }
        public int GroupControlNumber => InterchangeControlNumber;
        public int TransactionSetControlNumber { get; set; }
        public string ResponsibleAgencyCode => "X";   //Accredited Standard Committee X12

        public string TransactionType => "856";   //X12.10 Ship Notice/Manifest
      
        //IBsnBeginningSegmentShipNotice
        public TransactionSetPurposeCode TransactionSetPurposeCode { get; }
        public DateTimeOffset ProcessDate { get; set; }
        public string TransactionTypeCode => "BK"; //Booking 
        public int HierarchicalStructureCode { get; set; } 
        public string Version => "00401";

        public string PoNumber { get; set; }

        public OrderBookingHeader()
        {
        }
    }
}