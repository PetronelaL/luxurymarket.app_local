﻿namespace Gs1.Vics.Contracts.Models.Order
{
    public class OrderDetail
    {
        public int LineItemNumber { get; set; }
        public int QuantityOrdered { get; set; }
        public decimal UnitCost { get; set; }
        public string EanNumber { get; set; }
        public string VendorStyleNumber { get; set; }
        public string SkuNumber { get; set; }
        public decimal UnitPrice { get; set; }
        public string ProductDescription { get; set; }
        public string ColorDescription { get; set; }
        public string SizeDescription { get; set; }
        public string HangOrFlat { get; set; }
       
        public string ManufacturerName { get; set; }
        public string ManufacturerIdentificationCode { get; set; }
        public string ManufacturerAddressInformation { get; set; }
        public string ManufacturerCityName { get; set; }
        public string ManufacturerStateOrProvinceCode { get; set; }
        public string ManufacturerPostalCode { get; set; }
        public string ManufacturerCountryCode { get; set; }
        public int Quantity { get; set; }
        public string UnitOfMeasure { get; set; }
        
    }
}