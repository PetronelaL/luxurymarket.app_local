﻿namespace Gs1.Vics.Contracts.Models.Order
{
    public enum TermsOfSale
    {
        BasicTerm,
        DiscountNotApplicable,
        BasicDiscoutOffered,
        TenDaysEom,
    }
}