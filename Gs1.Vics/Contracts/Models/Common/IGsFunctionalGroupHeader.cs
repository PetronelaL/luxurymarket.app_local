﻿using System;

namespace Gs1.Vics.Contracts.Models.Common
{
    public interface IGsFunctionalGroupHeader
    {
        string DocumentId { get; }   //?Functional Identifier Code
        string ApplicationSenderCode { get; }
        string ApplicationReceiverCode { get; }
        DateTimeOffset GroupDateTime { get; }
        int GroupControlNumber { get; }
       // string ResponsibleAgencyCode { get; }
        string Version { get; }
    }
}