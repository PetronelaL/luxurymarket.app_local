﻿using System;

namespace Gs1.Vics.Contracts.Models.Common
{
    public interface IBsnBeginningSegmentShipNotice
    {
        TransactionSetPurposeCode TransactionSetPurposeCode { get; }
        DateTimeOffset ProcessDate { get; }   //TODO: what is Process Date?
        string HierarchicalStructureCode { get; }
    }
}
