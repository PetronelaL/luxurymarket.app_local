﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Gs1.Vics.Contracts.Models.Order;
using Gs1.Vics.Contracts.Services;
using LuxuryApp.Contracts;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using Gs1.Vics.Contracts.Models.Common;
using Gs1.Vics.Contracts.Models;

namespace Gs1.Vics.Implementations.Services
{
    public class X2EdiOrderSerializeService : IEdiSerializeService<Order>
    {
        public ApiResult<byte[]> Serialize(Order document)
        {
            byte[] result;
            var segmentedDocument = new SegmentedDocument();
            SerializeHeader(segmentedDocument, document.OrderHeader);
            foreach (var detail in document.OrderDetails)
            {
                SerializeDetail(segmentedDocument, detail);
            }
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream))
                {
                    segmentedDocument.Serialize(writer);
                    writer.Flush();
                }
                result = memoryStream.ToArray();
            }
            return result.ToApiResult(result.Length);
        }

        private void SerializeHeader(SegmentedDocument segmentedDocument, OrderHeader orderHeader)
        {
            segmentedDocument.BeginSegment("ISA", 16)
                .Add(1, "00")
                .Add(3, "00")
                .Add(5, "01")
                .Add(6, orderHeader.InterchangeSenderId)
                .Add(7, "12")
                .Add(8, orderHeader.InterchangeReceiverId)
                .Add(9, orderHeader.InterchangeDateTime.ToString("yyMMdd"))
                .Add(10, orderHeader.InterchangeDateTime.ToString("HHmm"))
                .Add(11, "U")
                .Add(12, orderHeader.InterchangeControlVersionNumber)
                .Add(13, orderHeader.InterchangeControlNumber.ToString("D9"))
                .Add(14, "0")
                .Add(15, orderHeader.TestIndicator.Text())
                .ElementSeparator(16)
                .EndSegment();

            segmentedDocument.BeginSegment("GS", 8)
                .Add(1, "PO")
                .Add(2, orderHeader.ApplicationSenderCode)
                .Add(3, orderHeader.ApplicationReceiverCode)
                .Add(4, orderHeader.GroupDateTime.ToString("yyyyMMdd"))
                .Add(5, orderHeader.GroupDateTime.ToString("HHmm"))
                .Add(6, orderHeader.GroupControlNumber.ToString())
                .Add(7, "X")
                .Add(8, orderHeader.Version)
                .EndSegment();

            segmentedDocument.BeginSegment("ST", 2)
                .Add(1, "850")
                .Add(2, orderHeader.TransactionSetControlNumber)
                .EndSegment();

            segmentedDocument.BeginSegment("BEG", 5)
                .Add(1, "00")
                .Add(2, "SA")
                .Add(3, orderHeader.PoNumber.Replace("#","")) // remove # by Alpi insistance
                .Add(5, orderHeader.PurchaseOrderDate.ToString("yyyyMMdd"))
                .EndSegment();

            segmentedDocument.BeginSegment("REF", 2)
                .Add(1, "VR")
                .Add(2, orderHeader.VendorOrSupplierSiteNumber)
                .EndSegment();

            segmentedDocument.BeginSegment("REF", 2)
                .Add(1, "DP")
                .Add(2, orderHeader.DepartmentNumber)
                .EndSegment();

            segmentedDocument.BeginSegment("REF", 2)
                .Add(1, "IA")
                .Add(2, orderHeader.ManufacturerNumber)
                .EndSegment();

            segmentedDocument.BeginSegment("FOB", 3)
                .Add(3, "EXW")
                .EndSegment();

            segmentedDocument.BeginSegment("ITD", 13)
                .Add(1, orderHeader.TermsOfSale.Text())
                .Add(2, "2")
                .Add(7, orderHeader.TermsNetDays.ToString())
                .EndSegment();

            segmentedDocument.BeginSegment("DTM", 2)
                .Add(1, "010")
                .Add(2, orderHeader.RequestedShipDate.ToString("yyyyMMdd"))
                .EndSegment();

            segmentedDocument.BeginSegment("DTM", 2)
                .Add(1, "001")
                .Add(2, orderHeader.CancelAfterDate.ToString("yyyyMMdd"))
                .EndSegment();

            segmentedDocument.BeginSegment("TD5", 4)
                .Add(4, orderHeader.ShipmentMethod.ToString())
                .EndSegment();

            segmentedDocument.BeginSegment("N1", 4)
                .Add(1, "SU")
                .Add(2, orderHeader.SupplierName)
                .Add(3, "92")
                .Add(4, orderHeader.SupplierIdentificationCode)
                .EndSegment();

            segmentedDocument.BeginSegment("N3", 1)
                .Add(1, orderHeader.SupplierAddressInformation)
                .EndSegment();

            segmentedDocument.BeginSegment("N4", 4)
                .Add(1, orderHeader.SupplierCityName)
                .Add(2, orderHeader.SupplierStateOrProvinceCode)
                .Add(3, orderHeader.SupplierPostalCode)
                .Add(4, orderHeader.SupplierCountry)
                .EndSegment();

            segmentedDocument.BeginSegment("N1", 4)
                .Add(1, "ST")
                .Add(2, orderHeader.ShipToDestinationName)
                .Add(3, "92")
                .Add(4, orderHeader.ShipToIdentificationCode)
                .EndSegment();

            segmentedDocument.BeginSegment("N3", 1)
                .Add(1, orderHeader.ShipToAddressInformation)
                .EndSegment();

            segmentedDocument.BeginSegment("N4", 4)
                .Add(1, orderHeader.ShipToCityName)
                .Add(2, orderHeader.ShipToStateOrProvinceCode)
                .Add(3, orderHeader.ShipToPostalCode)
                .Add(4, orderHeader.ShipToCountryCode)
                .EndSegment();
        }

        private void SerializeDetail(SegmentedDocument segmentedDocument, OrderDetail detail)
        {
            segmentedDocument.BeginSegment("PO1", 11)
                .Add(1, detail.LineItemNumber.ToString())
                .Add(2, detail.QuantityOrdered.ToString())
                .Add(3, detail.UnitOfMeasure)
                .Add(4, detail.UnitCost.ToString(CultureInfo.InvariantCulture))
                .Add(5, "QT")
                .Add(6, "EN")
                .Add(7, detail.EanNumber)
                .Add(8, "VA")
                .Add(9, detail.VendorStyleNumber)
                .Add(10, "SK")
                .Add(11, detail.SkuNumber)
                .EndSegment();

            segmentedDocument.BeginSegment("CTP", 5)
                .Add(1, "RS")
                .Add(2, "RES")
                .Add(3, detail.UnitPrice.ToString(CultureInfo.InvariantCulture))
                .Add(4, detail.Quantity.ToString())
                .Add(5, detail.UnitOfMeasure)
                .EndSegment();

            segmentedDocument.BeginSegment("PID", 5)
                .Add(1, "F")
                .Add(2, "08")
                .Add(3, "UN")
                .Add(4, "")
                .Add(5, detail.ProductDescription)
                .EndSegment();

            segmentedDocument.BeginSegment("PID", 5)
                .Add(1, "F")
                .Add(2, "75")
                .Add(5, detail.ColorDescription)
                .EndSegment();

            segmentedDocument.BeginSegment("PID", 5)
                .Add(1, "F")
                .Add(2, "91")
                .Add(5, detail.SizeDescription)
                .EndSegment();

            segmentedDocument.BeginSegment("SAC", 16)
                .Add(1, "N")
                .Add(3, "VI")
                .Add(4, "HA")
                .Add(15, detail.HangOrFlat)
                .EndSegment();

            segmentedDocument.BeginSegment("N1", 4)
                .Add(1, "SU")
                .Add(2, detail.ManufacturerName)
                .Add(3, "92")
                .Add(4, detail.ManufacturerIdentificationCode)
                .EndSegment();

            segmentedDocument.BeginSegment("N3", 1)
                .Add(1, detail.ManufacturerAddressInformation)
                .EndSegment();

            segmentedDocument.BeginSegment("N4", 4)
                .Add(1, detail.ManufacturerCityName)
                .Add(2, detail.ManufacturerStateOrProvinceCode)
                .Add(3, detail.ManufacturerPostalCode)
                .Add(4, detail.ManufacturerCountryCode)
                .EndSegment();
        }

       }

}



