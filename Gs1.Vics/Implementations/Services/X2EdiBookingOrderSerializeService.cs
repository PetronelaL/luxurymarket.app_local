﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Gs1.Vics.Contracts.Models.Order;
using Gs1.Vics.Contracts.Services;
using LuxuryApp.Contracts;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using Gs1.Vics.Contracts.Models.Common;
using Gs1.Vics.Contracts.Models.Booking;
namespace Gs1.Vics.Implementations.Services
{
    public class X2EdiBookingOrderSerializeService : IEdiSerializeService<OrderBooking>
    {
        public ApiResult<byte[]> Serialize(OrderBooking document)
        {
            byte[] result;
            var segmentedDocument = new Segmented856Document();
            SerializeHeader(segmentedDocument, document.OrderHeader);
            foreach (var detail in document.ShipmentDetails)
            {
                SerializeShipmentDetail(segmentedDocument, detail);

                foreach (var orderDetail in detail.OrderDetails)
                {
                    SerializeOrderDetail(segmentedDocument, orderDetail);
                    foreach (var itemDetail in orderDetail.ItemDetails)
                    {
                        SerializeItemDetail(segmentedDocument, itemDetail);
                    }
                }
            }

            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream))
                {
                    segmentedDocument.Serialize(writer);
                    writer.Flush();
                }
                result = memoryStream.ToArray();
            }
            return result.ToApiResult(result.Length);
        }

        private void SerializeHeader(SegmentedDocument segmentedDocument, OrderBookingHeader header)
        {
            segmentedDocument.BeginSegment("ISA", 16)
                .Add(1, "00")
                .Add(3, "00")
                .Add(5, "01")
                .Add(6, header.InterchangeSenderId)
                .Add(7, "12")
                .Add(8, header.InterchangeReceiverId)
                .Add(9, header.InterchangeDateTime.ToString("yyMMdd"))
                .Add(10, header.InterchangeDateTime.ToString("HHmm"))
                .Add(11, "U")
                .Add(12, header.InterchangeControlVersionNumber)
                .Add(13, header.InterchangeControlNumber.ToString("D9"))
                .Add(14, "0")   //AcknowledgementRequested (0/1) 
                .Add(15, header.TestIndicator.Text())
                .ElementSeparator(16)
                .EndSegment();

            segmentedDocument.BeginSegment("GS", 8)
                .Add(1, "PO")
                .Add(2, header.ApplicationSenderCode)
                .Add(3, header.ApplicationReceiverCode)
                .Add(4, header.GroupDateTime.ToString("yyyyMMdd"))
                .Add(5, header.GroupDateTime.ToString("HHmm"))
                .Add(6, header.GroupControlNumber.ToString())
                .Add(7, "X")
                .Add(8, header.Version)
                .EndSegment();

            segmentedDocument.BeginSegment("ST", 2)
                .Add(1, "856")
                .Add(2, header.TransactionSetControlNumber.ToString())
                .EndSegment();

            segmentedDocument.BeginSegment("BSN", 6)
             .Add(1, header.TransactionSetPurposeCode.Text())
             .Add(2, header.PoNumber)
             .Add(3, header.ProcessDate.ToString("yyyyMMdd"))
             .Add(4, header.ProcessDate.ToString("HHmm"))
             .Add(5, header.HierarchicalStructureCode.ToString())
             .EndSegment();

        }

        private void SerializeShipmentDetail(SegmentedDocument segmentedDocument, BookingShipmentLevel detail)
        {
            segmentedDocument.BeginSegment("HL", 3)
               .Add(1, detail.HierarchicalIdNumber.ToString())
               .Add(3, "S")
               .EndSegment();

            segmentedDocument.BeginSegment("TD1", 2)
             .Add(1, "CTN")
             .Add(2, detail.TotalCartons.ToString())
             .EndSegment();

            segmentedDocument.BeginSegment("TD1", 2)
            .Add(1, "UNT")
            .Add(2, detail.TotalUnits.ToString())
            .EndSegment();

            segmentedDocument.BeginSegment("TD5", 15)
            .Add(2, "2") //Standard Carrier Alpha Code
            .Add(3, "SCAC")
            .Add(4, detail.TransportationMethodCode)
            .Add(7, detail.LocationQualifier)
            .Add(8, detail.LocationIdentifier)
            .Add(9, detail.CountryIso2Code)
            .EndSegment();

            segmentedDocument.BeginSegment("REF", 2)
               .Add(1, "ZZ")
               .Add(2, detail.SellerCompanyName) //Shipping Vendor Name
               .EndSegment();

            segmentedDocument.BeginSegment("N1", 2)
            .Add(1, "ST")
            .Add(2, detail.BuyerCompanyName) //Buying Entity Name
            .EndSegment();

            segmentedDocument.BeginSegment("N1", 2)
             .Add(1, "SF")
             .Add(2, detail.SellerName)
                      .EndSegment();

            segmentedDocument.BeginSegment("N1", 2)
          .Add(1, "EX")
          .Add(2, detail.SellerCompanyName)
          .EndSegment();

            segmentedDocument.BeginSegment("N3", 2)
           .Add(1, detail.SellerAddress1)
           .Add(2, detail.SellerAddress2)
           .EndSegment();

            segmentedDocument.BeginSegment("N4", 6)
            .Add(1, detail.SellerCity)
            .Add(2, detail.SellerStateOrProvinceCode)
            .Add(3, detail.SellerPostalCode)
            .Add(4, detail.SellerCountryCode)
            .Add(6, detail.SellerRegion)
            .EndSegment();

            segmentedDocument.BeginSegment("V1", 2)
           .Add(2, "VENDOR")  //TODO?
           .Add(3, detail.PONumber)
           .EndSegment();

            segmentedDocument.BeginSegment("DTM", 3)
            .Add(1, "135")
            .Add(2, detail.BookedSupplierDate.ToString("yyyyMMdd"))
            .Add(3, detail.BookedSupplierDate.ToString("HHmm"))
            .EndSegment();

            segmentedDocument.BeginSegment("DTM", 2)
            .Add(1, "072")
            .Add(2, detail.DateSellerReady.ToString("yyyyMMdd"))
            .EndSegment();
        }

        private void SerializeOrderDetail(SegmentedDocument segmentedDocument, BookingOrderLevel detail)
        {
            segmentedDocument.BeginSegment("HL", 3)
               .Add(1, detail.HierarchicalIdNumber.ToString())
               .Add(2, detail.HierarchicalParentIdNumber.ToString())
               .Add(3, "O")
               .EndSegment();

            segmentedDocument.BeginSegment("PRF", 1)
             .Add(1, detail.PONumber)
             .EndSegment();

            segmentedDocument.BeginSegment("REF", 2)
            .Add(1, "19")
            .Add(2, "24")
            .EndSegment();

            segmentedDocument.BeginSegment("REF", 2)
            .Add(1, "BN")
            .Add(2, detail.VendorBookingNumber)
            .EndSegment();

        }

        private void SerializeItemDetail(SegmentedDocument segmentedDocument, BookingItemLevel detail)
        {
            segmentedDocument.BeginSegment("HL", 3)
               .Add(1, detail.LineItemNumber.ToString())
               .Add(2, detail.LineParentItemNumber.ToString())
               .Add(3, "I")
               .EndSegment();

            segmentedDocument.BeginSegment("LIN", 13)
             .Add(2, "VA")
             .Add(3, detail.VendorStyleNumber.ToString())
             .Add(4, "PS")
             .Add(5, detail.PageNumber.ToString())
             .Add(6, "A7")
             .Add(7, detail.LineItemNumber.ToString())
             .Add(8, "VE")
             .Add(9, detail.ColorDescription.ToString())
             .Add(10, "IZ")
             .Add(11, detail.SizeDescription.ToString())
              .Add(12, "IT")
               .Add(13, "Buyer sku")
             .EndSegment();

            segmentedDocument.BeginSegment("SN1", 3)
             .Add(2, detail.Quantity.ToString())
             .Add(3, detail.UnitOfMeasure.ToString())
             .EndSegment();
        }
    }

    public class Segmented856Document : SegmentedDocument
    {
        public override Segment[] GetSummarySegments(MessageSummary messageSummary)
        {
            return new[]
            {
                new Segment("CTT", 1)
                    .Add(1, messageSummary.CttNumberOfPo1SegmentsIncluded.ToString())
                    .EndSegment(),
                new Segment("SE", 2)
                    .Add(1, messageSummary.SeNumberOfSegmentsIncluded.ToString())
                    .Add(2, messageSummary.SeTransactionSetControlNumber)
                    .EndSegment(),
                new Segment("GE", 2)
                    .Add(1, messageSummary.GeNumberOfTransactionsIncluded.ToString())
                    .Add(2, messageSummary.GeGroupControlNumber)
                    .EndSegment(),
                new Segment("IEA", 2)
                    .Add(1, messageSummary.IeaNumberOfIncludedFunctionalGroups.ToString())
                    .Add(2, messageSummary.IeaInterchangeControlNumber)
                    .EndSegment(),
            };
        }

        public override MessageSummary GetMessageSummary()
        {
            var messageSummary = new MessageSummary();
            messageSummary.CttNumberOfPo1SegmentsIncluded = Segments.Count(x => x.Name == "LIN");
            messageSummary.SeNumberOfSegmentsIncluded = Segments.Count;
            messageSummary.SeTransactionSetControlNumber =
                Segments
                .Where(x => x.Name == "ST")
                .SelectMany(y => y.SegmentDatas)
                .SingleOrDefault(z => z.Position == 2)?.Value ?? "";
            messageSummary.GeNumberOfTransactionsIncluded =
                Segments.Count(x => x.Name == "GS");
            messageSummary.GeGroupControlNumber =
                Segments.Where(x => x.Name == "GS")
                    .SelectMany(y => y.SegmentDatas)
                    .SingleOrDefault(z => z.Position == 6)?.Value ?? "";
            messageSummary.IeaNumberOfIncludedFunctionalGroups =
                Segments.Count(x => x.Name == "ISA");
            messageSummary.IeaInterchangeControlNumber =
                Segments.Where(x => x.Name == "ISA")
                    .SelectMany(y => y.SegmentDatas)
                    .SingleOrDefault(z => z.Position == 13)
                    ?.Value ?? "";
            return messageSummary;
        }
    }
}



