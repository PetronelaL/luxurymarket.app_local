﻿using Autofac;
using Luxury.Ftp;
using LuxuryApp.Contracts.BackgroudServices;
using System;
using System.Threading;

namespace Debug.Luxury.FTPEdi
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new DependencyBuilder().GetDependencyContainer();
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

          var worker = container.Resolve<IWorker>();
            worker.Start(cancellationTokenSource.Token);

            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }
    }
}
