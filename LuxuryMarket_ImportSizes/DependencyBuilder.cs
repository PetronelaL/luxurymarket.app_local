﻿using Autofac;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.DataAccess;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Core.Infrastructure.Logging;

//using Gs1.Vics.Contracts.Factories;
//using Gs1.Vics.Implementations.Factories;
//using Luxury.Ftp.Contracts;
//using Luxury.Ftp.Implementations;
//using LuxuryApp.Contracts.Access;
//using LuxuryApp.Contracts.Agents;
//using LuxuryApp.Contracts.Interfaces;
//using LuxuryApp.Contracts.Repository;
//using LuxuryApp.Contracts.Services;
//using LuxuryApp.Core.DataAccess;
//using LuxuryApp.Core.DataAccess.Repository;
//using LuxuryApp.Core.Infrastructure.Authorization;
//using LuxuryApp.Processors.Access;
//using LuxuryApp.Processors.Agents;
//using LuxuryApp.Processors.Services;

namespace Luxury.CurrencyUpdater
{
    public class DependencyBuilder
    {
        public IContainer GetDependencyContainer()
        {
            var builder = new ContainerBuilder();
            
            builder.RegisterType<LuxuryLogger>().As<ILogger>();
            builder.RegisterType<LuxuryMarketConnectionStringProvider>().As<IConnectionStringProvider>();
            builder.RegisterType<CurrencyRepository>().As<ICurrencyRepository>();
         
          //ss  builder.RegisterType<ServiceCurrency>().AsSelf();
            var container = builder.Build();
            return container;
        }
    }
}
