﻿using System.Text.RegularExpressions;

namespace LuxuryApp.Utils.Helpers
{
    public static class SpecialCharacterRemover
    {
        public static string RemoveSpecialCharacters(this string value)
        {
            return Regex.Replace(value, @"\s+", string.Empty);
        }

    }
}
