﻿using System;
using System.Globalization;
using System.Resources;
using System.Threading;

namespace LuxuryApp.Utils.Helpers
{
    public static class ResourceManagerHelper
    {
        public static string GetResourceValue(this ResourceManager resourceManager, string key, CultureInfo cultureInfo)
        {
            // var entry = resourceManager.GetResourceSet(cultureInfo, true, true).GetString(key, true);

            var result = key;
            try
            {
                result = resourceManager.GetString(key, cultureInfo);
            }
            catch (Exception ex)
            {
                result = key;
            }
            return !string.IsNullOrWhiteSpace(result) ? result : key;
        }

        public static string GetResourceValue(this ResourceManager resourceManager, string key)
        {
            return GetResourceValue(resourceManager, key, Thread.CurrentThread.CurrentCulture);
        }
    }
}
