﻿using LuxuryApp.Contracts.BackgroudServices;
using System.Configuration;

namespace Luxury.VendorImagesUploader.Implementations
{
    public class WorkerConfigAppSettings: IWorkerConfig
    {
        public int DelaySeconds
        {
            get
            {
                int value = 0;
                if (!int.TryParse(ConfigurationManager.AppSettings["WorkerPeriodSeconds"] ?? "", out value))
                {
                    value = 10;
                };
                return value;
            }
        }

    }
}
