﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Luxury.CurrencyUpdater;
using LuxuryApp.Contracts.BackgroudServices;

namespace Debug.Luxury.CurrencyUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new DependencyBuilder().GetDependencyContainer();
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            var worker = container.Resolve<IWorker>();
            worker.Start(cancellationTokenSource.Token);

            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }
    }
}
