﻿using LuxuryApp.Cdn.CdnServers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Cdn.Contracts;
using System.Data;
using LuxuryApp.Cdn.CdnServers.Repository;
using LuxuryApp.Cdn.CdnServers.Model;

namespace LuxuryApp.Cdn.SqlRepository
{
    public class MultiCdnRepositorySql : IMultiCdnRepository
    {
        private string _connectionString;

        public MultiCdnRepositorySql(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<FileData> SaveFileData(IEnumerable<FileData> fileDatas)
        {
            IEnumerable<FileData> result;
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                result = conn.Query<FileData>("cdn.SaveFilesData",
                    new { filedata = FileData2DataTable(fileDatas).AsTableValuedParameter("cdn.FileData") },
                    commandType: CommandType.StoredProcedure);
                conn.Close();
            }
            return result;
        }

        private DataTable FileData2DataTable(IEnumerable<FileData> fileDatas)
        {
            var dataTable = new DataTable();
            dataTable.Columns.AddRange(new[] {
                new DataColumn("relative_directory"),
                new DataColumn("file_name"),
                new DataColumn("file_size_bytes")
            });
            foreach (var fileData in fileDatas)
            {
                dataTable.Rows.Add(new object[] {
                    fileData.RelativePath,
                    fileData.FileName,
                    fileData.FileSizeBytes,
                });
            }
            return dataTable;
        }

        public IEnumerable<FileData> GetFileData(IEnumerable<string> keys)
        {
            IEnumerable<FileData> result;
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                result = conn.Query<FileData>("cdn.GetFilesData",
                    new { files_keys = Keys2DataTable(keys).AsTableValuedParameter("cdn.FileKey") },
                    commandType: CommandType.StoredProcedure);
                conn.Close();
            }
            return result;
        }

        private DataTable Keys2DataTable(IEnumerable<string> keys)
        {
            var dataTable = new DataTable();
            dataTable.Columns.AddRange(new[] {
                new DataColumn("relative_path"),
                new DataColumn("file_name"),
            });
            foreach (var key in keys)
            {
                dataTable.Rows.Add(new object[] {
                    FileData.GetRelativePath(key),
                    FileData.GetFileName(key),
                });
            }
            return dataTable;
        }

        public void SetCdnFileStatuses(IEnumerable<FileConfigStatus> cdnFileStatuses)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                conn.Execute("cdn.SetFileCdnStatuses",
                    new
                    {
                        file_status_update = FileConfigStatuses2DataTable(cdnFileStatuses)
                            .AsTableValuedParameter("cdn.FileConfigStatusUpdate")
                    },
                    commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }

        public IEnumerable<FileConfigStatus> GetFileCdnStatuses(IEnumerable<FileConfigStatusRequest> fileConfigRequests)
        {
            IEnumerable<FileConfigStatus> result;
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                result = conn.Query<FileConfigStatus>("cdn.GetFileCdnStatuses",
                    new
                    {
                        file_status_request = FileConfigStatusRequests2DataTable(fileConfigRequests)
                            .AsTableValuedParameter("cdn.FileConfigStatusRequest")
                    },
                    commandType: CommandType.StoredProcedure);
                conn.Close();
            }
            return result;
        }

        private DataTable FileConfigStatuses2DataTable(IEnumerable<FileConfigStatus> cdnFileStatuses)
        {
            var dataTable = new DataTable();
            dataTable.Columns.AddRange(new[] {
                new DataColumn("file_info_id", typeof(int)),
                new DataColumn("cdn_configuration_id", typeof(int)),
                new DataColumn("cdn_status", typeof(int)),
            });
            foreach (var cdnFileStatus in cdnFileStatuses)
            {
                dataTable.Rows.Add(new object[] {
                    cdnFileStatus.FileInfoId,
                    cdnFileStatus.CdnConfigurationId,
                    (int)cdnFileStatus.CdnStatus,
                });
            }
            return dataTable;
        }

        private DataTable FileConfigStatusRequests2DataTable(IEnumerable<FileConfigStatusRequest> fileConfigRequests)
        {
            var dataTable = new DataTable();
            dataTable.Columns.AddRange(new[]{
                new DataColumn("file_info_id", typeof(int)),
                new DataColumn("cdn_configuration_id", typeof(int)),
            });
            foreach (var config in fileConfigRequests)
            {
                dataTable.Rows.Add(new object[] {
                    config.FileInfoId,
                    config.CdnConfigurationId,
                });
            };
            return dataTable;
        }
    }
}
