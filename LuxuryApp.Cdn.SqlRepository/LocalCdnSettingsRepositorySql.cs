﻿using LuxuryApp.Cdn.CdnServers.Model;
using LuxuryApp.Cdn.CdnServers.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using LuxuryApp.Cdn.CdnServers;

namespace LuxuryApp.Cdn.SqlRepository
{
    public class LocalCdnSettingsRepositorySql : 
        CdnSettingsRepositoryBase<LocalCdnSettings>
    {
        public LocalCdnSettingsRepositorySql(string connectionString) : 
            base(connectionString, CdnConfigurationType.Local) 
        { 
        }

        public override LocalCdnSettings GetCdnSettings(int configurationId)
        {
            LocalCdnSettings result;
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                result = conn.Query<LocalCdnSettings>
                    (@"SELECT ConfigurationId = CdnConfigurationId
			        ,LocalRootPath
			        ,WebRootPath FROM cdn.GetConfigurationLocal(@cdn_configuration_id)", new { cdn_configuration_id = configurationId }).SingleOrDefault();
                conn.Close();
            }
            return result;
        }

        public override int SaveCdnSettings(LocalCdnSettings settings)
        {
            int result;
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                result = conn.ExecuteScalar<int>("cdn.SaveConfigurationLocal",
                        new
                        {
                            local_root_path = settings.LocalRootPath,
                            web_root_path = settings.WebRootPath,
                        }, commandType: CommandType.StoredProcedure);
                conn.Close();
            }
            settings.ConfigurationId = result;
            return result;
        }
    }
}
