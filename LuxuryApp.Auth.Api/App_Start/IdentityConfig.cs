﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Threading.Tasks;
using LuxuryApp.Auth.DataAccess;
using LuxuryApp.Auth.Api.Models;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using Microsoft.Practices.ServiceLocation;
using LuxuryApp.Auth.Api.Helpers;

namespace LuxuryApp.Auth.Api
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public class ApplicationUserManager : UserManager<ApplicationUser, int>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, int> store)
        : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var userStore = new UserStore<ApplicationUser, ApplicationRole>(context.Get<ApplicationDbContext>());
            var manager = new ApplicationUserManager(userStore);
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };
            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;
            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug in here.
            manager.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider<ApplicationUser, int>
            {
                MessageFormat = "Your security code is: {0}"
            });
            manager.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider<ApplicationUser, int>
            {
                Subject = "SecurityCode",
                BodyFormat = "Your security code is {0}",
            });

            manager.EmailService = new EmailService(ServiceLocator.Current.GetInstance<Func<INotificationHandler<EmailModel>>>());
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser, int>(dataProtectionProvider.Create("ASP.NET Identity"))
                    {
                        TokenLifespan = TimeSpan.FromDays(ConfigHelper.TokenLifeSpanDays)
                    };
            }
            return manager;
        }
    }

    public class EmailService : IIdentityMessageService
    {
        public Func<INotificationHandler<EmailModel>> _emailServiceFactory;

        public EmailService(Func<INotificationHandler<EmailModel>> emailService)
        {
            _emailServiceFactory = emailService;
        }
        public async Task SendAsync(IdentityMessage message)
        {
            await _emailServiceFactory().SendAsync(new EmailModel
            {
                DataBag = null,
                Subject = message.Subject,
                To = message.Destination,
                EmailTemplateContent = message.Body
            });
            // Plug in your email service here to send an email.
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your sms service here to send a text message.
            return Task.FromResult(0);
        }
    }

}