﻿using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Cdn.Image.Contracts.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace LuxuryApp.Core.Infrastructure.Api.Helpers
{
    public class CdnClientHandler
    {
        public static async Task<CdnResult> UploadFile(string apiUrl, string filePath, string token)
        {
            var content = File.ReadAllBytes(filePath);
            var responseUpload = await ClientHelper.PostContentAsync<CdnResult>(token, new Uri(apiUrl), "api/filecdn/uploadsingle", new CdnUploadParameter { Key = Path.GetFileName(filePath), Content = content });
            return responseUpload;
        }

        public static async Task<IEnumerable<CdnResult>> UploadFileBatch(string apiUrl, IEnumerable<CdnUploadParameter> uploadParameters, string token)
        {
            var responseUpload = await ClientHelper.PostContentAsync<IEnumerable<CdnResult>>(token, new Uri(apiUrl), "api/filecdn/uploadbatch", uploadParameters);
            return responseUpload;
        }

        public static async Task<IEnumerable<CdnResult>> UploadImagesBatch(string apiUrl, IEnumerable<CdnImageUploadParameter> uploadParameters, string token)
        {
            //   string json = JsonConvert.SerializeObject(uploadParameters);
            var responseUpload = await ClientHelper.PostContentAsync<IEnumerable<CdnResult>>(token, new Uri(apiUrl), "api/imagecdn/uploadbatch", uploadParameters);
            return responseUpload;
        }

        public static async Task<CdnResult> UploadImage(string apiUrl, CdnImageUploadParameter uploadParameter, string token)
        {
            var responseUpload = await ClientHelper.PostContentAsync<CdnResult>(token, new Uri(apiUrl), "api/imagecdn/uploadsingle", uploadParameter);
            return responseUpload;
        }
    }
}