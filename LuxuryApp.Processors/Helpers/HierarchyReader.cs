﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Helpers
{
    public static class HierarchyReader
    {
        public static List<Hierarchy> ToHierarchyTree(this DataReader reader)
        {
            //var reader = new DataReader { Reader = dataReader };

            if (reader == null) return null;

            var hierarchies = new List<Hierarchy>();
            while (reader.Read())
            {
                hierarchies.Add(new Hierarchy()
                {
                    HierarchyID = reader.GetInt("HierarchyID"),
                    BusinessID = reader.GetInt("BusinessID"),
                    DivisionID = reader.GetInt("DivisionID"),
                    DepartmentID = reader.GetInt("DepartmentID"),
                    CategoryID = reader.GetIntNullable("CategoryID"),

                    BusinessName = reader.GetString("BusinessName"),
                    DivisionName = reader.GetString("DivisionName"),
                    DepartmentName = reader.GetString("DepartmentName"),
                    CategoryName = reader.GetString("CategoryName")
                });
            }

            return hierarchies;
        }

        public static List<SelectListModel> ToSelectList(this DataReader reader)
        {
            if (reader == null) return null;

            var list = new List<SelectListModel>();
            while (reader.Read())
            {
                list.Add(new SelectListModel()
                {
                    ID = reader.GetInt("ID"),
                    Name = reader.GetString("Name")
                });
            }

            return list;
        }
    }
}
