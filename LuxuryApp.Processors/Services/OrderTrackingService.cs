﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gs1.Vics.Contracts.Factories;
using Gs1.Vics.Contracts.Models.Common;
using Gs1.Vics.Contracts.Models.Order;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Orders;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using Nito.AsyncEx;
using X2Order = Gs1.Vics.Contracts.Models.Order.Order;

namespace LuxuryApp.Processors.Services
{
    public class OrderTrackingService : IOrderTrackingService
    {
        private readonly IAddressService _addressService;
        private readonly IOrderRepository _orderRepository;
        private readonly IEdiSerializeServiceFactory _ediSerializeServiceFactory;
        private readonly ICompanyRepository _companyRepository;
        private readonly IFtpRepository _ftpRepository;

        public OrderTrackingService(
            IEdiSerializeServiceFactory ediSerializeServiceFactory,
            IOrderRepository orderRepository,
            IAddressService addressService,
            ICompanyRepository companyRepository,
            IFtpRepository ftpRepository)
        {
            _ediSerializeServiceFactory = ediSerializeServiceFactory;
            _orderRepository = orderRepository;
            _addressService = addressService;
            _companyRepository = companyRepository;
            _ftpRepository = ftpRepository;
        }

        public ApiResult<byte[]> GetSellerOrderX2Document(int orderSellerId, out string poNumber)
        {
            var order = AsyncContext.Run(() => _orderRepository.GetOrderItemForSellerById(orderSellerId));
            poNumber = order.PONumber;
            var sellerAddress =
                AsyncContext.Run(() => _addressService.GetAllByCompanyId(order.SellerCompanyId))
                    .FirstOrDefault(x => x.IsMain);
            var sellerCompany = AsyncContext.Run(() => _companyRepository.GetCompanyById(order.SellerCompanyId));
            var buyerAddress =
                AsyncContext
                    .Run(() => _addressService.GetAllByCompanyId(order.BuyerCompanyId))
                    .FirstOrDefault(x => x.IsMain);
            var buyerCompany = AsyncContext.Run(() => _companyRepository.GetCompanyById(order.BuyerCompanyId));
            var x2Order = MapOrderToX2Order(order, sellerCompany, sellerAddress, buyerCompany, buyerAddress);
            var result = _ediSerializeServiceFactory.Serialize(x2Order);
            return result;
        }

        private X2Order MapOrderToX2Order(OrderSellerView orderSellerItemView, Company sellerCompany, AddressInfo sellerAddressInfo,
            Company buyerCompany, AddressInfo buyerAddressInfo)
        {
            var date = DateTimeOffset.UtcNow;

            var result = new X2Order
            {
                OrderHeader = new OrderHeader()
                {
                    InterchangeControlNumber = orderSellerItemView.OrderSellerId,
                    InterchangeSenderId = "LUXURY001",
                    InterchangeReceiverId = "IT01027240488",
                    ApplicationSenderCode = "LUXURY001",
                    ApplicationReceiverCode = "IT01027240488",

                    PoNumber = orderSellerItemView.PONumber,
                    PurchaseOrderDate = orderSellerItemView.CreatedDate,

                    TestIndicator = TestIndicator.Production,

                    InterchangeDateTime = date,
                    GroupDateTime = date,
                    RequestedShipDate = date,
                    CancelAfterDate = date.AddDays(7),

                    ShipmentMethod = 'A',

                    SupplierName = sellerCompany.CompanyName,
                    SupplierAddressInformation = (sellerAddressInfo?.Street1 ?? "") + (sellerAddressInfo?.Street2 != null ? " " + sellerAddressInfo?.Street2 : ""),
                    SupplierCityName = sellerAddressInfo?.City,
                    //SupplierStateOrProvinceCode = // TODO
                    SupplierPostalCode = sellerAddressInfo?.ZipCode,
                    SupplierCountry = sellerAddressInfo?.CountryIso2Code,
                    SupplierIdentificationCode = sellerCompany.CompanyId.ToString(),

                    ShipToDestinationName = buyerCompany.CompanyName,
                    ShipToAddressInformation = (buyerAddressInfo?.Street1 ?? "") + (buyerAddressInfo?.Street2 != null ? " " + buyerAddressInfo?.Street2 : ""),
                    ShipToCityName = buyerAddressInfo?.City,
                    //ShipToStateOrProvinceCode = // TODO
                    ShipToPostalCode = buyerAddressInfo?.ZipCode,
                    ShipToCountryCode = buyerAddressInfo?.CountryIso2Code,
                    ShipToIdentificationCode = buyerCompany.CompanyId.ToString(),

                },
                OrderDetails =
                    MapProductItems(orderSellerItemView.ProductItems, sellerCompany, sellerAddressInfo),
            };

            return result;
        }

        private OrderDetail[] MapProductItems(IEnumerable<OrderProductItemSellerView> productItems,
            Company sellerCompany, AddressInfo sellerAddressInfo)
        {
            var ods =
            (from p in productItems
             from s in p.SizeItems
             select new OrderDetail
             {
                 Quantity = s.Units,
                 QuantityOrdered = s.ConfirmedUnits ?? s.Units, //todo: maybe?
                 UnitCost = (decimal)p.OfferCost,
                 UnitPrice = (decimal)p.OfferCost,
                 ProductDescription = p.ProductDetails.Description,
                 ColorDescription = p.ProductDetails.ColorCode,
                 SizeDescription = p.ProductDetails.SizeTypeName,
                 SkuNumber = p.ProductDetails.SKU,
                 HangOrFlat = "FLAT", //todo: softcode?
                 UnitOfMeasure = "EA", // todo: softcode?

                 ManufacturerName = sellerCompany.CompanyName,
                 ManufacturerAddressInformation = (sellerAddressInfo?.Street1 ?? "") + (sellerAddressInfo?.Street2 != null ? " " + sellerAddressInfo?.Street2 : ""),
                 ManufacturerCityName = sellerAddressInfo?.City,
                 //ManufacturerStateOrProvinceCode = // TODO
                 ManufacturerPostalCode = sellerAddressInfo?.ZipCode,
                 ManufacturerCountryCode = sellerAddressInfo?.CountryIso2Code,
                 ManufacturerIdentificationCode = sellerCompany.CompanyId.ToString(),
             }).ToArray();
            for (var i = 0; i < ods.Length; i++)
            {
                ods[i].LineItemNumber = i + 1;
            }
            return ods;
        }

        public ApiResult PushSellerOrderX2Document(int orderSellerId)
        {
            var poNumber = string.Empty;
            var orderResult = GetSellerOrderX2Document(orderSellerId, out poNumber);
            if (!orderResult.IsSuccesfull)
            {
                return new ApiResult(orderResult.BusinessRulesFailedMessages, orderResult.Exception);
            }

            var orderData = orderResult.Data;

            var ftpConfig = _ftpRepository.GetAllFtpConfigs().First();
            var currentDate = DateTime.Now;
            var ftpQueueItem = new FtpQueueItem
            {
                FtpConfigId = ftpConfig.Id,
                FileName = $"Order850_{poNumber}_{currentDate.ToString("yyyyMMddHHmmss")}",
                FileContentBytes = orderData,
                OrderSellerId = orderSellerId,
                PONumber = poNumber
            };

            _ftpRepository.EnqueueItem(ftpQueueItem);
            return new ApiResult();
        }
    }
}
