﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Readers;

namespace LuxuryApp.Processors.Services
{
    public class StateService
    {
        private readonly Repository _connection;

        public StateService()
        {
            _connection = new Repository();
        }

        public List<State> Search(int? id, int? countryId)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@ID", id),
                 new SqlParameter("@CountryID", countryId),
            };
            var reader = _connection.LoadData(StoredProcedureNames.States_Search, parameters.ToArray());
            return reader.ToStateModel();
        }

    }
}
