﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Offer;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Processors.Readers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Utils.Helpers;

namespace LuxuryApp.Processors.Services
{
    public class OfferService : IOfferService
    {
        private readonly Repository _connection;
        private readonly Func<IOfferRepository> _offerRepository;
        private readonly Func<IContextProvider> _contextProvider;
        public readonly Func<IOfferImportRepository> _oiRepositoryFactory;

        public OfferService(Func<IOfferRepository> offerRepository, Func<IContextProvider> contextProvider, Func<IOfferImportRepository> offerImportRepositoryFactory)
        {
            _connection = new Repository();
            _offerRepository = offerRepository;
            _contextProvider = contextProvider;
            _oiRepositoryFactory = offerImportRepositoryFactory;
        }

        public OfferSearchResponse Search(OfferSearch model)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@CompanyID", model.CompanyID),
              new SqlParameter("@SellerID", model.SellerID),
              new SqlParameter("@PageSize", model.PageSize),
              new SqlParameter("@PageNumber", model.Page),
              new SqlParameter("@Keyword", model.Keyword),
              new SqlParameter("@StatusID", model.StatusID),
            };

            var reader = _connection.LoadData(StoredProcedureNames.Offer_Search, parameters);
            return reader.ToOfferSearchResult();
        }

        public OfferLiveSummary GetLiveSummary(OfferLiveSummaryRequest model)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@CompanyID",model.CompanyID),
              new SqlParameter("@SellerID", model.SellerID)
            };

            var reader = _connection.LoadData(StoredProcedureNames.Offer_GetLiveSummary, parameters);
            return reader.ToOfferLiveSummaryResult();
        }

        public async Task<IEnumerable<OfferStatusResponse>> GetStatuses()
        {
            List<OfferStatusResponse> enumValList = new List<OfferStatusResponse>();

            foreach (var e in Enum.GetValues(typeof(OfferStatus)))
            {
                var fi = e.GetType().GetField(e.ToString());
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                enumValList.Add(new OfferStatusResponse()
                {
                    ID = (int)e,
                    Name = (attributes.Length > 0) ? attributes[0].Description : e.ToString()
                });
            }

            return enumValList;
        }

        public OfferImport GetProducts(int offerNumber)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@OfferNumber",offerNumber)
            };

            var reader = _connection.LoadData(StoredProcedureNames.Offer_GetProducts, parameters);
            return reader.ToOfferItemsResult();
        }

        public void UpdateOfferEndDate(Offer model)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@OfferID",model.Id),
              new SqlParameter("@OfferEndDate",model.EndDate)
            };

            _connection.ExecuteProcedure(StoredProcedureNames.Offer_UpdateEndDate, parameters);
        }

        public void DeleteOffer(int offerId)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@OfferNumber",offerId),
            };

            _connection.ExecuteProcedure(StoredProcedureNames.Offer_Delete, parameters);
        }

        public async Task<ApiResult<bool>> IsActiveOffer(int offerId)
        {
            var result = await _offerRepository().IsActiveOffer(offerId);
            return result.ToApiResult();
        }

        public async Task<OfferSellerProducts> GetSellerProducts(SellerProductsSearch search, bool overridePageSize = false, int newPageSize = 50)
        {
            var pageSize = overridePageSize ? newPageSize : WebConfigs.SellerProductsPageSize;

            // setup params
            var brandIDs = new SqlParameter("@BrandIDs", SqlDbType.Structured);
            brandIDs.Value = ListToDataTable(search.BrandIDs);

            var seasonsIDs = new SqlParameter("@SeasonIDs", SqlDbType.Structured);
            seasonsIDs.Value = ListToDataTable(search.SeasonsIDs);

            var categoryIDs = new SqlParameter("@CategoryIDs", SqlDbType.Structured);
            categoryIDs.Value = ListToDataTable(search.CategoryIDs);

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@PageNumber", search.PageNumber),
                new SqlParameter("@PageSize", pageSize),
                new SqlParameter("@SellerID", search.SellerID),

                categoryIDs,
                brandIDs,
                seasonsIDs,

                new SqlParameter("@Keyword", String.IsNullOrWhiteSpace(search.Keyword) ? String.Empty : search.Keyword)
            };

            // make db call
            var reader = _connection.LoadData(StoredProcedureNames.Offer_GetSellerProducts, parameters);

            // create response
            var response = reader.ToOfferSellerProductsResult();
            response.PageNumber = search.PageNumber;
            response.TotalPages = (int)Math.Ceiling((double)response.TotalProducts / pageSize);

            return response;
        }

        public async Task<OfferSellerSizes> GetSellerSizesForProduct(int sellerProductId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@SellerProductID", sellerProductId)
            };
            var reader = _connection.LoadData(StoredProcedureNames.Offer_GetSellerSizesForProduct, parameters);
            return reader.ToOfferSellerSizesForProductResult();
        }

        public async Task<ApiResult<OfferImport>> CreateOffer(OfferCreate model, int companyId)
        {
            List<OfferCreateProductTable> filteredProducts = model.Products;
            if (model.AreAllSelected)
            {
                model.Search.PageNumber = 1;

                var productsResultedFromSearch = await GetSellerProducts(model.Search, true, 20000);
                filteredProducts = new List<OfferCreateProductTable>();

                foreach (var product in productsResultedFromSearch.OfferSellerProductsList)
                {
                    bool exclude = model.ExcludedProducts.Find(x => x.ProductId == product.SellerProductId) != null;
                    if (!exclude)
                    {
                        double newPrice = 0;
                        switch (model.PriceSettings.Type)
                        {
                            case PriceSettingType.BelowCost:
                                newPrice = product.WholesaleCost - ((model.PriceSettings.Percentage / 100) * product.WholesaleCost);
                                break;
                            case PriceSettingType.BelowRetail:
                                newPrice = product.RetailPrice - ((model.PriceSettings.Percentage / 100) * product.RetailPrice);
                                break;
                            case PriceSettingType.AboveCost:
                                newPrice = product.WholesaleCost + ((model.PriceSettings.Percentage / 100) * product.WholesaleCost);
                                break;
                        }

                        filteredProducts.Add(new OfferCreateProductTable()
                        {
                            OfferPrice = (decimal)newPrice,
                            ProductId = product.SellerProductId
                        });
                    }
                }
            }


            var products = filteredProducts.ConvertToDataTable();

            //var sizes = model.Sizes.ConvertToDataTable();
            var userId = _contextProvider().GetLoggedInUserId();

            var outputOfferImportBatchIdParam = new SqlParameter("@OfferImportBatchID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@StatusID", OfferStatus.SentToPublish),
                new SqlParameter("@CompanyID", companyId),
                new SqlParameter("@ProductTable", products),
                
                //new SqlParameter("@OfferProductSizes", sizes),

                new SqlParameter("@CurrentUser", userId),
                new SqlParameter("@CurrentDate", DateTimeOffset.Now),


                outputOfferImportBatchIdParam
            };

            _connection.ExecuteProcedure(StoredProcedureNames.Offer_CreateOffer, parameters);

            var offerImportBatchId = int.Parse(outputOfferImportBatchIdParam.Value.ToString());

            var offerImportItem = new OfferImport
            {
                CompanyId = companyId,
                Id = offerImportBatchId,
                Items = await _oiRepositoryFactory().GetOfferImportProductItems(offerImportBatchId, null)
            };

            var summary = await _oiRepositoryFactory().GetOfferImportSummary(offerImportBatchId);
            offerImportItem.OfferStatus = (OfferStatus?)summary.StatusId;
            offerImportItem.OfferStatusName = summary.StatusName;
            offerImportItem.TotalCost = summary.TotalCostOffer;
            offerImportItem.TotalUnits = summary.TotalNumberOfUnits;

            //var errorList = await GetValidateErrors(offerImportBatchId, null, true);

            //if (errorList?.Any() == true)
            //{
            //    offerImportItem.Items.ToList().ForEach(x => x.Errors = errorList.Where(y => y.OfferImportBatchItemId == x.OfferImportBatchItemId).ToList());
            //}

            return offerImportItem.ToApiResult();
        }

        public async Task<List<ProductOfferSize>> GetOfferProductSizes(OfferProductSizesRequest model)
        {
            var userId = _contextProvider().GetLoggedInUserId();

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@CompanyID", model.CompanyID),
                new SqlParameter("@UserID", userId),
                new SqlParameter("@OfferProductID", model.OfferProductID)
            };

            var reader = _connection.LoadData(StoredProcedureNames.Offer_GetProductSizes, parameters);
            return reader.ToOfferProductSizes();
        }

        /// TODO: This needs to be moved in a generic place
        private DataTable ListToDataTable(List<int> ids)
        {
            DataTable idsDataTable = null;
            if (ids != null)
            {
                idsDataTable = new DataTable();
                idsDataTable.Columns.Add("ID", typeof(int));
                foreach (var prodId in ids)
                {
                    idsDataTable.Rows.Add(prodId);
                }
            }

            return idsDataTable;
        }
    }
}
