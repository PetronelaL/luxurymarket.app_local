﻿using LuxuryApp.Contracts.Services;
using System;
using LuxuryApp.Contracts.Repository;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models.Orders;
using System.Collections.Generic;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Contracts.Access;
using System.Linq;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Processors.Helpers;
using System.Data;
using System.Configuration;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Processors.Helpers.Export;
using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Contracts.Helpers;
using System.IO;

namespace LuxuryApp.Processors.Services
{
    public class OrderService : IOrderService
    {
        public readonly Func<IOrderRepository> _orderRepositoryFactory;
        private readonly IContextProvider _contextProvider;
        public readonly Func<ICompanyAccess> _accessFactory;
        public readonly Func<ICustomerService> _customerFactory;
        private readonly Func<INotificationHandler<EmailModel>> _emailServiceFactory;
        private readonly IOrderBookingService _orderBookingService;

        public OrderService(IContextProvider contextProvider,
                         Func<IOrderRepository> orderRepositoryFactory,
                         Func<ICompanyAccess> accessFactory,
                         Func<ICustomerService> customerFactory,
                         Func<INotificationHandler<EmailModel>> emailServiceFactory,
                         IOrderBookingService orderBookingService
                         )
        {
            _contextProvider = contextProvider;
            _orderRepositoryFactory = orderRepositoryFactory;
            _accessFactory = accessFactory;
            _customerFactory = customerFactory;
            _emailServiceFactory = emailServiceFactory;
            _orderBookingService = orderBookingService;
        }

        public async Task<ApiResult<string>> SubmitOrderAsync(OrderSubmitDataView model, DateTimeOffset date)
        {
            if (!_accessFactory().CanUserSubmitCart(model.CartId))
            {
                return new ApiResultForbidden<string>(null);
            }

            var orderId = await _orderRepositoryFactory().SubmitOrder(model.CartId,
                                                                      model.PaymentMethodId,
                                                                      _contextProvider.GetLoggedInUserId(),
                                                                      date);
            if (orderId > 0)
            {
                var orderEmails = await _orderRepositoryFactory().GetPoNumbersForEmails(orderId);
                if (orderEmails == null)
                    return new ApiResultNotFound<string>(null, "Emails not found!");

                // UploadCdnService.UploadFile
                var attachments = new List<AttachmentModel>();
                var cndParameters = new List<CdnUploadParameter>();

                var localPath = WebConfigs.OrderGeneratedFolder;
                foreach (var sellerItem in orderEmails.SellerEmailAddressList)
                {
                    var suborderData = _orderRepositoryFactory().GetSuborderData(sellerItem.OrderSellerId);
                    var suborderPDFFileContent = PDFHelper.GenereateOrderSellerPDF(suborderData);

                    var fileName = $"{sellerItem.PoNumber}.pdf";
                    var filePath = Path.Combine(localPath, fileName);

                    //copy file locally
                    File.WriteAllBytes(filePath, suborderPDFFileContent.ToArray());

                    //send email for seller
                    await SendSellerEmailOrderSubmitedAsync(sellerItem.OrderSellerId, sellerItem.PoNumber, sellerItem.EmailList);

                    attachments.Add(new AttachmentModel
                    {
                        FilePath = filePath,
                        FileName = fileName
                    });

                    cndParameters.Add(new CdnUploadParameter
                    {
                        Content = suborderPDFFileContent,
                        Key = fileName
                    });
                }


                //send email to cms
                await SendLuxuryMarketEmailOrderSubmitedAsync(orderEmails.OrderId, orderEmails.OrderPoNumber, attachments);

                //send email to buyer
                await SendBuyerEmailOrderSubmitedAsync(orderEmails.OrderId, orderEmails.OrderPoNumber, orderEmails.BuyerEmailAddress, attachments);

                //await UploadCdnClient.UploadFileBatch(WebConfigs.LuxuryMarketCDNUrl, cndParameters)

                return orderEmails.OrderPoNumber.ToApiResult();
            }

            //something went wrong
            return new ApiResultInternalError<string>(null, "The order was not submitted");
        }

        public async Task<ApiResult<Order>> GetOrderDetailsForBuyerAsync(int orderId)
        {
            //check company
            var result = await _orderRepositoryFactory().GetOrderDetailsForBuyer(orderId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<OrderSellerView>> GetOrderItemForSellerByIdAsync(int orderSellerId)
        {
            //check company
            var result = await _orderRepositoryFactory().GetOrderItemForSellerById(orderSellerId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<OrderSellerManageList>> FilterSellerOrdersAsync(RequestFilterOrders model)
        {
            //check if has access to the companyid
            var customerCompanies = await _customerFactory().GetCurrentCustomerCompanies();
            model.CompanyId = CompaniesHelper.CheckCompanyAccessAndType(customerCompanies?.Companies, model.CompanyId, CompanyType.Seller);
            if (!model.CompanyId.HasValue)
                return new ApiResultForbidden<OrderSellerManageList>(null);

            var result = await _orderRepositoryFactory().FilterSellerOrders(model, _contextProvider.GetLoggedInUserId());
            return result.ToApiResult();
        }

        public async Task<ApiResult<OrderBuyerManageList>> FilterBuyerOrdersAsync(RequestFilterOrders model)
        {
            //check if has access to the companyid
            var customerCompanies = await _customerFactory().GetCurrentCustomerCompanies();
            model.CompanyId = CompaniesHelper.CheckCompanyAccessAndType(customerCompanies?.Companies, model.CompanyId, CompanyType.Buyer);
            if (!model.CompanyId.HasValue)
                return new ApiResultForbidden<OrderBuyerManageList>(null);

            var result = await _orderRepositoryFactory().FilterBuyerOrders(model, _contextProvider.GetLoggedInUserId());
            return result.ToApiResult();
        }

        public async Task<IEnumerable<PaymentMethod>> GetPaymentMethodAsync()
        {
            var result = await _orderRepositoryFactory().GetPaymentMethod();
            return result;
        }

        public async Task<IEnumerable<OrderStatusType>> GetSuborderStatusTypesAsync()
        {
            var result = await _orderRepositoryFactory().GetSuborderStatusTypes();
            return result;
        }

        public async Task<ApiResult<bool>> UpdateStatusAsync(OrderStatusUpdateView model, DateTimeOffset date)
        {
            double orderConfirmationPercentage = 90;
            double.TryParse(ConfigurationManager.AppSettings["OrderConfirmationPercentage"], out orderConfirmationPercentage);

            var errorMessage = string.Empty;
            var returnStatusId = _orderRepositoryFactory().UpdateStatus(model, orderConfirmationPercentage, _contextProvider.GetLoggedInUserId(), date, out errorMessage);
            if (errorMessage != string.Empty)
                return new ApiResultForbidden<bool>(false, errorMessage);

            var attachments = new List<AttachmentModel>();

            if (returnStatusId == (int)OrderStatusTypes.ConfirmPendingPayment || returnStatusId == (int)OrderStatusTypes.PartialConfirmation)
            {
                var buyerData = _orderRepositoryFactory().GetBuyerDataAfterSellerConfirmation(model.OrderSellerId);
                var suborderData = _orderRepositoryFactory().GetSuborderData(model.OrderSellerId);
                var suborderPDFFileContent = PDFHelper.GenereateOrderConfirmedSellerPDF(suborderData);
                
                var localPath = WebConfigs.OrderGeneratedFolder;
                var fileName = $"{buyerData.OrderSellerPoNumber}.pdf";
                var filePath = Path.Combine(localPath, fileName);

                //copy file locally
                File.WriteAllBytes(filePath, suborderPDFFileContent.ToArray());

                attachments.Add(new AttachmentModel
                {
                    FilePath = filePath,
                    FileName = fileName
                });

                // send email to buyer
                await SendBuyerEmailAfterSellerConfirmationAsync(buyerData, (OrderStatusTypes)returnStatusId, attachments);

                // send email to luxury market
                await SendLuxuryMarketEmailOrderConfirmedAsync(buyerData, (OrderStatusTypes)returnStatusId, attachments);

                // send email to seller with items pdf attached
                //////////////////////////////////////////////////////////////
                var suborderSellerPDFFileContent = PDFHelper.GenereateOrderForSellerPDF(suborderData);

                var fileNameSeller = $"{buyerData.OrderSellerPoNumber}_seller.pdf";
                var filePathSeller = Path.Combine(WebConfigs.OrderGeneratedFolder, fileName);

                //copy file locally
                File.WriteAllBytes(filePathSeller, suborderSellerPDFFileContent.ToArray());

                await SendSellerEmailOrderConfirmedAsync(buyerData, new List<AttachmentModel>() {
                    new AttachmentModel { FilePath = filePathSeller, FileName = fileNameSeller }
                });
                //////////////////////////////////////////////////////////////
            }

            return true.ToApiResult();
        }

        public async Task<ApiResult<OrderProductTotalForSeller>> ConfirmUnitsAsync(OrderConfirmUnitView model, DateTimeOffset date)
        {
            if (model == null || model.OrderSellerProductId <= 0 || !model.ConfirmUnits.Any() || model.ConfirmUnits.Any(x => x.Id <= 0 || x.Unit < 0))
            {
                return new ApiResultBadRequest<OrderProductTotalForSeller>(null, "Invalid input");
            }
            var duplicateIds = model.ConfirmUnits.GroupBy(x => x.Id).Where(x => x.Count() > 1).Select(x => x.Key);
            if (duplicateIds == null && duplicateIds.Any())
                return new ApiResultBadRequest<OrderProductTotalForSeller>(null, $"Duplicate ids :{string.Join(", ", duplicateIds.ToArray())}");

            var errorMessage = string.Empty;
            var userId = _contextProvider.GetLoggedInUserId();

            errorMessage = _orderRepositoryFactory().CanConfirmUnits(model.OrderSellerProductId, userId);
            if (!string.IsNullOrWhiteSpace(errorMessage))
                return new ApiResultForbidden<OrderProductTotalForSeller>(null, errorMessage);

            errorMessage = await _orderRepositoryFactory().ConfirmUnits(model, userId, date);
            if (!string.IsNullOrWhiteSpace(errorMessage))
                return new ApiResultForbidden<OrderProductTotalForSeller>(null, errorMessage);

            var result = await _orderRepositoryFactory().GetOrderProductTotal(model.OrderSellerProductId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<OrderFileList>> GetFilesAsync(int? mainOrderId, int? orderSellerId, int? companyId)
        {
            var userId = _contextProvider.GetLoggedInUserId();
            if (!IsUserAssociatedToOrder(userId, mainOrderId, orderSellerId, companyId))
                return new ApiResultForbidden<OrderFileList>(null);

            var result = await _orderRepositoryFactory().GetFilesAsync(mainOrderId, orderSellerId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<int>> UpdateFileAsync(OrderFile model, int? companyId)
        {
            var userId = _contextProvider.GetLoggedInUserId();
            var result = await _orderRepositoryFactory().UpdateFileAsync(model, userId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<int>> InsertFileAsync(OrderFile model, int? companyId)
        {
            var userId = _contextProvider.GetLoggedInUserId();
            if (!IsUserAssociatedToOrder(userId, model.MainOrderId, model.OrderSellerId, companyId))
                return new ApiResultForbidden<int>(-1);

            var result = _orderRepositoryFactory().InsertFile(model, _contextProvider.GetLoggedInUserId());
            return result.ToApiResult();
        }

        public ApiResult<byte[]> GenerateOrderSellerPDF(int orderSellerId)
        {
            var suborderData = _orderRepositoryFactory().GetSuborderData(orderSellerId);
            var suborderPDFFileContent = PDFHelper.GenereateOrderSellerPDF(suborderData);

            return suborderPDFFileContent.ToApiResult(suborderPDFFileContent.Length);
        }

        #region Deprecated - one single save for package information

        public async Task<ApiResult<OrderSellerPackageCollection>> GetOrderSellerPackagesAsync(int ordersellerId)
        {
            var result = await _orderRepositoryFactory().GetOrderSellerPackages(ordersellerId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<OrderSellerPackingInformationInsert>> GetOrderSellerPackingInformationAsync(int orderSellerId)
        {
            var result = await _orderRepositoryFactory().GetOrderSellerPackingInformation(orderSellerId);
            return result.ToApiResult();
        }

        #endregion

        public async Task<ApiResult<int>> InsertOrderSellerPackingInformationAsync(OrderSellerPackingInformationInsert model)
        {
            var packageId = await _orderRepositoryFactory().InsertOrderSellerPackingInformation(model, _contextProvider.GetLoggedInUserId());
            if (packageId > 0)
            {
                _orderBookingService.PushSellerOrderBookingDocument(model.OrderSellerId);
                //send email
                await _orderBookingService.SendEmailAsync(model.OrderSellerId);
            }
            return packageId.ToApiResult();
        }

        public async Task<ApiResult<bool>> IsSubmittedPackageInformation(int ordersellerId)
        {
            var result = await _orderRepositoryFactory().IsSubmittedPackageInformation(ordersellerId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<byte[]>> ExportOrderExcel(int orderId, bool isBuyer)
        {
            if (isBuyer)
            {
                var buyerOrderInformation = await GetOrderDetailsForBuyerAsync(orderId);
                var buyerOrderExcelFileContent = ExportHelper.ExportBuyerOrderExcel(buyerOrderInformation.Data);
                return buyerOrderExcelFileContent.ToApiResult(buyerOrderExcelFileContent.Length);
            }
            else
            {
                var sellerOrderInformation = await GetOrderItemForSellerByIdAsync(orderId);
                var sellerOrderExcelFileContent = ExportHelper.ExportSellerOrderExcel(sellerOrderInformation.Data);
                return sellerOrderExcelFileContent.ToApiResult(sellerOrderExcelFileContent.Length);
            }
        }

        #region Private Methods

        private async Task SendBuyerEmailOrderSubmitedAsync(int orderId, string ponumber, string emailAddress, List<AttachmentModel> files)
        {
            await _emailServiceFactory().SendAsync(new EmailModel
            {
                DataBag = new
                {
                    PoNumber = ponumber,
                    Link = $"{ConfigurationManager.AppSettings["WebUrl"]}/#/admin/orders/{orderId}",
                    LOGOURL = WebConfigs.LogoUrl,
                },
                Subject = $"Your order {ponumber} was submited.",
                To = emailAddress,
                EmailTemplateName = "OrderSubmitedBuyer",
                Attachments = files
            });
        }

        private async Task SendBuyerEmailAfterSellerConfirmationAsync(BuyerEmailDataAfterSellerConfirmation model, OrderStatusTypes statusType, List<AttachmentModel> files)
        {
            String emailTemplate = string.Empty;
            String subject = String.Empty;

            switch (statusType)
            {
                case OrderStatusTypes.ConfirmPendingPayment:
                    emailTemplate = "OrderSellerConfirmedBySeller";
                    subject = $"{model.OrderSellerPoNumber} was confirmed by the seller.";
                    break;
                case OrderStatusTypes.PartialConfirmation:
                    emailTemplate = "OrderSellerPartialConfirmedBySeller";
                    subject = $"{model.OrderSellerPoNumber} was confirmed by the seller.";
                    break;
            }

            await _emailServiceFactory().SendAsync(new EmailModel
            {
                DataBag = new
                {
                    FullName = model.BuyerFullName,
                    SellerAlias = model.SellerAlias,
                    PoNumber = model.OrderSellerPoNumber,
                    Link = $"{ConfigurationManager.AppSettings["WebUrl"]}/#/admin/orders/{model.OrderId}",
                    LOGOURL = WebConfigs.LogoUrl
                },
                Subject = subject,
                To = model.BuyerEmail,
                EmailTemplateName = emailTemplate,
                Attachments = files
            });
        }

        private async Task SendSellerEmailOrderSubmitedAsync(int orderId, string ponumber, List<string> emailAddress)
        {
            // send email to sellers
            await _emailServiceFactory().SendAsync(new EmailModel
            {
                DataBag = new
                {
                    PoNumber = ponumber,
                    Link = $"{ConfigurationManager.AppSettings["WebUrl"]}/#/admin/orders/{orderId}",//?
                    LOGOURL = WebConfigs.LogoUrl,

                },
                Subject = $"New Order {ponumber}",
                To = string.Join(";", emailAddress),
                EmailTemplateName = "OrderSubmitedSeller"
            });
        }

        private async Task SendSellerEmailOrderConfirmedAsync(BuyerEmailDataAfterSellerConfirmation model, List<AttachmentModel> files)
        {
            // send email to sellers
            await _emailServiceFactory().SendAsync(new EmailModel
            {
                DataBag = new
                {
                    PoNumber = model.OrderSellerPoNumber,
                    Link = $"{ConfigurationManager.AppSettings["WebUrl"]}/#/admin/orders/{model.OrderSellerId}",
                    LOGOURL = WebConfigs.LogoUrl
                },
                Subject = $"Order {model.OrderSellerPoNumber} Details",
                To = string.Join(";", model.SellerEmail),
                EmailTemplateName = "OrderConfirmedSeller",
                Attachments = files
            });
        }

        private async Task SendLuxuryMarketEmailOrderSubmitedAsync(int orderId, string ponumber, List<AttachmentModel> files)
        {
            // send email to luxurymarket
            await _emailServiceFactory().SendAsync(new EmailModel
            {
                DataBag = new
                {
                    PoNumber = ponumber,
                    Link = $"{ConfigurationManager.AppSettings["CMSUrl"]}/Order?statusId=&search={ponumber.Replace("#", "")}",
                    LOGOURL = WebConfigs.LogoUrl
                },
                Subject = $"New Order Placed {ponumber}",
                To = ConfigurationManager.AppSettings["OrderSubmitedLM"],
                EmailTemplateName = "OrderSubmitedLuxuryMarket",
                Attachments = files
            });

        }

        private async Task SendLuxuryMarketEmailOrderConfirmedAsync(BuyerEmailDataAfterSellerConfirmation model, OrderStatusTypes statusType, List<AttachmentModel> files)
        {

            String subject = String.Empty;
            switch (statusType)
            {
                case OrderStatusTypes.ConfirmPendingPayment:
                    subject = $"New Order Confirmed {model.OrderSellerPoNumber}";
                    break;
                case OrderStatusTypes.PartialConfirmation:
                    subject = $"New Order Partially Confirmed {model.OrderSellerPoNumber}";
                    break;
            }

            // send email to luxurymarket
            await _emailServiceFactory().SendAsync(new EmailModel
            {
                DataBag = new
                {
                    PoNumber = model.OrderSellerPoNumber,
                    Link = $"{ConfigurationManager.AppSettings["CMSUrl"]}/Order?statusId=&search={model.OrderSellerPoNumber.Replace("#", "")}",
                    LOGOURL = WebConfigs.LogoUrl
                },
                Subject = subject,
                To = ConfigurationManager.AppSettings["OrderSubmitedLM"],
                EmailTemplateName = "OrderConfirmedLuxuryMarket",
                Attachments = files
            });
        }

        private bool IsUserAssociatedToOrder(int userId, int? mainOrderId, int? orderSellerId, int? companyId)
        {
            var isUserAssociated = false;
            if ((mainOrderId ?? 0) > 0)
                isUserAssociated = _orderRepositoryFactory().IsUserAssociatedToMainOrder(mainOrderId.Value, userId, companyId);
            else if ((orderSellerId ?? 0) > 0)
                isUserAssociated = _orderRepositoryFactory().IsUserAssociatedToOrderSeller(orderSellerId.Value, userId, companyId);

            return isUserAssociated;
        }

        #endregion
    }
}
