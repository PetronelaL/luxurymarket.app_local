﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Core.Infrastructure.Logging;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LuxuryApp.Cdn.Image.Contracts.Model;
using LuxuryApp.Cdn.Image.Contracts;
using System.Net;
using System.Diagnostics;

namespace LuxuryApp.Processors.Agents
{
    public class VendorImageAgent : IVendorImageAgent
    {
        private readonly IVendorImageRepository _vendorImageRepository;
        private readonly IImageCdnService _imageCdnService;
        private ILogger _logger;

        private readonly int _UserId;
        private readonly int _DelaySeconds;

        public VendorImageAgent(IVendorImageRepository vendorImageRepository, IImageCdnService imageCdnService, ILogger logger)
        {
            _vendorImageRepository = vendorImageRepository;
            _imageCdnService = imageCdnService;
            _logger = logger;
            _UserId = Convert.ToInt32(ConfigurationManager.AppSettings["userId"]);
            _DelaySeconds = 10;
            int.TryParse(ConfigurationManager.AppSettings["WorkerPeriodSeconds"], out _DelaySeconds);
        }

        public async Task<double> UploadImagesAsync()
        {
            _logger.Information($"{DateTime.UtcNow} - Start VendorImageAgent");

            var watch = new Stopwatch();
            watch.Start();
            var numberProcessedItems = 0;
            var numberErrorItems = 0;

            var items = await GetUnprocessedImaged();

            foreach (var item in items)
            {
                try
                {
                    //download

                    item.FileContentBytes = GetFileContent(item.Url);
                    item.FileSizeBytes = item.FileContentBytes.Length;
                    item.FileName = CreateImageName(item.Url, item.SellerSKU);

                    //cdn upload
                    item.IsProcessed = UploadToCdn(item.FileContentBytes, item.FileName, item.Url);

                    //update processed image

                    if (item.IsProcessed)
                    {
                        await UpdateProcessedImageAsync(item);
                    }
                    numberProcessedItems++;
                }
                catch (Exception ex)
                {
                    _logger.Error($"Error at processing itemid={item.Id}; url {item.Url} - {ex.ToString()}");
                    numberErrorItems++;
                }
            }

            watch.Stop();
            _logger.Information($"Task Summary:ExecutionTime={watch.Elapsed};NumberErrors={numberErrorItems};NumberUploadedImages={numberProcessedItems}.\n");

            return watch.Elapsed.TotalSeconds;
        }

        #region Private Methods

        private byte[] GetFileContent(string url)
        {
            using (var webClient = new WebClient())
            {
                byte[] data = webClient.DownloadData(url);
                return data;
            }
        }

        private string CreateImageName(string url, string sellerSKU)
        {
            var imageName = string.Empty;
            try
            {
                var parseUrl = url.Split('/');
                var sellerImageName = parseUrl[parseUrl.Length - 1];

                var extension = sellerImageName.Substring(sellerImageName.LastIndexOf(".") + 1);
                imageName = $"{sellerImageName.Remove(sellerImageName.LastIndexOf("."))}_{DateTime.UtcNow.ToFileTimeUtc()}.{extension}";
            }
            catch (Exception ex)
            {
                _logger.Error($"CreateImageName - error creating name {ex.ToString()}");
                imageName = $"{sellerSKU}_{DateTime.UtcNow.ToFileTimeUtc()}.png";  //default
            }
            return imageName;
        }
        private async Task<IEnumerable<VendorImageItem>> GetUnprocessedImaged()
        {
            var images = await _vendorImageRepository.GetUnprocessedImagesAsync();
            return images;
        }

        private async Task<int> UpdateProcessedImageAsync(VendorImageItem item)
        {
            var result = await _vendorImageRepository.UpdateProcessedImageAsync(item, _UserId);
            return result;
        }

        private bool UploadToCdn(byte[] content, string fileName, string url)
        {
            var cdnParameter = new CdnImageUploadParameter
            {
                Content = content,
                Key = fileName,
                ImageSizes = CdnImageSizeList.ImageSizes
            };

            var result = _imageCdnService.Upload(cdnParameter);

            if (result == null || !result.IsSuccessful)
            {
                _logger.Error($"Images {fileName} from url {url} not uploaded! cdnResults is null or not successful; { result?.Exception?.Message ?? string.Empty}");
                return false;
            }
            return true;
        }



        #endregion
    }
}
