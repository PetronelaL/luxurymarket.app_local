﻿
namespace LuxuryApp.Processors
{
    public static class StoredProcedureNames
    {
        public static string RegisterRequest_Insert = "App.RegistrationRequest_Insert";

        public static string Products_GetByID = "app.usp_ProductGetByID";
        public static string Products_GetBySKU = "App.usp_ProductGetBySku";
        public static string Products_Search = "APP.usp_ProductSearch";
        public static string Products_Search_FeaturedEvent = "usp_Products_GetForFeaturedEvent";
        public static string Products_GetBestSellers = "usp_ProductGetBestSellers";

        public static string OfferImport_Insert = "usp_OfferImport_Insert";

        public static string Countries_Search = "Countries_GetByID";
        public static string States_Search = "States_Search";
        public static string CurrencyGetByID = "usp_CurrencyGetByID";
        public static string CurrencyGetByName = "usp_CurrencyGetByName";

        public static string BrandsGetByID = "usp_BrandsGetByID";
        public static string ColorsSearch = "usp_ColorsSearch";
        public static string StandardColorsSearch = "usp_StandardColorsSearch";
        public static string BrandsSearch = "usp_BrandsSearch";
        public static string MaterialSearch = "usp_MaterialSearch";

        public static string OfferImportBatch_Update = "usp_OfferImportBatch_Update";
        public static string Offer_Search = "dbo.usp_OfferSearch";
        public static string Offer_GetLiveSummary = "dbo.usp_getLiveOffersSummary";
        public static string Offer_GetProducts = "dbo.usp_GetOfferProducts";
        public static string Offer_UpdateEndDate = "dbo.usp_Offer_UpdateEndDate";
        public static string Offer_Delete = "dbo.usp_Offer_Delete";
        public static string Offer_GetSellerProducts = "usp_Offer_GetSellerProducts";
        public static string Offer_GetSellerSizesForProduct = "usp_Offer_GetSellerSizesForProduct";
        public static string Offer_CreateOffer = "usp_Offer_CreateOffer";

        public static string HierarchiesGet = "Hierachy_Get";

        public static string OfferImportBatchUpdateLineBuy = "usp_OfferImportBatchUpdateLineBuy";
        public static string OfferImportBatchItem_Validation = "usp_OfferImportBatchItem_Validation";

        public static string Products_GetModelNumberByBrandID = "usp_Products_GetModelNumberByBrandID";
        public static string OfferImportReportedErrors_Insert = "usp_OfferImportReportedErrors_Insert";
        public static string OfferImportBatch_GetByAppUserID = "usp_OfferImportBatch_GetByAppUserID";

        public static string FeaturedEvent_Get = "usp_GetFeaturedEvents";

        public static string AddressGetAllByCompanyID = "usp_Addresses_GetAllByCompanyID";
        public static string AddressGetByAddressID = "usp_Addresses_GetByAddressID";
        public static string AddressSetMain = "usp_Addresses_SetMain";
        public static string AddressDelete = "usp_Addresses_Delete";
        public static string AddressSave = "usp_Addresses_SaveInfo";

        public static string CategoriesGet = "usp_GetCategories";
        public static string SeasonsGet = "usp_Seasons_GetAll";

        #region Inventory

        public static string Inventory_GetSellerAPISettings = "Inventory_GetSellerAPISettings";
        public static string Inventory_GetSellerSKUsForInventoryUpdate = "Inventory_GetSellerSKUsForInventoryUpdate";

        #endregion

        public static string Offer_GetProductSizes = "usp_GetOfferProductSizes";

    }
}
