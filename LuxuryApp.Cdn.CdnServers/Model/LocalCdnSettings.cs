﻿using LuxuryApp.Cdn.Contracts;

namespace LuxuryApp.Cdn.CdnServers.Model
{
    public class LocalCdnSettings : ICdnSettings
    {
        public int ConfigurationId { get; set; }
        public string LocalRootPath { get; set; }
        public string WebRootPath { get; set; }
    }
}
