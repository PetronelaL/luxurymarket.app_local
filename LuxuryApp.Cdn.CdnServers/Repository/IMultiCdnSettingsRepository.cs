﻿using System.Collections.Generic;
using LuxuryApp.Cdn.Contracts;

namespace LuxuryApp.Cdn.CdnServers.Repository
{
    public interface IMultiCdnGetter<out TCdnSettings> where TCdnSettings : ICdnSettings
    {
        TCdnSettings GetCdnSettings(int configurationId);
        IEnumerable<TCdnSettings> GetAllCdnSettings();
    }

    public interface IMultiCdnSetter<in TCdnSettings> where TCdnSettings : ICdnSettings
    {
        int SaveCdnSettings(TCdnSettings settings);
    }

    public interface IMultiCdnSettingsRepository<TCdnSettings>:
        IMultiCdnGetter<TCdnSettings>,
        IMultiCdnSetter<TCdnSettings>
        where TCdnSettings: ICdnSettings
    {
        IEnumerable<int> GetCdnSettingsIds(CdnConfigurationType cdnConfigurationType);
    }
}
