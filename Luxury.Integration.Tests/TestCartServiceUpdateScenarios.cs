﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using FluentAssertions;
//using LuxuryApp.Core.Infrastructure.Api.Models;
//using LuxuryApp.Contracts.Models.Cart;
//using NUnit.Framework.Internal;
//using NUnit.Framework;

//namespace Luxury.Integration.Tests
//{
//    [TestFixture]
//    public class TestCartServiceUpdateScenarios: TestCartService
//    {
//        [Test]
//        public void UpdateCartItemsQuantities_WhenAnyProductOfferHasSetLineBuy_ShouldReturnUprocessableEntity()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductSetLineBuy();
//                var offerProductSize = t.Item1;
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 1)).Data.CartSummaryItem.CartId;
//                // Act

//                var result =
//                    cartService.UpdateCartItemsQuantities(new SaveOfferProductSizesToCartModel(buyerCompanyId,
//                        offerProductSize.Id, 2));

//                // Assert
//                result.Should().BeOfType<ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>>();
//                result.BusinessRulesFailedMessages.Single()
//                    .Should()
//                    .Contain("product")
//                    .And.Contain("line")
//                    .And.Contain("buy");
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void UpdateCartItemsQuantities_WhenAnyProductOfferHasTakeAll_ShouldReturnUnprocessableEntity()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferTakeAll();
//                var offerProductSize = t.OfferProducts.First().OfferProductSizes.First();
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 1)).Data.CartSummaryItem.CartId;
//                // Act

//                var result =
//                    cartService.UpdateCartItemsQuantities(new SaveOfferProductSizesToCartModel(buyerCompanyId,
//                        offerProductSize.Id, 2));

//                // Assert
//                result.Should().BeOfType<ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>>();
//                result.BusinessRulesFailedMessages.Single()
//                    .Should()
//                    .Contain("offer")
//                    .And.Contain("take")
//                    .And.Contain("all");
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void UpdateCartItemsQuantities_WhenAnyQuantityIsNegative_ShouldReturnUnprocessableEntity()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductNotSetLineBuy();
//                var offerProductSize = t.Item1;
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 1)).Data.CartSummaryItem.CartId;
//                // Act

//                var result =
//                    cartService.UpdateCartItemsQuantities(new SaveOfferProductSizesToCartModel(buyerCompanyId,
//                        offerProductSize.Id, -1));

//                // Assert
//                result.Should().BeOfType<ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>>();
//                result.BusinessRulesFailedMessages.Single().Should().Contain("nonnegative");
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void UpdateCartItemsQuantities_WhenAnyQuantityIsGreatherThanInOffer_ShouldReturnUnprocessableEntity()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductNotSetLineBuy();
//                var offerProductSize = t.Item1;
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 1)).Data.CartSummaryItem.CartId;
//                // Act

//                var result =
//                    cartService.UpdateCartItemsQuantities(new SaveOfferProductSizesToCartModel(buyerCompanyId,
//                        offerProductSize.Id, 10000000));

//                // Assert
//                result.Should().BeOfType<ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>>();
//                result.BusinessRulesFailedMessages.First()
//                    .Should()
//                    .Contain("not")
//                    .And.Contain("available")
//                    .And.Contain("stock");
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void UpdateCartItemsQuantities_WhenAnyOfferProductSizeIdIsNotInCart_ShouldReturnUnprocessableEntity()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductNotSetLineBuy();
//                var offerProductSize = t.Item1;
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 1)).Data.CartSummaryItem.CartId;
//                // Act

//                var result =
//                    cartService.UpdateCartItemsQuantities(new SaveOfferProductSizesToCartModel(buyerCompanyId,
//                        -1, 2));

//                // Assert
//                result.Should().BeOfType<ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>>();
//                result.BusinessRulesFailedMessages.Single()
//                    .Should()
//                    .Contain("item")
//                    .And.Contain("not")
//                    .And.Contain("found");
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void UpdateCartItemsQuantities_WhenProductIsNotLineBuyOrTakeAllAndQuantityIsOk_ShouldUpdateQuantities()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductNotSetLineBuy();
//                var offerProductSize = t.Item1;
//                var expectedCart = GetExpectedCartWhenProductOfferHasNotSetLineBuy(buyerCompanyId, true);
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 1)).Data.CartSummaryItem.CartId;
//                var expectedResultData = new UpdateCartItemsQuantitiesResult
//                {
//                    CartSummary = GetExpectedCartSummaryWhenProductOfferHasNotSetLineBuy(buyerCompanyId, true),
//                    CartItemSummaries = new CartItemSummary[]
//                    {
//                        new CartItemSummary
//                        {
//                            ItemTotalOfferCost = (decimal) (2*2000*Constants.CompanyFee),
//                            CartItemSummaries = new[]
//                            {
//                                new CartItemSellerSummary
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    UnitsCount = 2,
//                                    TotalOfferCost = (decimal) (2*2000*Constants.CompanyFee),
//                                }
//                            }
//                        }
//                    }
//                };
//                // Act

//                var result =
//                    cartService.UpdateCartItemsQuantities(new SaveOfferProductSizesToCartModel(buyerCompanyId,
//                        offerProductSize.Id, 2));

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                result.Data.ShouldBeEquivalentTo(expectedResultData);
//                var actualCart = cartService.GetCartForBuyer(buyerCompanyId).Data;
//                actualCart.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [TestCase(false, false)]
//        [TestCase(false, true)]
//        [TestCase(true, true)]
//        [TestCase(true, false)]
//        public void SetCartItemSellerUnitSizeActiveFlag_WhenProductOfferHasNotSetLineBuy_ShouldSetFlagToSelectedItem(
//            bool initialValue, bool valueToSet)
//        {
//            // Arrange 
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            var sellerCompanyId = GetSellerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductNotSetLineBuy();
//                var offerProductSize = t.Item1;
//                var expectedCart = GetExpectedCartWhenProductOfferHasNotSetLineBuy(buyerCompanyId, valueToSet);
//                var expectedResult =
//                    GetExpectedSetCartItemActiveFlagResultWhenProductOfferHasNotSetLineBuy(buyerCompanyId, valueToSet);
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 2)).Data.CartSummaryItem.CartId;
//                cartService.SetCartItemSellerUnitSizeActiveFlag(new SetCartItemActiveModel
//                {
//                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier
//                    {
//                        CartId = cartId,
//                        OfferProductId = t.Item2.Id,
//                    },
//                    ActiveFlag = initialValue,
//                });

//                // Act

//                var result =
//                    cartService.SetCartItemSellerUnitSizeActiveFlag(new SetCartItemActiveModel
//                    {
//                        CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier
//                        {
//                            CartId = cartId,
//                            OfferProductId = t.Item2.Id,
//                        },
//                        ActiveFlag = valueToSet,
//                    });

//                // Assert

//                result.IsSuccesfull.Should().BeTrue();
//                result.Data.ShouldBeEquivalentTo(expectedResult);
//                cartService.GetCartForBuyer(buyerCompanyId).Data.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        private SetCartItemActiveFlagResult GetExpectedSetCartItemActiveFlagResultWhenProductOfferHasNotSetLineBuy(int companyId, bool isItemActive)
//        {
//            var expectedCartTotals = isItemActive
//                ? GetExpectedCartTotalsWhenProductOfferHasNotSetLineBuy()
//                : GetExpectedCartTotalsZero();
//            return new SetCartItemActiveFlagResult
//            {
//                CartSummary = new CartSummary { 
//                    ItemsCount = expectedCartTotals.ItemsCount,
//                    UnitsCount = expectedCartTotals.UnitsCount,
//                    TotalCustomerCost = expectedCartTotals.TotalOfferCost,
//                    Currency = expectedCartTotals.Currency,
//                },
//                CartItems = GetExpectedCartWhenProductOfferHasNotSetLineBuy(companyId, isItemActive).Items,
//            };
//        }

//        [TestCase(false, false)]
//        [TestCase(false, true)]
//        [TestCase(true, true)]
//        [TestCase(true, false)]
//        public void
//            SetSetCartItemSellerUnitSizeActiveFlag_WhenProductOfferHasSetLineBuy_ShouldSetFlagToProductWithAllSizesFromCart
//            (bool initialValue, bool valueToSet)
//        {
//            // Arrange 
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            var sellerCompanyId = GetSellerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductSetLineBuy();
//                var offerProductSize = t.Item1;
//                var expectedCart = GetExpectedCartWhenProductOfferHasSetLineBuy(buyerCompanyId, valueToSet);
//                var expectedResult =
//                    GetExpectedSetCartItemActiveFlagResultWhenProductOfferHasSetLineBuy(buyerCompanyId, valueToSet);
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 2)).Data.CartSummaryItem.CartId;
//                // Act

//                var result =
//                    cartService.SetCartItemSellerUnitSizeActiveFlag(new SetCartItemActiveModel
//                    {
//                        CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier
//                        {
//                            CartId = cartId,
//                            OfferProductId = t.Item2.Id,
//                        },
//                        ActiveFlag = valueToSet,
//                    });

//                // Assert

//                result.IsSuccesfull.Should().BeTrue();
//                result.Data.ShouldBeEquivalentTo(expectedResult);
//                cartService.GetCartForBuyer(buyerCompanyId).Data.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        private SetCartItemActiveFlagResult GetExpectedSetCartItemActiveFlagResultWhenProductOfferHasSetLineBuy(int buyerCompanyId, bool isItemActive)
//        {
//            var expectedCartTotals = isItemActive
//                ? GetExpectedCartTotalsWhenProductOfferHasSetLineBuy()
//                : GetExpectedCartTotalsZero();
//            return new SetCartItemActiveFlagResult
//            {
//                CartSummary = new CartSummary
//                {
//                    ItemsCount = expectedCartTotals.ItemsCount,
//                    UnitsCount = expectedCartTotals.UnitsCount,
//                    TotalCustomerCost = expectedCartTotals.TotalOfferCost,
//                    Currency = expectedCartTotals.Currency,
//                },
//                CartItems = GetExpectedCartWhenProductOfferHasSetLineBuy(buyerCompanyId, isItemActive).Items,
//            };
//        }

//        [TestCase(false, false)]
//        [TestCase(false, true)]
//        [TestCase(true, true)]
//        [TestCase(true, false)]
//        public void
//            SetSetCartItemSellerUnitSizeActiveFlag_WhenOfferHasSetTakeAll_ShouldSetFlagToAllOfferProductsWithAllSizesFromCart
//            (bool initialValue, bool valueToSet)
//        {
//            // Arrange 
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            var sellerCompanyId = GetSellerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferTakeAll();
//                var offerProductSize = t.OfferProducts.First().OfferProductSizes.First();
//                var expectedCart = GetExpectedCartWhenOfferHasTakeAll(buyerCompanyId, valueToSet);
//                var expectedResult =
//                    GetExpectedSetCartItemActiveFlagResultWhenOfferHasTakeAll(buyerCompanyId, valueToSet);
//                cartId =
//                    cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId,
//                        offerProductSize.Id, 2)).Data.CartSummaryItem.CartId;

//                // Act

//                var result =
//                    cartService.SetCartItemSellerUnitSizeActiveFlag(new SetCartItemActiveModel
//                    {
//                        CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier
//                        {
//                            CartId = cartId,
//                            OfferProductId = t.OfferProducts.First().Id,
//                        },
//                        ActiveFlag = valueToSet,
//                    });

//                // Assert

//                result.IsSuccesfull.Should().BeTrue();
//                result.Data.ShouldBeEquivalentTo(expectedResult);
//                cartService.GetCartForBuyer(buyerCompanyId).Data.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        private SetCartItemActiveFlagResult GetExpectedSetCartItemActiveFlagResultWhenOfferHasTakeAll(int buyerCompanyId, bool isItemActive)
//        {
//            var expectedCartTotals = isItemActive
//                ? GetExpectedCartTotalsWhenOfferHasTakeAll()
//                : GetExpectedCartTotalsZero();
//            return new SetCartItemActiveFlagResult
//            {
//                CartSummary = new CartSummary
//                {
//                    ItemsCount = expectedCartTotals.ItemsCount,
//                    UnitsCount = expectedCartTotals.UnitsCount,
//                    TotalCustomerCost = expectedCartTotals.TotalOfferCost,
//                    Currency = expectedCartTotals.Currency,
//                },
//                CartItems = GetExpectedCartWhenOfferHasTakeAll(buyerCompanyId, isItemActive).Items,
//            };
//        }
//    }
//}
