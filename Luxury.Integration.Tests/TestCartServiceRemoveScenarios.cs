﻿//using System;
//using System.CodeDom;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Autofac;
//using FluentAssertions;
//using LuxuryApp.Contracts.Models.Cart;
//using LuxuryApp.Contracts.Models.Offer;
//using LuxuryApp.Contracts.Repository;
//using LuxuryApp.Contracts.Services;
//using NUnit.Framework.Internal;
//using NUnit.Framework;

//namespace Luxury.Integration.Tests
//{
//    [TestFixture]
//    public class TestCartServiceRemoveScenarios
//        : TestCartService
//    {

//        [Test]
//        public void GetCart_AfterEmptyCart_ShouldReturnEmptyCart()
//        {
//            // Arrange
//            Offer[] offers = null;
//            int cartId = 0;
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            try
//            {
//                var emptyCart =
//                    RemoveProductScenariosProvider.GetEmptyCart(RemoveProductScenariosProvider.DefaultCurrency);
//                var expectedData =
//                    RemoveProductScenariosProvider.GetEmptyCartSummary(RemoveProductScenariosProvider.DefaultCurrency);
//                PrepareCartForScenario(RemoveProductScenariosProvider.GetSampleSellerOffers(), out cartId, out offers);

//                // Act

//                var result = cartService.EmptyCartForBuyer(buyerCompanyId);

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                result.Data.ShouldBeEquivalentTo(expectedData);

//                var cart = cartService.GetCartForBuyer(buyerCompanyId).Data;
//                cart.ShouldBeEquivalentTo(emptyCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//                CleanOffers(offers);
//            }
//        }

//        [TestCaseSource(typeof(RemoveProductScenariosProvider),nameof(RemoveProductScenariosProvider.GetRemoveProductScenarions))]
//        public void GetCart_AfterRemoveProduct_ShouldReturnExpected(RemoveProductScenario scenario)
//        {
//            // Arrange
//            Offer[] offers = null;
//            int cartId = 0;
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            try
//            {
//                PrepareCartForScenario(scenario.SellerOffers, out cartId, out offers);

//                var removeProductIds = GetProductIds(scenario.RemoveProducts);
//                // Act

//                cartService.RemoveProductsFromCart(new RemoveProductsFromCartModel
//                {
//                    BuyerCompanyId = buyerCompanyId,
//                    ProductIds = removeProductIds
//                });

//                // Assert
//                var cart = cartService.GetCartForBuyer(buyerCompanyId).Data;
//                cart.ShouldBeEquivalentTo(scenario.ExpectedCart,
//                    config => config.Excluding(x => x.SelectedMemberPath.EndsWith("DiscountPercentage")));
//            }
//            finally
//            {
//                CleanCart(cartId);
//                CleanOffers(offers);
//            }
//        }

//        [TestCaseSource(typeof(RemoveProductScenariosProvider), nameof(RemoveProductScenariosProvider.GetRemoveProductScenarions))]
//        public void RemoveProduct_ShouldReturnExpected(RemoveProductScenario scenario)
//        {
//            // Arrange
//            Offer[] offers = null;
//            int cartId = 0;
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            try
//            {
//                PrepareCartForScenario(scenario.SellerOffers, out cartId, out offers);

//                var removeProductIds = GetProductIds(scenario.RemoveProducts);

//                var expected = GetRemoveProductExpectedResult(scenario);

//                // Act

//                var result = cartService.RemoveProductsFromCart(new RemoveProductsFromCartModel
//                {
//                    BuyerCompanyId = buyerCompanyId,
//                    ProductIds = removeProductIds
//                });

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                result.Data.ShouldBeEquivalentTo(expected, 
//                    ctx => ctx.Excluding(c => c.CartSummary)); // we'll suppose anyway the summary will be correct
//            }
//            finally
//            {
//                CleanCart(cartId);
//                CleanOffers(offers);
//            }
//        }

//        private RemoveProductFromCartResult GetRemoveProductExpectedResult(RemoveProductScenario scenario)
//        {
//            var expectedRemovedProducIds = GetProductIds(scenario.ExpectedRemovedProducts);

//            var result = new RemoveProductFromCartResult
//            {
//                RemovedProductIds = expectedRemovedProducIds,
//            };
//            return result;
//        }

//        [TestCaseSource(typeof(RemoveProductScenariosProvider), nameof(RemoveProductScenariosProvider.GetRemoveItemSellerUnitScenarios))]
//        public void GetCart_AfterRemoveItemSellerUnit_ShouldReturnExpected(RemoveItemSellerUnitScenario scenario)
//        {
//            // Arrange
//            Offer[] offers = null;
//            int cartId = 0;
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            try
//            {
//                PrepareCartForScenario(scenario.SellerOffers, out cartId, out offers);

//                var sellerUnitIdentifier = GetCartItemSellerUnitIdentifiers(cartId, scenario.RemoveProduct, offers);
//                // Act

//                cartService.RemoveItemSellerUnitFromCart(new RemoveItemSellerUnitFromCartModel
//                {
//                    BuyerCompanyId = buyerCompanyId,
//                    CartItemSellerUnitIdentifier = sellerUnitIdentifier,
//                });

//                // Assert
//                var cart = cartService.GetCartForBuyer(buyerCompanyId).Data;
//                cart.ShouldBeEquivalentTo(scenario.ExpectedCart,
//                    config => config.Excluding(x => x.SelectedMemberPath.EndsWith("DiscountPercentage")));
//            }
//            finally
//            {
//                CleanCart(cartId);
//                CleanOffers(offers);
//            }
//        }

//        [TestCaseSource(typeof(RemoveProductScenariosProvider), nameof(RemoveProductScenariosProvider.GetRemoveItemSellerUnitScenarios))]
//        public void RemoveItemSellerUnit_ShouldReturnExpectedRemovedProductIds(RemoveItemSellerUnitScenario scenario)
//        {
//            // Arrange
//            Offer[] offers = null;
//            int cartId = 0;
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            try
//            {
//                PrepareCartForScenario(scenario.SellerOffers, out cartId, out offers);

//                var sellerUnitIdentifier = GetCartItemSellerUnitIdentifiers(cartId, scenario.RemoveProduct, offers);

//                var expectedRemovedProducIds =
//                    GetProductIds(scenario.ExpectedRemoved.RemovedProductsSkus);
//                // Act

//                var result = cartService.RemoveItemSellerUnitFromCart(new RemoveItemSellerUnitFromCartModel
//                {
//                    BuyerCompanyId = buyerCompanyId,
//                    CartItemSellerUnitIdentifier = sellerUnitIdentifier,
//                });

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                result.Data.RemovedProductIds.ShouldBeEquivalentTo(expectedRemovedProducIds, because:"Expected removed products doesn't match");
//            }
//            finally
//            {
//                CleanCart(cartId);
//                CleanOffers(offers);
//            }
//        }

//        [TestCaseSource(typeof(RemoveProductScenariosProvider), nameof(RemoveProductScenariosProvider.GetRemoveItemSellerUnitScenarios))]
//        public void RemoveItemSellerUnit_ShouldReturnExpectedAffectedLinesForProductIds(RemoveItemSellerUnitScenario scenario)
//        {
//            // Arrange
//            Offer[] offers = null;
//            int cartId = 0;
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            try
//            {
//                PrepareCartForScenario(scenario.SellerOffers, out cartId, out offers);

//                var sellerUnitIdentifier = GetCartItemSellerUnitIdentifiers(cartId, scenario.RemoveProduct, offers);

//                var expectedAffectedProductIds =
//                    GetProductIds(scenario.ExpectedRemoved.AffectedItemsProductSkus);
//                // Act

//                var result = cartService.RemoveItemSellerUnitFromCart(new RemoveItemSellerUnitFromCartModel
//                {
//                    BuyerCompanyId = buyerCompanyId,
//                    CartItemSellerUnitIdentifier = sellerUnitIdentifier,
//                });

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();

//                result.Data.AffectedRemainingCartItems.Select(x => x.ProductId).ToArray()
//                    .ShouldBeEquivalentTo(expectedAffectedProductIds); /* we'll just test wether the affected lines are the ones we expect.
//                                                                        * and we'll suppose the actual items values are computed properly */
//            }
//            finally
//            {
//                CleanCart(cartId);
//                CleanOffers(offers);
//            }
//        }

//        private Offer[] CreateOffers(SellerOffer[] sellerOffers)
//        {
//            var offerRepository = Container.Resolve<IOfferRepository>();
//            var tmpOffers = new List<Offer>(sellerOffers.Length);
//            foreach (var sellerOffer in sellerOffers)
//            {
//                sellerOffer.Offer.SellerCompanyId = GetSellerCompanyId(sellerOffer.CompanyName);
//                var offerId = offerRepository.SaveOffer(sellerOffer.Offer, GetSellerCompanyId(sellerOffer.CompanyName));
//                tmpOffers.Add(sellerOffer.Offer);
//                if (offerId == 0)
//                {
//                    throw new ApplicationException("Failed to prepare scenarion: " +
//                                                   $"failed to save offer {sellerOffer.Offer.OfferName} with number {sellerOffer.Offer.OfferNumber} ");
//                }
//            }
//            return tmpOffers.ToArray();
//        }

//        private void CleanOffers(Offer[] offers)
//        {
//            var offerRepository = Container.Resolve<IOfferRepository>();
//            if (offers == null)
//            {
//                return;
//            }
//            foreach (var offer in offers)
//            {
//                offerRepository.DeleteOffer(offer.Id);
//            }
//        }

//        private void PrepareCartForScenario(SellerOffer[] sellerOffers,
//            out int cartId,
//            out Offer[] offers)
//        {
//            var cartService = Container.Resolve<ICartService>();
//            offers = CreateOffers(sellerOffers);
//            var buyerCompanyId = GetBuyerCompanyId();
//            foreach (var offer in offers)
//            {
//                cartService.AddEntireOfferToCart(new AddEntireOfferToCartModel(buyerCompanyId, offer.Id));
//            }
//            cartId = cartService.GetCartSummaryForBuyer(buyerCompanyId).Data.CartId;
//        }

//        private int[] GetProductIds(RemoveProductScenario.RemoveProductData[] scenarioRemoveProducts)
//        {

//            return GetProductIds(scenarioRemoveProducts.Select(x => x.ProductSku).ToArray());
//        }

//        private int[] GetProductIds(string[] skus)
//        {
//            var productService = Container.Resolve<IProductService>();
//            return skus.Select(sku => productService.GetProductBySku(sku).ID).ToArray();
//        }

//        private CartItemSellerUnitIdentifier GetCartItemSellerUnitIdentifiers(
//            int cartId,
//            RemoveItemSellerUnitScenario.RemoveProductData scenarioRemoveProductData,
//            Offer[] offers)
//        {
//            var cartRepository = Container.Resolve<ICartRepository>();
//            var cartItems = cartRepository.GetCartItems(cartId);

//            var productService = Container.Resolve<IProductService>();
//            var productId = productService.GetProductBySku(scenarioRemoveProductData.ProductSku).ID;
//            var sellerCompanyId = GetSellerCompanyId(scenarioRemoveProductData.SellerCompanyName);
//            var offerProductIds = offers
//                .Where(o => o.SellerCompanyId == sellerCompanyId)
//                .SelectMany(x => x.OfferProducts)
//                .Where(y => y.ProductId == productId)
//                .Select(y => y.Id).ToArray();

//            var result = cartItems.SelectMany(x => x.SellerUnits)
//                .Where(y => y.CartItemSellerUnitIdentifier.CartId == cartId &&
//                            offerProductIds.Any(oi => oi == y.CartItemSellerUnitIdentifier.OfferProductId))
//                .Select(r => r.CartItemSellerUnitIdentifier)
//                .Single();
//            return result;
//        }
//    }
//}