﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using Autofac;
//using FluentAssertions;
//using LuxuryApp.Auth.Api.Models;
//using LuxuryApp.Contracts.Access;
//using LuxuryApp.Contracts.Enums;
//using LuxuryApp.Contracts.Interfaces;
//using LuxuryApp.Contracts.Models;
//using LuxuryApp.Contracts.Models.Cart;
//using LuxuryApp.Contracts.Models.Offer;
//using LuxuryApp.Contracts.Models.Shipment;
//using LuxuryApp.Contracts.Models.Shipping;
//using LuxuryApp.Contracts.Repository;
//using NUnit.Framework;
//using LuxuryApp.Contracts.Services;
//using LuxuryApp.Core.DataAccess;
//using LuxuryApp.Core.DataAccess.Repository;
//using LuxuryApp.Processors.Access;
//using LuxuryApp.Processors.Services;
//using Microsoft.AspNet.Identity;
//using LuxuryApp.Core.Infrastructure.Authorization;

//namespace Luxury.Integration.Tests
//{

//    [TestFixture]
//    public class TestShippingService
//    {
//        protected const string BuyerCompanyName = "Eyelash Bouquet"; 
//        protected const string BuyerUserName = "RhondaMorfunt@mejix.com";

//        private static IContainer _container;

//        protected static IProductService ProductService { get; set; }
//        protected static ICartService CartService { get; set; }
//        protected static IShippingService ShippingService { get; set; }

//        protected static IOfferRepository OfferRepository { get; set; }
//        protected static ICartRepository CartRepository { get; set; }
//        protected static ICompanyRepository CompanyRepository { get; set; }

//        static TestShippingService()
//        {
//            var builder = new ContainerBuilder();
//            builder.RegisterType<LuxuryMarketConnectionStringProvider>()
//                .As<IConnectionStringProvider>();

//            #region fake IContextProvider
//            TestHelpers.RegisterUserStore(builder);
//            builder.Register(c => new FakeContextProvider(c.Resolve<IUserStore<ApplicationUser, int>>(), BuyerUserName))
//                .As<IContextProvider>();
//            #endregion

//            builder.RegisterType<CompanyRepository>()
//                .As<ICompanyRepository>();
//            builder.RegisterType<OfferRepository>()
//                .As<IOfferRepository>();
//            builder.RegisterType<CurrencyService>()
//                .As<ICurrencyService>();
//            builder.RegisterType<CartRepository>()
//                .As<ICartRepository>();
//            builder.RegisterType<TrivialCompanyAccess>()
//                .As<ICompanyAccess>();
//            builder.RegisterType<CartService>()
//                .As<ICartService>();
//            builder.RegisterType<ProductService>()
//                .As<IProductService>();
//            builder.RegisterType<ShippingRepository>()
//                .As<IShippingRepository>();
//            builder.RegisterType<ProductRepository>()
//                .As<IProductRepository>();
//            builder.RegisterType<ShippingService>()
//                .As<IShippingService>();
//            _container = builder.Build();

//            ProductService = _container.Resolve<IProductService>();
//            CartService = _container.Resolve<ICartService>();
//            ShippingService = _container.Resolve<IShippingService>();
//            OfferRepository = _container.Resolve<IOfferRepository>();
//            CartRepository = _container.Resolve<ICartRepository>();
//            CompanyRepository = _container.Resolve<ICompanyRepository>();
//        }

//        //[Test]
//        //public void PreviewShipping_WhenCartIsFromOneSeller_ShouldReturnExpected()
//        //{
//        //    throw new NotImplementedException();
//        //}

//        //[Test]
//        //public void PreviewShipping_WhenCartHasItemsFromDifferentRegions_ShoulReturnExpected()
//        //{
//        //    throw new NotImplementedException();
//        //}

//        [Test, TestCaseSource(nameof(PreviewShippingScenarioTestCases))]
//        public void PreviewShipping_ShouldReturnExpected(PreviewShippingScenario scenario)
//        {
//            // Arrange 
//            int cartId = 0;
//            int[] offerIds = null;
//            try
//            {
//                PrepareCartForScenario(scenario.SellerOfferCases, out cartId, out offerIds);

//                // Act
//                var result = ShippingService.PreviewShippingForCart(cartId);

//                // Assert
//                result.IsSuccesfull.Should().Be(true);
//                result.Data.ShouldBeEquivalentTo(scenario.ExpectedShipping);
//            }
//            finally
//            {
//                Clean(cartId, offerIds);
//            }
//        }

//        private void PrepareCartForScenario(SellerOffer[] sellerOffers,
//            out int cartId,
//            out int[] offerIds)
//        {
            
//            offerIds = CreateOffers(sellerOffers);
//            var buyerCompanyId = GetBuyerCompanyId();
//            foreach (var offerId in offerIds)
//            {
//                CartService.AddEntireOfferToCart(new AddEntireOfferToCartModel(buyerCompanyId, offerId));
//            }
//            cartId = CartService.GetCartSummaryForBuyer(buyerCompanyId).Data.CartId;
//        }

//        private int GetBuyerCompanyId()
//        {
//            int totalRecords;
//            return CompanyRepository.SearchCompaniesByName(BuyerCompanyName, out totalRecords).Single().CompanyId;
//        }

//        private int[] CreateOffers(SellerOffer[] sellerOffers)
//        {
//            List<int> tmpOfferIds = new List<int>(sellerOffers.Length);
//            foreach (var sellerOffer in sellerOffers)
//            {
//                sellerOffer.Offer.SellerCompanyId = GetSellerCompanyId(sellerOffer.CompanyName);
//                tmpOfferIds.Add(OfferRepository.SaveOffer(sellerOffer.Offer, GetSellerUserIdForOffer(sellerOffer)));
//            }
//            return tmpOfferIds.ToArray();
//        }

//        private int GetSellerCompanyId(string sellerOfferCompanyName)
//        {
//            return CompanyRepository.SearchCompaniesByName(sellerOfferCompanyName).First().CompanyId;
//        }

//        private int GetSellerUserIdForOffer(SellerOffer sellerOffer)
//        {
//            return
//                _container.Resolve<IUserStore<ApplicationUser, int>>().FindByNameAsync(sellerOffer.UserName).Result.Id;
//        }

//        public static IEnumerable PreviewShippingScenarioTestCases
//        {
//            get
//            {
//                var currency = new Currency
//                {
//                    CurrencyId = 2,
//                    Name = "USD",
//                    Rate = 1,
//                };
//                var productNonApparel = ProductService.GetProductBySku("414581DHY0N1000");
//                var productApparel = ProductService.GetProductBySku("12345DHY0N1000");
//                yield return
//                    new TestCaseData(
//                        new PreviewShippingScenario
//                        {
//                            SellerOfferCases = new[]
//                            {
//                                new SellerOffer
//                                {
//                                    CompanyName = "Polka Inferno",
//                                    UserName = "UlyssesPlulpigger@mejix.com",
//                                    Offer = new Offer
//                                    {
//                                        OfferName = "Offer_By_[Polka Inferno]",
//                                        OfferNumber = 90823983,
//                                        StartDate = DateTime.Today,
//                                        OfferStatus = OfferStatus.Published,
//                                        OfferProducts = new[]
//                                        {
//                                            new OfferProduct
//                                            {
//                                                ProductId = productNonApparel.ID,
//                                                OfferCost = 50,
//                                                OfferProductSizes = new OfferProductSize[]
//                                                {
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 2,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "44",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    },
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 3,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "48",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    }
//                                                },
//                                                Currency = currency,
//                                            },
//                                            new OfferProduct
//                                            {
//                                                ProductId = productApparel.ID,
//                                                OfferCost = 60,
//                                                OfferProductSizes = new OfferProductSize[]
//                                                {
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 4,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "44",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    },
//                                                },
//                                                Currency = currency,
//                                            }
//                                        },
//                                        Currency = currency,
//                                    }
//                                }
//                            },
//                            ExpectedShipping = new Shipping
//                            {
//                                Currency = currency,
//                                Shipments = new Shipment[]
//                                {
//                                    new Shipment
//                                    {
//                                        ApparelCost = (decimal) (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)),
//                                        NonApparelCost = (decimal) ((2 + 3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)),
//                                        Currency = currency,
//                                        ShipsFromRegion = "Europe",
//                                        ShipingIterval = "2 to 5 days",
//                                        TotalShippingCost = (decimal) 
//                                                            (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17) + 
//                                                            (2 + 3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)),
//                                        TotalStylesCount = 2,
//                                        ShipmentItems = new ShipmentItem[]
//                                        {
//                                            new ShipmentItem
//                                            {
//                                                ProductName = "De Manta Clutch",
//                                                BrandName = "ALEXANDER MCQUEEN",
//                                                ProductMainImageName = "0a765a22-8e24-4fa5-9a12-8a93a825887e",
//                                                IsApparel = false,
//                                                ShipmentItemByBySellers = new[]
//                                                {
//                                                    new ShipmentItemBySeller
//                                                    {
//                                                        SellerAlias = "ONR13",
//                                                        UnitOfferCost = (decimal) Math.Ceiling(50*Constants.CompanyFee),
//                                                        UnitsCount = 2 + 3,
//                                                        TotalOfferCost = (decimal) ((2 + 3)*Math.Ceiling(50*Constants.CompanyFee)),
//                                                        Currency = currency,
//                                                    },
//                                                },
//                                                TotalUnits = 2 + 3,
//                                                TotalCost = (decimal) ((2 + 3)*Math.Ceiling(50*Constants.CompanyFee)),
//                                                ShippingCost = (decimal) ((2 + 3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)),
//                                                Currency = currency,
//                                            },
//                                            new ShipmentItem
//                                            {
//                                                ProductName = "23armani canvaspure black armani",
//                                                BrandName = "BALENCIAGA",
//                                                ProductMainImageName = "1144e36e-ea91-4bf3-82c0-b03a6ca8b9f2",
//                                                IsApparel = true,
//                                                ShipmentItemByBySellers = new[]
//                                                {
//                                                    new ShipmentItemBySeller
//                                                    {
//                                                        SellerAlias = "ONR13",
//                                                        UnitOfferCost = (decimal) Math.Ceiling(60*Constants.CompanyFee),
//                                                        UnitsCount = 4,
//                                                        TotalOfferCost = (decimal) (4*Math.Ceiling(60*Constants.CompanyFee)),
//                                                        Currency = currency,
//                                                    },
//                                                },
//                                                TotalUnits = 4,
//                                                TotalCost = (decimal) (4*Math.Ceiling(60*Constants.CompanyFee)),
//                                                ShippingCost = (decimal) (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)),
//                                                Currency = currency,
//                                            }
//                                        }
//                                    }
//                                },
//                                TotalShippingCost = (decimal) ((2 + 3) * Math.Ceiling(Math.Ceiling(50 *Constants.CompanyFee)*0.21)) +
//                                                    (decimal) (4 * Math.Ceiling(Math.Ceiling(60 *Constants.CompanyFee)*0.17))
//                            }
//                        }).SetName("Shipping from one seller");

//                yield return
//                    new TestCaseData(
//                        new PreviewShippingScenario
//                        {
//                            SellerOfferCases = new[]
//                            {
//                                new SellerOffer
//                                {
//                                    CompanyName = "Polka Inferno",
//                                    UserName = "UlyssesPlulpigger@mejix.com",
//                                    Offer = new Offer
//                                    {
//                                        OfferName = "Offer_By_[Polka Inferno]",
//                                        OfferNumber = 90823983,
//                                        StartDate = DateTime.Today,
//                                        OfferStatus = OfferStatus.Published,
//                                        OfferProducts = new[]
//                                        {
//                                            new OfferProduct
//                                            {
//                                                ProductId = productNonApparel.ID,
//                                                OfferCost = 50,
//                                                OfferProductSizes = new OfferProductSize[]
//                                                {
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 2,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "44",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    },
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 3,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "48",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    }
//                                                },
//                                                Currency = currency,
//                                            },
//                                            new OfferProduct
//                                            {
//                                                ProductId = productApparel.ID,
//                                                OfferCost = 60,
//                                                OfferProductSizes = new OfferProductSize[]
//                                                {
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 4,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "44",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    },
//                                                },
//                                                Currency = currency,
//                                            }
//                                        },
//                                        Currency = currency,
//                                    }
//                                },
//                                new SellerOffer
//                                {
//                                    CompanyName = "Bernadina Enrico",
//                                    UserName = "UlyssesPlulpigger@mejix.com",
//                                    Offer = new Offer
//                                    {
//                                        OfferName = "Offer_By_[Bernadina Enrico]",
//                                        OfferNumber = 90823989,
//                                        StartDate = DateTime.Today,
//                                        OfferStatus = OfferStatus.Published,
//                                        OfferProducts = new[]
//                                        {
//                                            new OfferProduct
//                                            {
//                                                ProductId = productNonApparel.ID,
//                                                OfferCost = 40,
//                                                OfferProductSizes = new OfferProductSize[]
//                                                {
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 4,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "44",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    },
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 5,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "48",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    }
//                                                },
//                                                Currency = currency,
//                                            },
//                                            new OfferProduct
//                                            {
//                                                ProductId = productApparel.ID,
//                                                OfferCost = 70,
//                                                OfferProductSizes = new OfferProductSize[]
//                                                {
//                                                    new OfferProductSize
//                                                    {
//                                                        Quantity = 8,
//                                                        Size =
//                                                            new Size
//                                                            {
//                                                                Name = "44",
//                                                                SizeType = new SizeType {Name = "T2"}
//                                                            },
//                                                    },
//                                                },
//                                                Currency = currency,
//                                            }
//                                        },
//                                        Currency = currency,
//                                    }
//                                }
//                            }
//                            ,
//                            ExpectedShipping = new Shipping
//                            {
//                                Currency = currency,
//                                Shipments = new Shipment[]
//                                {
//                                    new Shipment
//                                    {
//                                        ApparelCost = (decimal) (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)),
//                                        NonApparelCost = (decimal) ((2+3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)),
//                                        Currency = currency,
//                                        ShipsFromRegion = "Europe",
//                                        ShipingIterval = "2 to 5 days",
//                                        TotalShippingCost = (decimal) ((4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)) + ((2+3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21))),
//                                        TotalStylesCount = 2,
//                                        ShipmentItems = new ShipmentItem[]
//                                        {
//                                            new ShipmentItem
//                                            {
//                                                ProductName = "De Manta Clutch",
//                                                BrandName = "ALEXANDER MCQUEEN",
//                                                ProductMainImageName = "0a765a22-8e24-4fa5-9a12-8a93a825887e",
//                                                IsApparel = false,
//                                                ShipmentItemByBySellers = new[]
//                                                {
//                                                    new ShipmentItemBySeller
//                                                    {
//                                                        SellerAlias = "ONR13",
//                                                        UnitOfferCost = (decimal) (Math.Ceiling(50*Constants.CompanyFee)),
//                                                        UnitsCount = 2 + 3,
//                                                        TotalOfferCost = (decimal) ((2 + 3)*Math.Ceiling(50*Constants.CompanyFee)),
//                                                        Currency = currency,
//                                                    },
//                                                },
//                                                TotalUnits = 2 + 3,
//                                                TotalCost = (decimal) ((2 + 3)*Math.Ceiling(50*Constants.CompanyFee)),
//                                                ShippingCost = (decimal) ((2+3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)),
//                                                Currency = currency,
//                                            },
//                                            new ShipmentItem
//                                            {
//                                                ProductName = "23armani canvaspure black armani",
//                                                BrandName = "BALENCIAGA",
//                                                IsApparel = true,
//                                                ProductMainImageName = "1144e36e-ea91-4bf3-82c0-b03a6ca8b9f2",
//                                                ShipmentItemByBySellers = new[]
//                                                {
//                                                    new ShipmentItemBySeller
//                                                    {
//                                                        SellerAlias = "ONR13",
//                                                        UnitOfferCost = (decimal) (Math.Ceiling(60*Constants.CompanyFee)),
//                                                        UnitsCount = 4,
//                                                        TotalOfferCost = (decimal) (4*Math.Ceiling(60*Constants.CompanyFee)),
//                                                        Currency = currency,
//                                                    },
//                                                },
//                                                TotalUnits = 4,
//                                                TotalCost = (decimal) (4*Math.Ceiling(60*Constants.CompanyFee)),
//                                                ShippingCost = (decimal) (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)),
//                                                Currency = currency,
//                                            }
//                                        }
//                                    },
//                                    new Shipment
//                                    {
//                                        ApparelCost = (decimal) (8*Math.Ceiling(Math.Ceiling(70*Constants.CompanyFee)*0.16)),
//                                        NonApparelCost = (decimal) ((4+5)*Math.Ceiling(Math.Ceiling(40*Constants.CompanyFee)*0.23)),
//                                        Currency = currency,
//                                        ShipsFromRegion = "Europe",
//                                        ShipingIterval = "5 to 7 days",
//                                        TotalShippingCost = (decimal) ((8*Math.Ceiling(Math.Ceiling(70*Constants.CompanyFee)*0.16)) + ((4+5)*Math.Ceiling(Math.Ceiling(40*Constants.CompanyFee)*0.23))),
//                                        TotalStylesCount = 2,
//                                        ShipmentItems = new ShipmentItem[]
//                                        {
//                                            new ShipmentItem
//                                            {
//                                                ProductName = "De Manta Clutch",
//                                                BrandName = "ALEXANDER MCQUEEN",
//                                                ProductMainImageName = "0a765a22-8e24-4fa5-9a12-8a93a825887e",
//                                                IsApparel = false,
//                                                ShipmentItemByBySellers = new[]
//                                                {
//                                                    new ShipmentItemBySeller
//                                                    {
//                                                        SellerAlias = "OCI16",
//                                                        UnitOfferCost = (decimal) Math.Ceiling(40*Constants.CompanyFee),
//                                                        UnitsCount = 4 + 5,
//                                                        TotalOfferCost = (decimal) ((4+5)*Math.Ceiling(40*Constants.CompanyFee)),
//                                                        Currency = currency,
//                                                    },
//                                                },
//                                                TotalUnits = 4 + 5,
//                                                TotalCost = (decimal) ((4+5)*Math.Ceiling(40*Constants.CompanyFee)),
//                                                ShippingCost = (decimal) ((4+5)*Math.Ceiling(Math.Ceiling(40*Constants.CompanyFee)*0.23)),
//                                                Currency = currency,
//                                            },
//                                            new ShipmentItem
//                                            {
//                                                ProductName = "23armani canvaspure black armani",
//                                                BrandName = "BALENCIAGA",
//                                                ProductMainImageName = "1144e36e-ea91-4bf3-82c0-b03a6ca8b9f2",
//                                                IsApparel = true,
//                                                ShipmentItemByBySellers = new[]
//                                                {
//                                                    new ShipmentItemBySeller
//                                                    {
//                                                        SellerAlias = "OCI16",
//                                                        UnitOfferCost = (decimal) (Math.Ceiling(70*Constants.CompanyFee)),
//                                                        UnitsCount = 8,
//                                                        TotalOfferCost = (decimal) (8*Math.Ceiling(70*Constants.CompanyFee)),
//                                                        Currency = currency,
//                                                    },
//                                                },
//                                                TotalUnits = 8,
//                                                TotalCost = (decimal) (8*Math.Ceiling(70*Constants.CompanyFee)),
//                                                ShippingCost = (decimal) (8*Math.Ceiling(Math.Ceiling(70*Constants.CompanyFee)*0.16)),
//                                                Currency = currency,
//                                            }
//                                        }
//                                    },
//                                },
//                                TotalShippingCost =
//                                    (decimal) (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)) + (decimal) ((2+3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)) +
//                                    (decimal) (8*Math.Ceiling(Math.Ceiling(70*Constants.CompanyFee)*0.16)) + (decimal) ((4+5)*Math.Ceiling(Math.Ceiling(40*Constants.CompanyFee)*0.23)),
//                            }
//                        }).SetName("Shipping from two sellers from different regions");
//            }
//        }

//        public class PreviewShippingScenario
//        {
//            public SellerOffer[] SellerOfferCases { get; set; }
//            public Shipping ExpectedShipping { get; set; }
//        }

//        [Test]
//        public void CancelShipment_ShouldReturnExpected()
//        {
//            // Arrange
//            int cartId = 0;
//            int[] offerIds = null;
//            try
//            {
//                CancelShipmentModel cancelShipmentModel;
//                ArrangeCancelShipmentScenario(out cartId, out offerIds, out cancelShipmentModel);
//                var expectedData = GetCancelShipmentExpectedShippingSummary(cartId);
//                // Act
//                var result = ShippingService.CancelShipment(cancelShipmentModel);

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                result.Data.ShouldBeEquivalentTo(expectedData);
//            }
//            finally
//            {
//                Clean(cartId, offerIds);
//            }
//        }

//        private ShippingSummary GetCancelShipmentExpectedShippingSummary(int cartId)
//        {
//            var currency = new Currency
//            {
//                CurrencyId = 2,
//                Name = "USD",
//                Rate = 1,
//            };
//            return new ShippingSummary
//            {
//                CartId = cartId,
//                ShipmentsCount = 1,
//                TotalShippingCost = (decimal) (4 * Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)) + 
//                                    (decimal)((2 + 3) * Math.Ceiling(Math.Ceiling(50 *Constants.CompanyFee)*0.21)),
//                Currency = currency,
//            };
//        }

//        [Test]
//        public void PreviewShipping_AfterCancelShipment_ShouldReturnExpected()
//        {
//            // Arrange
//            int cartId = 0;
//            int[] offerIds = null;
//            try
//            {
//                CancelShipmentModel cancelShipmentModel;
//                ArrangeCancelShipmentScenario(out cartId, out offerIds, out cancelShipmentModel);
//                var expectedShipping = GetCancelShipmentExpectedShipping();
//                // Act
//                var result = ShippingService.CancelShipment(cancelShipmentModel);

//                var shipping = ShippingService.PreviewShippingForCart(cartId).Data;

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                shipping.ShouldBeEquivalentTo(expectedShipping);
//            }
//            finally
//            {
//                Clean(cartId, offerIds);
//            }
//        }

//        [Test]
//        public void GetCart_AfterCancelShipment_ShouldReturnExpected()
//        {
//            // Arrange
//            int cartId = 0;
//            int[] offerIds = null;
//            try
//            {
//                CancelShipmentModel cancelShipmentModel;
//                ArrangeCancelShipmentScenario(out cartId, out offerIds, out cancelShipmentModel);
//                var expectedCart = GetCancelShipmentExpectedCart();
//                // Act
//                var result = ShippingService.CancelShipment(cancelShipmentModel);
//                var cart = CartRepository.GetCart(cartId);

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                cart.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                Clean(cartId, offerIds);
//            }
//        }

//        private void ArrangeCancelShipmentScenario(out int cartId, out int[] offerIds, out CancelShipmentModel cancelShipmentModel)
//        {
//            var sellerOfferCases = GetCancelShipmentScenarioSellerOfferCases();
//            PrepareCartForScenario(sellerOfferCases, out cartId, out offerIds);

//            cancelShipmentModel = GetCancelShipmentModel(cartId);
//        }

//        private CancelShipmentModel GetCancelShipmentModel(int cartId)
//        {
//            var shipping = ShippingService.PreviewShippingForCart(cartId);
//            var shippingRegionId = shipping.Data.Shipments.Where(x => x.ShipsFromRegion == "Europe"
//                                               && x.ShipingIterval == "5 to 7 days").Select(y => y.ShippingRegionId).Single();
//            return new CancelShipmentModel
//            {
//                CartId = cartId,
//                ShippingRegionId = shippingRegionId,
//            };
//        }

//        private SellerOffer[] GetCancelShipmentScenarioSellerOfferCases()
//        {
//            var currency = new Currency
//            {
//                CurrencyId = 2,
//                Name = "USD",
//                Rate = 1,
//            };
//            var productNonApparel = ProductService.GetProductBySku("414581DHY0N1000");
//            var productApparel = ProductService.GetProductBySku("12345DHY0N1000");
//            var sellerOfferCases = new[]
//            {
//                new SellerOffer
//                {
//                    CompanyName = "Polka Inferno",
//                    UserName = "UlyssesPlulpigger@mejix.com",
//                    Offer = new Offer
//                    {
//                        OfferName = "Offer_By_[Polka Inferno]",
//                        OfferNumber = 90823983,
//                        StartDate = DateTime.Today,
//                        OfferStatus = OfferStatus.Published,
//                        OfferProducts = new[]
//                        {
//                            new OfferProduct
//                            {
//                                ProductId = productNonApparel.ID,
//                                OfferCost = 50,
//                                OfferProductSizes = new OfferProductSize[]
//                                {
//                                    new OfferProductSize
//                                    {
//                                        Quantity = 2,
//                                        Size =
//                                            new Size
//                                            {
//                                                Name = "44",
//                                                SizeType = new SizeType {Name = "T2"}
//                                            },
//                                    },
//                                    new OfferProductSize
//                                    {
//                                        Quantity = 3,
//                                        Size =
//                                            new Size
//                                            {
//                                                Name = "48",
//                                                SizeType = new SizeType {Name = "T2"}
//                                            },
//                                    }
//                                },
//                                Currency = currency,
//                            },
//                            new OfferProduct
//                            {
//                                ProductId = productApparel.ID,
//                                OfferCost = 60,
//                                OfferProductSizes = new OfferProductSize[]
//                                {
//                                    new OfferProductSize
//                                    {
//                                        Quantity = 4,
//                                        Size =
//                                            new Size
//                                            {
//                                                Name = "44",
//                                                SizeType = new SizeType {Name = "T2"}
//                                            },
//                                    },
//                                },
//                                Currency = currency,
//                            }
//                        },
//                        Currency = currency,
//                    }
//                },
//                new SellerOffer
//                {
//                    CompanyName = "Bernadina Enrico",
//                    UserName = "UlyssesPlulpigger@mejix.com",
//                    Offer = new Offer
//                    {
//                        OfferName = "Offer_By_[Bernadina Enrico]",
//                        OfferNumber = 90823989,
//                        StartDate = DateTime.Today,
//                        OfferStatus = OfferStatus.Published,
//                        OfferProducts = new[]
//                        {
//                            new OfferProduct
//                            {
//                                ProductId = productNonApparel.ID,
//                                OfferCost = 40,
//                                OfferProductSizes = new OfferProductSize[]
//                                {
//                                    new OfferProductSize
//                                    {
//                                        Quantity = 4,
//                                        Size =
//                                            new Size
//                                            {
//                                                Name = "44",
//                                                SizeType = new SizeType {Name = "T2"}
//                                            },
//                                    },
//                                    new OfferProductSize
//                                    {
//                                        Quantity = 5,
//                                        Size =
//                                            new Size
//                                            {
//                                                Name = "48",
//                                                SizeType = new SizeType {Name = "T2"}
//                                            },
//                                    }
//                                },
//                                Currency = currency,
//                            },
//                            new OfferProduct
//                            {
//                                ProductId = productApparel.ID,
//                                OfferCost = 70,
//                                OfferProductSizes = new OfferProductSize[]
//                                {
//                                    new OfferProductSize
//                                    {
//                                        Quantity = 8,
//                                        Size =
//                                            new Size
//                                            {
//                                                Name = "44",
//                                                SizeType = new SizeType {Name = "T2"}
//                                            },
//                                    },
//                                },
//                                Currency = currency,
//                            }
//                        },
//                        Currency = currency,
//                    }
//                }
//            };
//            return sellerOfferCases;
//        }

//        private Shipping GetCancelShipmentExpectedShipping()
//        {
//            var currency = new Currency
//            {
//                CurrencyId = 2,
//                Name = "USD",
//                Rate = 1,
//            };
//            return new Shipping
//            {
//                Currency = currency,
//                Shipments = new Shipment[]
//                {
//                    new Shipment
//                    {
//                        ApparelCost = (decimal) (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)),
//                        NonApparelCost = (decimal) ((2 + 3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)),
//                        Currency = currency,
//                        ShipsFromRegion = "Europe",
//                        ShipingIterval = "2 to 5 days",
//                        TotalShippingCost = (decimal) (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17) + (2 + 3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)),
//                        TotalStylesCount = 2,
//                        ShipmentItems = new ShipmentItem[]
//                        {
//                            new ShipmentItem
//                            {
//                                ProductName = "De Manta Clutch",
//                                BrandName = "ALEXANDER MCQUEEN",
//                                ProductMainImageName = "0a765a22-8e24-4fa5-9a12-8a93a825887e",
//                                IsApparel = false,
//                                ShipmentItemByBySellers = new[]
//                                {
//                                    new ShipmentItemBySeller
//                                    {
//                                        SellerAlias = "ONR13",
//                                        UnitOfferCost = (decimal) Math.Ceiling(50*Constants.CompanyFee),
//                                        UnitsCount = 2 + 3,
//                                        TotalOfferCost = (decimal) ((2 + 3)*Math.Ceiling(50*Constants.CompanyFee)),
//                                        Currency = currency,
//                                    },
//                                },
//                                TotalUnits = 2 + 3,
//                                TotalCost = (decimal) Math.Ceiling((2 + 3)*Math.Ceiling(50*Constants.CompanyFee)),
//                                ShippingCost = (decimal) ((2 + 3)*Math.Ceiling(Math.Ceiling(50*Constants.CompanyFee)*0.21)),
//                                Currency = currency,
//                            },
//                            new ShipmentItem
//                            {
//                                ProductName = "23armani canvaspure black armani",
//                                BrandName = "BALENCIAGA",
//                                IsApparel = true,
//                                ProductMainImageName = "1144e36e-ea91-4bf3-82c0-b03a6ca8b9f2",
//                                ShipmentItemByBySellers = new[]
//                                {
//                                    new ShipmentItemBySeller
//                                    {
//                                        SellerAlias = "ONR13",
//                                        UnitOfferCost = (decimal) Math.Ceiling(60*Constants.CompanyFee),
//                                        UnitsCount = 4,
//                                        TotalOfferCost = (decimal) (4*Math.Ceiling(60*Constants.CompanyFee)),
//                                        Currency = currency,
//                                    },
//                                },
//                                TotalUnits = 4,
//                                TotalCost = (decimal) (4*Math.Ceiling(60*Constants.CompanyFee)),
//                                ShippingCost = (decimal) (4*Math.Ceiling(Math.Ceiling(60*Constants.CompanyFee)*0.17)),
//                                Currency = currency,
//                            }
//                        }
//                    },
                   
//                },
//                TotalShippingCost =
//                    (decimal) (4 * Math.Ceiling(Math.Ceiling(60 *Constants.CompanyFee)*0.17)) + 
//                    (decimal) ((2 + 3) * Math.Ceiling(Math.Ceiling(50 *Constants.CompanyFee)*0.21)),
//            };
//        }

//        private Cart GetCancelShipmentExpectedCart()
//        {
//            var buyerCompanyId = GetBuyerCompanyId();
            
//            var productNonApparel = ProductService.GetProductBySku("414581DHY0N1000");
//            var productApparel = ProductService.GetProductBySku("12345DHY0N1000");
//            var currency = new Currency
//            {
//                CurrencyId = 2,
//                Name = "USD",
//                Rate = 1,
//            };
//            var cart = TestHelpers.ComputedTotalsCart(new Cart
//            {
//                BuyerCompanyId = buyerCompanyId,
//                ItemsCount = 2,
//                UnitsCount = 4 + (2 + 3),
//                //TotalCustomerCost = 5 * (decimal)(50 * Constants.CompanyFee) + 4 * (decimal)(60 * Constants.CompanyFee),
//                Currency = currency,
//                Items = new CartItem[]
//                {
//                     new CartItem
//                    {
//                        ProductId = productNonApparel.ID,
//                        ProductName = productNonApparel.Name,
//                        ProductMainImageName =productNonApparel.MainImageName,

//                        //BrandId = ,
//                        BrandName = productNonApparel.BrandName, //"",

//                        CategoryName = "CLUTCHES & EVENING BAGS",

//                        ProductSku = productNonApparel.SKU, //"",

//                        //MaterialId = ,
//                        MaterialCode = productNonApparel.MaterialName, //"",

//                        //ColorId = ,
//                        ColorName = productNonApparel.ColorName, //"",

//                        //SeasonId = ,
//                        SeasonName = productNonApparel.SeasonName, // "",

//                        AverageUnitCost = Math.Ceiling((decimal) (50*Constants.CompanyFee)),
//                        AverageUnitDiscountPercentage = 93,
//                        TotalCustomerCost = 5*(decimal) (Math.Ceiling(50*Constants.CompanyFee)),

//                        SellerUnits = new[]
//                        {
//                            new CartItemSellerUnit
//                            {
//                                CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                // in tests are ignored properties ending with Id
//                                SellerAlias = "ONR13",
//                                ShipsFromRegion = "Europe",
//                                UnitsCount = 5,
//                                CustomerUnitPrice = Math.Ceiling((decimal) (50*Constants.CompanyFee)),
//                                UnitShippingTaxPercentage = Constants.ShippingPercentageNonApparel,
//                                DiscountPercentage = 93,
//                                //TotalCustomerCost = (decimal) (50*Constants.CompanyFee)*5,
//                                IsTakeOffer = false,
//                                IsLineBuy = false,
//                                Active = true,
//                                SellerUnitSizes = new CartItemSellerUnitSize[]
//                                {
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 2,
//                                        AvailableQuantity = 2,
//                                        //public int SizeId = ,
//                                        SizeName = "44",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 3,
//                                        AvailableQuantity = 3,
//                                        //public int SizeId = ,
//                                        SizeName = "48",
//                                    },
//                                }
//                            },
//                        }
//                    },
//                    new CartItem
//                    {
//                        ProductId = productApparel.ID,
//                        ProductName = productApparel.Name,
//                        ProductMainImageName = productApparel.MainImageName,

//                        //BrandId = ,
//                        BrandName = productApparel.BrandName,

//                        CategoryName = "UNDERWEAR",

//                        ProductSku = productApparel.SKU,

//                        //MaterialId = ,
//                        MaterialCode = productApparel.MaterialName,// "cot",

//                        //ColorId = ,
//                        ColorName = productApparel.ColorName, //"blk",

//                        //SeasonId = ,
//                        SeasonName = productApparel.SeasonName,//"PRE SS17",

//                        AverageUnitCost = Math.Floor((decimal) (Math.Ceiling(60*Constants.CompanyFee))),
//                        AverageUnitDiscountPercentage = 53,
//                        TotalCustomerCost = 4*(decimal) (Math.Ceiling(60*Constants.CompanyFee)),

//                        SellerUnits = new[]
//                        {
//                            new CartItemSellerUnit
//                            {
//                                CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                // in tests are ignored properties ending with Id
//                                SellerAlias = "ONR13",
//                                ShipsFromRegion = "Europe",
//                                UnitsCount = 4,
//                                CustomerUnitPrice = Math.Ceiling((decimal) (60*Constants.CompanyFee)),
//                                UnitShippingTaxPercentage = Constants.ShippingPercentageApparel,
//                                DiscountPercentage = 53,
//                                //TotalCustomerCost = (decimal) (60*Constants.CompanyFee)*4,
//                                IsTakeOffer = false,
//                                IsLineBuy = false,
//                                Active = true,
//                                SellerUnitSizes = new CartItemSellerUnitSize[]
//                                {
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 4,
//                                        AvailableQuantity = 4,
//                                        //public int SizeId = ,
//                                        SizeName = "44",
//                                    },
//                                }
//                            },
//                        }
//                    },  
//                }
//            });
//            return cart;
//        }

//        private void Clean(int cartId, int[] offerIds)
//        {
//            if (offerIds != null)
//            {
//                foreach (var offerId in offerIds)
//                {
//                    OfferRepository.DeleteOffer(offerId);
//                }
//            }
//            if (cartId > 0)
//            {
//                CartRepository.PhysicalDeleteCart(cartId);
//            }
//        }

       
//    }
//}

