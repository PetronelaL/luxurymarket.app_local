﻿using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Models.Offer;

namespace Luxury.Integration.Tests
{
    public class RemoveProductScenario
    {
        public SellerOffer[] SellerOffers { get; set; }

        public RemoveProductData[] RemoveProducts { get; set; }

        public Cart ExpectedCart { get; set; }
        public RemoveProductData[] ExpectedRemovedProducts { get; set; }

        public class RemoveProductData
        {
            public string ProductSku { get; set; }
        }
    }
}