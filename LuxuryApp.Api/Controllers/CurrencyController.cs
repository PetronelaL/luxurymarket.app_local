﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Processors.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace LuxuryApp.Api.Controllers
{
    //[Authorize]
    [RoutePrefix("api/currencies")]
    public class CurrencyController : ApiController
    {
        private CurrencyService _currencyService;

        public CurrencyController()
        {
            _currencyService = new CurrencyService();
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            var entities = _currencyService.GetById(null);
            return Ok(entities);
        }


        [HttpGet]
        [Route("{currencyId:int}")]
        public IHttpActionResult GetById(int currencyId)
        {
            var countries = _currencyService.GetById(currencyId);
            return Ok(countries);
        }
    }
}
