﻿using System;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Api.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/states")]
    public class StateController : ApiController
    {
        private StateService _stateService;

        public StateController()
        {
            _stateService = new StateService();
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetAll()
        {
            var states = _stateService.Search(null, null);
            return Ok(states);
        }

        [HttpGet]
        [Route("{stateId:int}")]
        public IHttpActionResult GetById(int stateId)
        {
            var states = _stateService.Search(stateId, null);
            return Ok(states);
        }

        [HttpGet]
        [Route("country/{countryId:int}")]
        public IHttpActionResult GetByCountryId(int countryId)
        {
            var states = _stateService.Search(null, countryId);
            return Ok(states);
        }
    }
}