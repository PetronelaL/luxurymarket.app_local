﻿using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Extensions;
using LuxuryApp.Processors.Resources.Export;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/cart")]
    public class CartController : ApiController
    {
        private readonly ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [HttpPost] // it's POST because it's having some side effects.
        [Route("get_or_create_cart_summay_for_buyer")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<CartSummary>))]
        public IHttpActionResult GetCartSummaryForBuyer([FromUri] int buyerCompanyId)
        {
            return _cartService.GetCartSummaryForBuyer(buyerCompanyId)
                .ToHttpActionResult(this);
        }

        /// <summary>
        /// Gets cart for buyer company
        /// </summary>
        /// <param name="buyerCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("get_or_create_cart_for_buyer")]
        //[ResponseType(typeof(ApiResult<Cart>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<Cart>))]
        public IHttpActionResult GetCartForBuyer([FromUri] int buyerCompanyId)
        {
            return _cartService.GetCartForBuyer(buyerCompanyId).ToHttpActionResult(this);
        }

        [HttpPost]
        [Route("get_cart_preview")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<CartSplitPreview>))]
        public IHttpActionResult GetCartSplitPreview([FromUri] int buyerCompanyId)
        {
            return _cartService.GetCartSplitPreview(buyerCompanyId).ToHttpActionResult(this);
        }

        /// <summary>
        /// Adds and offer product size to cart.
        /// It may 
        ///    - add a single item if the product under offer is not "Line Buy" and the offer is not "Take all"
        ///    - add all item sizes for product id product under offer is "Line buy" and the offer is not "Take all"
        ///    - add all items under offer is offer is "Take all"
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Cart ID</returns>
        [HttpPost]
        [Route("add_offer_product_sizes")]
        //[ResponseType(typeof(ApiResult<int>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<CartOfferProductSummary>))]
        public IHttpActionResult AddOfferProductSizeToCart(SaveOfferProductSizesToCartModel model)
        {
            return
                _cartService.AddOfferProductSizeToCart(model)
                    .ToHttpActionResult(this);
        }

        /// <summary>
        /// Adds entire offer to cart, regardless if the offer is "Take all" or not.
        /// </summary>
        /// <returns>Cart ID</returns>
        [HttpPost]
        [Route("add_offer")]
        //[ResponseType(typeof(ApiResult<int>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<CartOfferProductSummary>))]
        public IHttpActionResult AddEntireOfferToCart(AddEntireOfferToCartModel model)
        {
            return _cartService.AddEntireOfferToCart(model).ToHttpActionResult(this);
        }

        /// <summary>
        /// Add products to cart
        /// </summary>
        /// <returns>Cart ID</returns>
        [HttpPost]
        [Route("add_products")]
        //[ResponseType(typeof(ApiResult<int>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<CartOfferProductSummary>))]
        public IHttpActionResult AddEntireProductsToCart(AddProductsToCartModel model)
        {
            return _cartService.AddProductsToCart(model).ToHttpActionResult(this);
        }

        /// <summary>
        /// Add all products to cart
        /// </summary>
        /// <returns>Cart ID</returns>
        [HttpPost]
        [Route("add_all_products")]
        //[ResponseType(typeof(ApiResult<int>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<CartOfferProductSummary>))]
        public IHttpActionResult AddAllProductsToCart(AddAllProductToCartModel model)
        {
            return _cartService.AddAllProductToCart(model).ToHttpActionResult(this);
        }

        /// <summary>
        /// Set cart item active/inactive flag
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("set_active_flag")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<SetCartItemActiveFlagResult>))]
        public IHttpActionResult SetCartItemSellerUnitSizeActiveFlag(SetCartItemActiveModel model)
        {
            return _cartService.SetCartItemSellerUnitSizeActiveFlag(model).ToHttpActionResult(this);
        }

        [HttpPost]
        [Route("update_items_quantities")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<UpdateCartItemsQuantitiesResult>))]
        public IHttpActionResult UpdateCartItemsQuantities(SaveOfferProductSizesToCartModel model)
        {
            return _cartService.UpdateCartItemsQuantities(model).ToHttpActionResult(this);
        }

        [HttpPost]
        [Route("empty_cart")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<CartSummary>))]
        public IHttpActionResult EmptyCartForBuyer([FromUri] int buyerCompanyId)
        {
            return _cartService.EmptyCartForBuyer(buyerCompanyId).ToHttpActionResult(this);
        }

        [HttpPost]
        [Route("remove_product_from_cart")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<RemoveProductFromCartResult>))]
        public IHttpActionResult RemoveProductsFromCart(RemoveProductsFromCartModel model)
        {
            return _cartService.RemoveProductsFromCart(model).ToHttpActionResult(this);
        }

        [HttpPost]
        [Route("remove_item_seller_unit_from_cart")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<RemoveCartItemSellerUnitResult>))]
        public IHttpActionResult RemoveItemSellerUnitFromCart(RemoveItemSellerUnitFromCartModel model)
        {
            return _cartService.RemoveItemSellerUnitFromCart(model).ToHttpActionResult(this);
        }

        [HttpGet]
        [Route("{cartId:int}/getTotalSummary")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(CartTotalSummary))]
        public IHttpActionResult GetCartTotalSummary(int cartId, bool onlyValidItems)
        {
            var paymentMethods = _cartService.GetTotalSummary(cartId, onlyValidItems);
            return Ok(paymentMethods);
        }

        [HttpGet]
        [Route("export_cart")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(System.Web.Mvc.FileResult))]
        public HttpResponseMessage ExportCart(int buyerCompanyId)
        {
            var excelFile = _cartService.ExportCartExcel(buyerCompanyId);
            string fileName = ExcelNames.CART_FILE_NAME + DateTimeOffset.Now.ToFileTime() + ExcelNames.EXPORT_EXTENSION;

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(excelFile.Data);
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.ms-excel");

            return response;
        }

        [HttpGet]
        [Route("{cartId:int}/getinvalidItems")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(CartItemValidatelCollection))]
        public IHttpActionResult GetInvalidItems(int cartId)
        {
            var paymentMethods = _cartService.GetInvalidItems(cartId);
            return Ok(paymentMethods);
        }

        [HttpPost]
        [Route("relockProduct")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(CartItemReservedCollection))]
        public IHttpActionResult RelockCartProduct(CartProductRequest model)
        {
            var paymentMethods = _cartService.RelockProduct(model.CartId, model.OfferProductId ?? 0);
            return Ok(paymentMethods);
        }

        [HttpPost]
        [Route("reservedTimes")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(CartItemReservedCollection))]
        public IHttpActionResult GetReservedTimes(int cartId)
        {
            var paymentMethods = _cartService.GetCartTimes(cartId);
            return Ok(paymentMethods);
        }
    }
}
