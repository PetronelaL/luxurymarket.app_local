﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Extensions;
using Swashbuckle.Swagger.Annotations;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("ordertracking")]
    public class OrderTrackingController : ApiController
    {
        private readonly IOrderTrackingService _orderTrackingService;

        public OrderTrackingController(IOrderTrackingService orderTrackingService)
        {
            _orderTrackingService = orderTrackingService;
        }

        [Route("get_x2_order")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(byte[]))]
        [SwaggerResponse((HttpStatusCode)422, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.Forbidden, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ApiResult))]
        public IHttpActionResult GetX2Order(int orderSellerId)
        {
            var poNumber = string.Empty;
            var r = _orderTrackingService.GetSellerOrderX2Document(orderSellerId,out poNumber);
            return r.ToHttpActionResult(this);
        }

        [Route("push_x2_order")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(byte[]))]
        [SwaggerResponse((HttpStatusCode) 422, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.Forbidden, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ApiResult))]
        public IHttpActionResult PushX2Order(int orderSellerId)
        {
            return _orderTrackingService.PushSellerOrderX2Document(orderSellerId).ToHttpActionResult(this);
        }
    }
}
