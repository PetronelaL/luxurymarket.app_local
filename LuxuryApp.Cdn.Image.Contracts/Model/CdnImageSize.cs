﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.Image.Contracts.Model
{
    public enum CdnImageDimension
    {
        Width = 1 << 0,
        Height = 1 << 1,
    }

    public class CdnImageSize
    {
        public CdnImageDimension Dimension { get; set; }
        public int Size { get; set; }
        public string SubFolder { get; set; }
    }
}
