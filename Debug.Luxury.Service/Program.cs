﻿using Autofac;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryMarket.Services.Vendor.AlducaSync;
using System;
using System.Threading;

namespace Debug.Luxury.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new DependencyBuilder().GetDependencyContainer();
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            var worker = container.Resolve<IWorker>();
            worker.Start(cancellationTokenSource.Token);

            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }
    }
}
