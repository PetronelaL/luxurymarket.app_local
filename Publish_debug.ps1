# valid versions are [2.0, 3.5, 4.0, 12.0, 14.0]
$dotNetVersion = "14.0"
$regKey = "HKLM:\software\Microsoft\MSBuild\ToolsVersions\$dotNetVersion"
$regProperty = "MSBuildToolsPath"

$msbuildExe = join-path -path (Get-ItemProperty $regKey).$regProperty -childpath "msbuild.exe"

&$msbuildExe LuxuryMarketApp.sln /p:DeployOnBuild=true /p:PublishProfile=Debug /m