(function () {
  'use strict';

  angular.module('luxurymarket.directives')
    .directive('scroll', [
      '$window',
      '$state',
      Scroll
    ]);

    function Scroll($window, $state) {
      return function(scope, element, attrs) {

        scope.lastScrollTop = 0;
        angular.element($window).bind("scroll", function() {
          if ($state.current.name === 'site.products-wrapper.list') {

            scope.st = this.pageYOffset;


            if (scope.st > scope.lastScrollTop) {
              element[0].classList.add('sticky-element');
              console.log('scroll top');
            } else {
              console.log('scroll down');
              element[0].classList.add('sticky-element');
            }
            // if (this.pageYOffset >= 30) {
            //   scope.boolChangeClass = true;
            //   //  console.log('Scrolled below header.');
            //   // if (element[0].classList.contains('sticky-element')) {
            //       element[0].classList.add('sticky-element');
            //   // }
            // } else {
            //   scope.boolChangeClass = false;
            //   //  console.log('Header is in view.');
            //   element[0].classList.remove('sticky-element');
            // }
            scope.lastScrollTop = scope.st;
            scope.$apply();
            console.log('pageYOffset ' , this.pageYOffset);
          }
        });
    };
    }

})(angular);
