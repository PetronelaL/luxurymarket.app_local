﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket', [
        'luxurymarket.components',
        'luxurymarket.services',
        'luxurymarket.directives',
        'luxurymarket.libs',
        'ui.router',
        'ui.select',
        'ngMessages',//
        'ngFileUpload',//
        'LocalStorageModule',
        'angular.filter',
        'pascalprecht.translate',//
        'ngCookies',//
        'ngSanitize',//
        'ngAnimate', //
        'ui.bootstrap.datetimepicker',
        'ui-notification',
        'ui.bootstrap',
        //'slick',
        'timer',

    ]);

})(window.angular);
