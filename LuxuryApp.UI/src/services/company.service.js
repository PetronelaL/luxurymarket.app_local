﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('CompanyService', CompanyService);

    CompanyService.$inject = ['$http', 'SERVICES_CONFIG'];

    function CompanyService($http, SERVICES_CONFIG) {

        var svc = {
            acceptTermsAndConditions: acceptTermsAndConditions
        };

        return svc;

        function acceptTermsAndConditions(companyId) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/companies/accept_terms_and_conditions/' + companyId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on accepting terms and conditions. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }
    }
})(window.angular);