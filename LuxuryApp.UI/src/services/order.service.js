﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('OrderService', OrderService);

    OrderService.$inject = ['$http', 'SERVICES_CONFIG', 'AuthenticationService', 'COMPANY_TYPES', 'localStorageService', '$q', 'Upload'];

    function OrderService($http, SERVICES_CONFIG, AuthenticationService, COMPANY_TYPES, localStorageService, $q, Upload) {

        var svc = {
            getAvailablePaymentMethods: getAvailablePaymentMethods,
            submit: submit,
            getAll: getAll,
            getOrderStatuses: getOrderStatuses,
            getOrderStatusesByType: getOrderStatusesByType,
            getDetails: getDetails,
            confirmUnits: confirmUnits,
            updateStatus: updateStatus,
            uploadSupportingDocument: uploadSupportingDocument,
            updateSupportingDocument: updateSupportingDocument,
            getDocuments: getDocuments,
            insertPackageInformation: insertPackageInformation,
            isSubmittedPackageInformation: isSubmittedPackageInformation
        };

        return svc;

        function uploadSupportingDocument(params) {
            return Upload.upload({
                url: SERVICES_CONFIG.apiUrl + '/api/orders/' + params.orderId + '/files/upload',
                file: params.file,
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error on document upload. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function updateSupportingDocument(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/orders/' + params.orderId + '/files', {
                id: params.fileId,
                orderId: params.orderId,
                deleted: params.deleted
            }).then(function (response) {
                if (response.data.code === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'There was an error updating the file. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function getAvailablePaymentMethods() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/orders/paymentMethods')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on payments load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function submit(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/orders/submit', {
                cartId: params.cartId,
                paymentMethodId: params.paymentMethodId
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to submit order. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to submit order. Please try again' };
            });
        }

        function getAll(params) {
            var apiCall = AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer ? '/api/orders/getAllForBuyer' : '/api/orders/getAllForSeller';

            return $http.post(SERVICES_CONFIG.apiUrl + apiCall, {
                "companyId": params.companyId,
                "statusTypeId": params.statusId
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to load orders. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to load orders. Please try again' };
            });
        }

        function getOrderStatuses() {
            var statuses = localStorageService.get('orderStatuses');
            if (statuses) {
                return $q.resolve({ success: true, data: statuses });
            }
            else {
                return $http.get(SERVICES_CONFIG.apiUrl + '/api/orders/orderStatusTypes')
                    .then(function (response) {
                        if (response.status === 200) {
                            localStorageService.set('orderStatuses', response.data);
                            return { success: true, data: response.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load statuses' };
                        }
                    }, function (response) {
                        return { success: false, error_message: 'Unable to load statuses' };
                    });
            }
        }

        function getOrderStatusesByType() {
            var statuses = localStorageService.get('orderStatusesByType' + AuthenticationService.getCurrentUserType());
            if (statuses) {
                return $q.resolve({ success: true, data: statuses });
            }
            else {
                return $http.get(SERVICES_CONFIG.apiUrl + '/api/orders/suborderStatusTypes')
                    .then(function (response) {
                        if (response.status === 200) {
                            localStorageService.set('orderStatusesByType' + AuthenticationService.getCurrentUserType(), response.data);
                            return { success: true, data: response.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load statuses' };
                        }
                    }, function (response) {
                        return { success: false, error_message: 'Unable to load statuses' };
                    });
            }
        }

        function getDetails(orderId) {
            var apiCall = AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer ? '/api/orders/getBuyerDetails/' : '/api/orders/getSellerDetails/';

            return $http.get(SERVICES_CONFIG.apiUrl + apiCall + orderId).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to load order details. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to load order details. Please try again' };
            });
        }

        function updateStatus(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/orders/updateStatus', params)
                .then(function (response) {
                    if (response.data.code === 200) {
                        return { success: true, data: response.data.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to update order status. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable to update order status. Please try again' };
                });
        }

        function confirmUnits(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/orders/confirmUnits', {
                "orderSellerProductId": params.orderSellerProductId,
                "confirmUnits": params.confirmUnits
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to confirm order units. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to confirm order units. Please try again' };
            });
        }

        function getDocuments(orderSellerId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/orders/' + orderSellerId + '/files')
                    .then(function (response) {
                        if (response.status === 200) {
                            return { success: true, data: response.data.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load files' };
                        }
                    }, function (response) {
                        return { success: false, error_message: 'Unable to load files' };
                    });
        }

        function insertPackageInformation(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/orders/confirmPackages', params)
               .then(function (response) {
                   if (response.status === 200) {
                       return { success: true, data: response.data.data };
                   }
                   else {
                       return { success: false, error_message: 'Unable to add pickup packages information. Please try again' };
                   }
               }, function (response) {
                   return { success: false, error_message: 'Unable to add pickup packages information. Please try again' };
               });
        }

        function isSubmittedPackageInformation(orderSellerId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/orders/isSubmittedPackageInformation/' + orderSellerId)
               .then(function (response) {
                   if (response.status === 200) {
                       return { success: true, data: response.data.data };
                   }
                   else {
                       return { success: false, error_message: 'Unable to check for packages information. Please try again' };
                   }
               }, function (response) {
                   return { success: false, error_message: 'Unable to check for packages information. Please try again' };
               });
        }

      }
})(window.angular);