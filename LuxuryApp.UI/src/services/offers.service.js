﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('OfferService', OfferService);

    OfferService.$inject = ['$http', 'Upload', 'SERVICES_CONFIG', 'AuthenticationService'];

    function OfferService($http, Upload, SERVICES_CONFIG, AuthenticationService) {

        var svc = {
            uploadNewOffer: uploadNewOffer,
            getItemDetails: getItemDetails,
            updateSettings: updateSettings,
            updateLineBuy: updateLineBuy,
            updateItem: updateItem,
            uploadSupportingDocument: uploadSupportingDocument,
            validateItem: validateItem,
            getSummary: getSummary,
            reportError: reportError,
            updateProductSize: updateProductSize,
            updateSupportingDocument: updateSupportingDocument,
            batchUpdateItems: batchUpdateItems,
            createOffer: createOffer,

            // published offers
            getOffers: getOffers,
            getStatuses: getStatuses,
            getLiveSummary: getLiveSummary,
            getProducts: getProducts,
            updateEndDate: updateEndDate,
            deleteOffer: deleteOffer,
            getSellerProducts: getSellerProducts,
            getSellerSizesForProduct: getSellerSizesForProduct,
            getCategories: getCategories,
            getSeasons: getSeasons,
            getSizesProductList: getSizesProductList
        };

        return svc;

        function uploadNewOffer(file) {
            return Upload.upload({
                url: SERVICES_CONFIG.apiUrl + '/api/offerImports/upload',
                file: file,
                headers: { 'CompanyID': AuthenticationService.getCurrentUserID() }
            }).then(function (response) {
                if (response.data.code === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: response.data.validationErrorMessages.$values[0] };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function getItemDetails(itemId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/offerImports/item/' + itemId)
                .then(function (response) {
                    if (response.data.code === 200) {
                        return { success: true, data: response.data.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on product load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function updateSettings(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offerImports', params).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error on product update. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function updateLineBuy(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offerImports/lineBuy', {
                "offerImportBatchId": params.offerId,
                "lineBuy": params.lineBuy,
                "ids": params.ids
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error on product update. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function updateItem(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offerImports/item', {
                "id": params.offerItemId,
                "active": params.active,
                "brandId": params.brandId,
                "colorId": params.colorId,
                "materialId": params.materialId,
                "offerCost": params.offerCost
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'There was an error on product update. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function validateItem(itemId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/offerImports/item/' + itemId + '/validate')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on product validate. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function uploadSupportingDocument(params) {
            return Upload.upload({
                url: SERVICES_CONFIG.apiUrl + '/api/offerImports/' + params.offerId + '/files/upload',
                file: params.file,
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error on document upload. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function updateSupportingDocument(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offerImports/' + params.offerImportBatchId + '/files/' + params.offerImportFileId, {
                "offerImportFileId": params.offerImportFileId,
                "offerImportBatchId": params.offerImportBatchId,
                "deleted": params.deleted,
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error updating the file. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function getSummary(offerId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/offerImports/' + offerId + '/summary')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error loading offer summary. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function reportError(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offerImports/' + params.offerImportBatchId + '/reportError', {
                "offerImportBatchId": params.offerImportBatchId,
                "offerImportBatchItemId": params.offerImportBatchItemId,
                "productId": params.productId,
                "brandId": params.brandId,
                "errorDescription": params.error,
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error reporting the error. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function updateProductSize(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offerImports/item/' + params.offerImportBatchItemId + '/sizes', {
                "offerImportBatchItemId": params.offerImportBatchItemId,
                "sizeId": params.sizeId,
                "quantity": params.quantity,
                "deleted": params.quantity === 0
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error updating the quantity. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function getOffers(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offers', {
                "page": params.page,
                "statusID": params.statusID,
                "keyword": params.keyword
            }, {
                headers: {
                    'CompanyID': AuthenticationService.getCurrentUserID()
                }
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error updating the quantity. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function getStatuses() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/offers/statuses')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error loading offer statuses. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function getLiveSummary() {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offers/livesummary', {
                "CompanyID": AuthenticationService.getCurrentUserID()
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error loading the summary. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function getProducts(offerNumber) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/offers/products/' + offerNumber)
               .then(function (response) {
                   if (response.status === 200) {
                       return { success: true, data: response.data };
                   }
                   else {
                       return { success: false, error_message: 'There was an error loading offer products. Please try again' };
                   }
               }, function (response) {
                   return { success: false, error_message: response.data.Message };
               });
        }

        function updateEndDate(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offers/update_end_date', params)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on offer update. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function deleteOffer(offerId) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offers/delete?offerId=' + offerId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on offer delete. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function batchUpdateItems(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offerImports/batch_update_item', params)
               .then(function (response) {
                   if (response.status === 200) {
                       return { success: true, data: response.data.data };
                   }
                   else {
                       return { success: false, error_message: 'There was an error on offer delete. Please try again' };
                   }
               }, function (response) {
                   return { success: false, error_message: response.data.Message };
               });
        }

        function getSellerProducts(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offers/getSellerProducts', {
                pageNumber: params.page,
                sellerID: params.sellerID,
                brands: params.brands,
                categories: params.categories,
                seasons: params.seasons,
                keyword: params.keyword
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error loading seller products. Please try again' };
                }
            }, function (reject) {
                return { success: false, error_message: reject.data.Message };
            });
        }


        function getSellerSizesForProduct(sellerProductId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/offers/getSellerSizesForProduct?sellerProductId=' + sellerProductId)
              .then(function (response) {
                  if (response.status === 200) {
                      return { success: true, data: response.data };
                  }
                  else {
                      return { success: false, error_message: 'There was an error loading seller sizes for product. Please try again' };
                  }
              }, function (reject) {
                  return { success: false, error_message: reject.data.Message };
              });
        }

        function getCategories() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/hierarchy/categories')
              .then(function (response) {
                  if (response.status === 200) {
                      return { success: true, data: response.data };
                  }
                  else {
                      return { success: false, error_message: 'There was an error loading categories. Please try again' };
                  }
              }, function (reject) {
                  return { success: false, error_message: reject.data.Message };
              });
        }

        function getSeasons() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/hierarchy/seasons')
              .then(function (response) {
                  if (response.status === 200) {
                      return { success: true, data: response.data };
                  }
                  else {
                      return { success: false, error_message: 'There was an error loading seasons. Please try again' };
                  }
              }, function (reject) {
                  return { success: false, error_message: reject.data.Message };
              });
        }

        function createOffer(products, excludedProducts, areAllSelected, search, priceSettings, companyId) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offers/createOffer?companyId=' + companyId,
                {
                    products: products,
                    excludedProducts: excludedProducts,
                    areAllSelected: areAllSelected,
                    search: search,
                    priceSettings: priceSettings
                }).then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error loading seasons. Please try again' };
                    }
                }, function (reject) {
                    return { success: false, error_message: reject.data.Message };
                });
        }


        function getSizesProductList(companyId, offerProductId) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/offers/sizes', {
              companyID: companyId,
              offerProductID: offerProductId
            })
              .then(function (response) {
                  if (response.status === 200) {
                      return { success: true, data: response.data };
                  }
                  else {
                      return { success: false, error_message: 'There was an error loading sizes in product listing. Please try again' };
                  }
              }, function (reject) {
                  return { success: false, error_message: reject.data.Message };
              });
        }
    }
})(window.angular);
