﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('AddressService', AddressService);

    AddressService.$inject = ['$http', 'SERVICES_CONFIG'];

    function AddressService($http, SERVICES_CONFIG) {

        var svc = {
            getByCompanyId: getByCompanyId,
            getByAddressId: getByAddressId,
            saveInfo: saveInfo,
            setMain: setMain,
            getMainAddresses: getMainAddresses
        };

        return svc;

        function getByCompanyId(companyId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/address/byCompanyId/' + companyId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on product load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function getByAddressId(addressId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/address/byaddressId/' + addressId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on product load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function saveInfo(model) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/address/saveInfo', model)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data.data };
                    }
                    else {
                        return { success: false, error_message: 'Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.exceptionMessage };
                });
        }

        function setMain(addressId) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/address/setMain?addressId=' + addressId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true };
                    }
                    else {
                        return { success: false, error_message: 'Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.exceptionMessage };
                });
        }

        function getMainAddresses(companyId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/address/' + companyId + '/main')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }
    }
})(window.angular);