﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site', [
        'luxurymarket.components.site.home',
        'luxurymarket.components.site.product',
        'luxurymarket.components.site.cart',
        'luxurymarket.components.site.shipment',
        'luxurymarket.components.site.menu',
        'luxurymarket.components.site.featuredevent',
        'luxurymarket.components.site.ordersummary',
        'luxurymarket.components.site.brand'
    ]);

})(window.angular);