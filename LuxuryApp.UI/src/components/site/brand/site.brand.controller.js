﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.brand')
        .controller('BrandsController', BrandsController);

    BrandsController.$inject = ['$state', '$stateParams', 'AttributesService', 'Notification', 'PRODUCT_LIST_TYPE'];

    function BrandsController($state, $stateParams, AttributesService, Notification, PRODUCT_LIST_TYPE) {
        var vm = this;

        // view models
        vm.list = [];

        // functions
        vm.viewProducts = viewProducts;

        // init
        _init();

        // functions implementations
        ///////////////////////////////////////
        function viewProducts(brand) {
            $state.go('site.products-wrapper.list', {
                brandId: brand.id,
                header: brand.name,
                listType: PRODUCT_LIST_TYPE.OTHER
            });
        }

        //private functions
        ///////////////////////////////////////
        function _init() {
            AttributesService.getBrands().then(function (response) {
                if (response.success) {
                    var brands = _.sortBy(response.data, 'name');
                    // split brands in 4 columns
                    var brandsPerColumn = (response.data.length / 4);
                    _.each(brands, function (value, index) {
                        var column = parseInt(index / brandsPerColumn);
                        vm.list[column] = vm.list[column] || [];
                        vm.list[column].push(value);
                    });
                }
                else {
                    Notification("Unable to load brands");
                }
            });
        }

    }
})(window.angular);