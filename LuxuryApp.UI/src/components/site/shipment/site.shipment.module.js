﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.shipment', []);

})(window.angular);