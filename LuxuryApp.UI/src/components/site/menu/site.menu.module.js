﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.menu', []);

})(window.angular);