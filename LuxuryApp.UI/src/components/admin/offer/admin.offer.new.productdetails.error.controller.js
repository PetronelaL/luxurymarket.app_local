﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferProductErrorController', NewOfferProductErrorController);

    NewOfferProductErrorController.$inject = ['_', '$stateParams', '$state', 'OfferService', 'Notification'];

    function NewOfferProductErrorController(_, $stateParams, $state, OfferService, Notification) {
        var vm = this;

        if ($stateParams.uniqueId === null) {
            $state.go('admin.new-offer.upload');
        }

        // view models
        vm.productDetails = $stateParams.productDetails;
        vm.issue = null;
        vm.isLoading = false;

        // functions
        vm.back = back;
        vm.submitError = submitError;

        function back($event, type) {
            $event.preventDefault();
            switch (type) {
                case 1:
                    $state.go('admin.new-offer.details', $stateParams);
                    break;
                case 2:
                    $state.go('admin.new-offer.product-details', $stateParams);
                    break;
            }
        }

        function submitError() {
            vm.isLoading = true;
            OfferService.reportError({
                offerImportBatchId: $stateParams.offerId,
                offerImportBatchItemId: vm.productDetails.offerImportBatchItemId,
                productId: vm.productDetails.productId,
                brandId: vm.productDetails.brandId,
                error: vm.issue
            }).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    Notification('Error reported with success');
                    $state.go('admin.new-offer.details', $stateParams);
                }
                else {
                    Notification(response.error_message);
                }
            });
        }

    }

})(window.angular);