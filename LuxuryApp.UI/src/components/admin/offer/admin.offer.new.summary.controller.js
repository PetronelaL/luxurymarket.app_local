﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferSummaryController', NewOfferSummaryController);

    NewOfferSummaryController.$inject = ['$scope', '_', '$stateParams', '$state', 'OfferService', 'OFFER_STATUS_TYPES', 'CURRENCY'];

    function NewOfferSummaryController($scope, _, $stateParams, $state, OfferService, OFFER_STATUS_TYPES, CURRENCY) {
        var vm = this;

        if ($stateParams.uniqueId === null) {
            $state.go('admin.new-offer.upload');
        }

        // update steps
        $scope.$emit('updateSteps', { number: 4, complete: false, active: true });

        // view models
        vm.agreeTC = false;
        vm.summary = null;
        vm.isLoading = false;

        // functions
        vm.back = back;
        vm.publish = publish;

        _init();

        function back($event) {
            $event.preventDefault();
            $state.go('admin.new-offer.documents', $stateParams);
        }

        function publish($event) {
            vm.isLoading = true;
            OfferService.updateSettings({
                id: vm.summary.offerImportBatchId,
                offerStatus: OFFER_STATUS_TYPES.PendingApproval
            }).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    $state.go('admin.new-offer.published');
                }
                else {
                    // show error
                }
            });
        }

        // private functions
        /////////////////////////////////////////////
        function _init() {
            OfferService.getSummary($stateParams.offerId)
                .then(function (response) {
                    if (response.success) {
                        vm.summary = response.data;
                        vm.summary.currency = CURRENCY[vm.summary.currencyID];
                        vm.summary.offerImportBatchId = $stateParams.offerId;
                        vm.summary.startDateFormat = moment(vm.summary.startDate).format('MM/DD/YYYY, h:mm:ss a');
                        if (vm.summary.endDate) {
                            vm.summary.endDateFormat = moment(vm.summary.endDate).format('MM/DD/YYYY, h:mm:ss a');
                        }
                        else {
                            vm.summary.endDateFormat = 'Not specified';
                        }
                    }
                    else {
                        // display error
                    }
                });
        }
    }

})(window.angular);