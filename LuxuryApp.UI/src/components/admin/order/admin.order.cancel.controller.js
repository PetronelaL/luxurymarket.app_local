﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.order')
        .controller('CancelOrderController', CancelOrderController);

    CancelOrderController.$inject = ['$rootScope', '$uibModalInstance', 'Notification', 'order', 'OrderService', 'ORDER_STATUS_TYPES', 'canceledBy', 'ORDER_CUSTOMER_TYPE', '$state', '$translatePartialLoader'];

    function CancelOrderController($rootScope, $uibModalInstance, Notification, order, OrderService, ORDER_STATUS_TYPES, canceledBy, ORDER_CUSTOMER_TYPE, $state, $translatePartialLoader) {
        var vm = this;

        // view models
        vm.isLoading = false;
        vm.question = null;
        vm.translationData = null;

        // functions
        vm.close = close;
        vm.submit = submit;

        // init
        $translatePartialLoader.addPart('order-cancel');

        if (canceledBy === ORDER_CUSTOMER_TYPE.Buyer) {
            vm.title = 'CANCEL_ORDER_TITLE';
            vm.question = 'CANCEL_ORDER_QUESTION';
            vm.translationData = {
                orderPONumber: order.poNumber,
                sellerAlias: order.sellerAlias
            };
        }
        else {
            vm.title = 'REJECT_ORDER_TITLE';
            vm.question = 'REJECT_ORDER_QUESTION';
            vm.translationData = {
                orderPONumber: order.poNumber,
                buyerAlias: order.buyerAlias
            };
        }

        // functions      
        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function submit($event) {
            vm.isLoading = true;
            var status = null;
            if (canceledBy === ORDER_CUSTOMER_TYPE.Buyer) {
                status = ORDER_STATUS_TYPES.CANCEL;
            }
            else {
                status = ORDER_STATUS_TYPES.REJECT;
            }

            OrderService.updateStatus({
                "orderSellerId": order.orderSellerId,
                "statusTypeId": status,
                "orderCustomerType": canceledBy
            }).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    close();

                    if (canceledBy === ORDER_CUSTOMER_TYPE.Buyer) {
                        $rootScope.$broadcast('updateOrderListAfterAction', order, status);
                    }
                    else {
                        $state.go('admin.orders');
                    }

                    Notification('Order canceled with success');
                }
                else {
                    Notification('There was an error. Please try again.');
                }
            });
        }
    }
})(window.angular);