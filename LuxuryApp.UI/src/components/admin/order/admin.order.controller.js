﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.order')
        .controller('OrdersController', OrdersController);

    OrdersController.$inject = ['COMPANY_TYPES', 'AuthenticationService', 'OrderService', '$state', 'ORDER_STATUS_TYPES', 'SERVICES_CONFIG'];

    function OrdersController(COMPANY_TYPES, AuthenticationService, OrderService, $state, ORDER_STATUS_TYPES, SERVICES_CONFIG) {
        var vm = this;

        // view models
        vm.isLoading = false;
        vm.isLoadingFilters = false;
        vm.showStatusFilters = false;
        vm.details = null;
        vm.statuses = null;
        vm.selectedStatus = {
            id: null,
            name: 'FILTER_BY_STATUS'
        };
        vm.isBuyer = AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer;

        // init
        _getOrders();

        // functions
        /////////////////////////////////
        vm.openFilterByStatus = openFilterByStatus;
        vm.applyFilter = applyFilter;
        vm.viewDetails = viewDetails;

        function openFilterByStatus($event) {
            if (!vm.showStatusFilters) {
                // load filters

                vm.isLoadingFilters = true;
                OrderService.getOrderStatusesByType()
                    .then(function (response) {
                        vm.isLoadingFilters = false;
                        if (response.success) {
                            vm.showStatusFilters = true;
                            response.data.splice(0, 0, {
                                id: null,
                                name: 'All'
                            });
                            vm.statuses = response.data;
                        }
                        else {
                            // show error
                        }
                    });
            }
        }

        function applyFilter(status) {
            if (vm.selectedStatus.id !== status.id) {
                vm.selectedStatus = status;
                _getOrders();
            }
        }

        function viewDetails($event, order) {
            $event.preventDefault();
            if (AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer) {
                $state.go('admin.order-details', {
                    orderId: order.orderId
                });
            }
            else {
                $state.go('admin.order-details', {
                    orderId: order.orderSellerId
                });
            }
        }

        // private functions
        /////////////////////////////////
        function _getOrders() {
            var params = {
                companyId: AuthenticationService.getCurrentUserID()
            };

            if (vm.selectedStatus.id !== null) {
                params.statusId = vm.selectedStatus.id;
            }

            vm.isLoading = true;
            OrderService.getAll(params).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    vm.details = { items: response.data.items.$values };
                    _.forEach(vm.details.items, function (item) {
                        var sellerOrBuyerName = '';

                        if (AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer) {
                            var sellersAlias = [],
                                orderStatuses = [];
                            _.forEach(item.subOrders, function (so) {
                                sellersAlias.push(so.sellerAlias);
                                orderStatuses.push(so.orderSellerPONumber + '-' + so.statusTypeName);
                            });
                            sellerOrBuyerName = sellersAlias.join(',');

                            item.orderStatusTypeName = orderStatuses.join('. ');
                            //item.styles = item.totalStyles;
                            //item.units = item.totalUnits;

                            item.exportLink = SERVICES_CONFIG.apiUrl + '/api/orders/export_order?orderId=' + item.orderId + '&isBuyer=' + vm.isBuyer + '&token=' + AuthenticationService.getCurrentUserInfo().token;
                        }
                        else {
                            sellerOrBuyerName = item.buyerAlias;
                            item.orderStatusTypeName = item.statusTypeName;

                            var initialValues = item.statusTypeId === ORDER_STATUS_TYPES.PENDING_CONFIRMATION ||
                                                  item.statusTypeId === ORDER_STATUS_TYPES.CANCEL ||
                                                  item.statusTypeId === ORDER_STATUS_TYPES.REJECT;

                            //item.styles = initialValues ? item.totalStyles : item.confirmedTotalStyles;
                            //item.units = initialValues ? item.totalUnits : item.confirmedTotalUnits;

                            item.exportLink = SERVICES_CONFIG.apiUrl + '/api/orders/export_order?orderId=' + item.orderSellerId + '&isBuyer=' + vm.isBuyer + '&token=' + AuthenticationService.getCurrentUserInfo().token;
                        }

                        item.orderCDate = new Date(item.createdDate);
                        item.sellerOrBuyerName = sellerOrBuyerName;
                    });
                }
                else {

                }
            });
        }

    }

})(window.angular);