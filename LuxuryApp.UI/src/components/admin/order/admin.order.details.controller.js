﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.order')
        .controller('OrderDetailsController', OrderDetailsController);

    OrderDetailsController.$inject = ['COMPANY_TYPES', 'AuthenticationService', 'OrderService', '$stateParams', '$state', '_', 'ORDER_STATUS_TYPES', 'Notification', '$uibModal', '$scope', 'ORDER_CUSTOMER_TYPE'];

    function OrderDetailsController(COMPANY_TYPES, AuthenticationService, OrderService, $stateParams, $state, _, ORDER_STATUS_TYPES, Notification, $uibModal, $scope, ORDER_CUSTOMER_TYPE) {
        var vm = this;

        if (_.isUndefined($stateParams.orderId) || $stateParams.orderId.length === 0) {
            $state.go('admin.dashboard');
            return false;
        }

        // view models
        vm.details = {
            orderSellerList: []
        };
        vm.isBuyer = AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer;

        // init
        _load();

        // events
        $scope.$on('updateOrderListAfterAction', _updateOrderListAfterAction);

        // functions
        vm.showProducts = showProducts;
        vm.confirmOrderSeller = confirmOrderSeller;
        vm.confirmOrderBuyer = confirmOrderBuyer;
        vm.confirmUnits = confirmUnits;
        vm.cancelOrder = cancelOrder;
        vm.goToUploadDocuments = goToUploadDocuments;
        vm.goToPickupInformation = goToPickupInformation;

        function showProducts(order) {
            if (_.isUndefined(order.viewProducts)) {
                order.viewProducts = false;
            }

            order.viewProducts = !order.viewProducts;
        }

        function confirmOrderSeller($event, order) {
            $event.preventDefault();

            var hasChanges = false;
            _.forEach(order.productItems, function (product) {
                _.forEach(product.sizeItems, function (size) {
                    if (size.displayUnits !== size.units) {
                        hasChanges = true;
                        return false;
                    }
                });
            });

            var status = null;
            if (hasChanges) {
                status = ORDER_STATUS_TYPES.PARTIAL_CONFIRMATION;
            }
            else {
                status = ORDER_STATUS_TYPES.CONFIRM_PENDING_PAYMENT;
            }


            order.isLoading = true;
            OrderService.updateStatus({
                "orderSellerId": order.orderSellerId,
                "statusTypeId": status,
                "orderCustomerType": ORDER_CUSTOMER_TYPE.Seller
            }).then(function (response) {
                order.isLoading = false;
                if (response.success) {
                    $state.go('admin.order-files', {
                        orderId: order.orderSellerId,
                        oc: true
                    });
                }
                else {
                    Notification('There was an error. Please try again');
                }
            });
        }

        function goToUploadDocuments($event, order) {
            $state.go('admin.order-files', {
                orderId: order.orderSellerId,
                oc: false
            });
        }

        function confirmOrderBuyer($event, order) {
            $event.preventDefault();

            var status = ORDER_STATUS_TYPES.CONFIRM_PENDING_PAYMENT;
            order.isLoading = true;
            OrderService.updateStatus({
                "orderSellerId": order.orderSellerId,
                "statusTypeId": status,
                "orderCustomerType": ORDER_CUSTOMER_TYPE.Buyer
            }).then(function (response) {
                order.isLoading = false;
                if (response.success) {
                    Notification('Order confirmed with success');
                    _updateOrderListAfterAction($event, order, status);
                }
                else {
                    Notification('There was an error. Please try again');
                }
            });
        }

        function confirmUnits($event, product, order) {
            $event.preventDefault();

            var validUnits = true;
            _.forEach(product.sizeItems, function (size) {
                var form = vm['productSizeForm' + size.orderSellerItemId]; // jshint ignore:line
                if (form.$invalid) {
                    validUnits = false;
                    return false;
                }
            });

            if (validUnits) {
                var units = [], orderSellerProductId = null;
                _.forEach(product.sizeItems, function (size) {
                    units.push({
                        id: size.orderSellerItemId,
                        unit: parseInt(size.displayUnits)
                    });

                    if (orderSellerProductId === null) {
                        orderSellerProductId = size.orderSellerProductId;
                    }
                });

                OrderService.confirmUnits({
                    orderSellerProductId: orderSellerProductId,
                    confirmUnits: units
                }).then(function (response) {
                    if (response.success) {
                        product.totalProductCost = response.data.totalCost;
                        product.totalUnits = response.data.totalUnits;
                        Notification('Units confirmed with success');
                    }
                    else {
                        Notification('There was an error. Please try again');
                    }
                });
            }
            else {
                $event.stopPropagation();
                Notification('Please insert valid units to confirm');
            }
        }

        function cancelOrder($event, order, canceledByBuyer) {
            $event.preventDefault();
            $event.stopPropagation();

            var canceledBy = null;
            if (canceledByBuyer) {
                canceledBy = ORDER_CUSTOMER_TYPE.Buyer;
            }
            else {
                canceledBy = ORDER_CUSTOMER_TYPE.Seller;
            }

            $uibModal.open({
                templateUrl: 'admin/order/order-confirm-cancel.html',
                controller: 'CancelOrderController',
                controllerAs: 'co',
                resolve: {
                    order: [function () {
                        return order;
                    }],
                    canceledBy: [function () {
                        return canceledBy;
                    }]
                }
            });
        }

        function goToPickupInformation($event, order) {
            $state.go('admin.order-pickup-information', {
                orderId: order.orderSellerId
            });
        }

        // private functions
        function _load() {
            OrderService.getDetails($stateParams.orderId)
                .then(function (response) {
                    if (response.success) {
                        if (vm.isBuyer) {
                            vm.details = response.data;
                        }
                        else {
                            vm.details.orderSellerList = [response.data];
                        }

                        _.forEach(vm.details.orderSellerList, function (o) {
                            o.orderCDate = new Date(o.createdDate);

                            if ((o.statusTypeId === ORDER_STATUS_TYPES.PENDING_CONFIRMATION || o.statusTypeId === ORDER_STATUS_TYPES.PARTIAL_CONFIRMATION) && vm.isBuyer) {
                                o.canCancel = true;
                            }
                            else {
                                o.canCancel = false;
                            }

                            if (o.statusTypeId === ORDER_STATUS_TYPES.PARTIAL_CONFIRMATION && vm.isBuyer) {
                                o.canReConfirm = true;
                            }
                            else {
                                o.canReConfirm = false;
                            }

                            if (o.statusTypeId === ORDER_STATUS_TYPES.REJECT || o.statusTypeId === ORDER_STATUS_TYPES.PARTIAL_CONFIRMATION || o.statusTypeId === ORDER_STATUS_TYPES.CANCEL || o.statusTypeId === ORDER_STATUS_TYPES.CONFIRM_PENDING_PAYMENT) {
                                o.hideActions = true;
                            }
                            else {
                                o.hideActions = false;
                            }

                            if (vm.isBuyer) {
                                o.totalLC = o.totalLandedCost;

                            }
                            else {
                                o.totalLC = o.totalProductCost;
                            }

                            if (!vm.isBuyer && o.statusTypeId === ORDER_STATUS_TYPES.START_SHIPPING) {
                                o.canGoToPickupInformation = true;
                            }
                            else {
                                o.canGoToPickupInformation = false;
                            }

                            _.forEach(o.productItems, function (product) {
                                if (o.statusTypeId === ORDER_STATUS_TYPES.PARTIAL_CONFIRMATION && vm.isBuyer) {
                                    if (product.totalUnits !== product.orderedTotalUnits) {
                                        product.isChanged = true;
                                    }
                                    else {
                                        product.isChanged = false;
                                    }
                                }
                                else {
                                    product.isChanged = false;
                                }

                                _.forEach(product.sizeItems, function (size) {
                                    if (o.statusTypeId === ORDER_STATUS_TYPES.PENDING_CONFIRMATION && vm.isBuyer) {
                                        size.unitsToDisplay = size.units;
                                    }
                                    else {
                                        size.unitsToDisplay = size.displayUnits;

                                        if (size.confirmedUnits !== size.units) {
                                            size.isChanged = true;
                                        }
                                        else {
                                            size.isChanged = false;
                                        }
                                    }
                                });
                            });
                        });
                    }
                    else {
                        // show error
                    }
                });
        }

        function _updateOrderListAfterAction($event, order, status) {
            var orderToUpdate = _.findWhere(vm.details.orderSellerList, { orderId: order.orderId });
            orderToUpdate.statusTypeId = status;
            orderToUpdate.canCancel = false;
            orderToUpdate.canReConfirm = false;

            if (orderToUpdate.statusTypeId === ORDER_STATUS_TYPES.REJECT || orderToUpdate.statusTypeId === ORDER_STATUS_TYPES.PARTIAL_CONFIRMATION || orderToUpdate.statusTypeId === ORDER_STATUS_TYPES.CANCEL || orderToUpdate.statusTypeId === ORDER_STATUS_TYPES.CONFIRM_PENDING_PAYMENT) {
                orderToUpdate.hideActions = true;
            }
            else {
                orderToUpdate.hideActions = false;
            }

            OrderService.getOrderStatusesByType()
                    .then(function (response) {
                        if (response.success) {
                            var myStatus = _.findWhere(response.data, { id: status });
                            orderToUpdate.statusTypeName = myStatus.name;
                        }
                        else {
                            // show error
                        }
                    });
        }
    }

})(window.angular);