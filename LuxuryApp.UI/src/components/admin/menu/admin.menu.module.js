﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.menu', [
        'ui.router'
    ]);

})(window.angular);