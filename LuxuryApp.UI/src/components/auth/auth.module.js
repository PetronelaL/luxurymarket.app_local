﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth', [
        'luxurymarket.components.auth.register',
        'luxurymarket.components.auth.login',
        'luxurymarket.components.auth.forgotpassword',
        'luxurymarket.components.auth.message',
        'luxurymarket.components.auth.resetpassword',
        'luxurymarket.components.auth.staticcontent',
        'luxurymarket.components.auth.customerservice',
        'ui.router'
    ]);

})(window.angular);