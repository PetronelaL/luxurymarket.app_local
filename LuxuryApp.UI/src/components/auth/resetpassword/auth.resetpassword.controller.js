﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.resetpassword')
        .controller('ResetPasswordController', ResetPasswordController);

    ResetPasswordController.$inject = ['$state', '$stateParams', 'AuthenticationService', 'COMPANY_TYPES', 'TokenValidationResponseType', 'TokenPurpose'];

    function ResetPasswordController($state, $stateParams, AuthenticationService, COMPANY_TYPES, TokenValidationResponseType, TokenPurpose) {
        var vm = this;

        if (!$stateParams.code) {
            $state.go('auth.login');
        }

        // view models
        vm.setupPasswordModel = {
            id: $stateParams.id,
            email: null,
            password: null,
            confirmPassword: null,
            code: $stateParams.code
        };
        vm.isLoading = false;
        vm.isResetPassword = $stateParams.new.toLowerCase() === 'false';

        vm.isCheckingToken = true;
        vm.showTokenValidationMessage = false;
        vm.tokenValidationStatus = TokenValidationResponseType.Valid;
        vm.TokenValidationResponseType = TokenValidationResponseType;

        // functions
        vm.submit = submit;
        vm.validateField = validateField;

        _validateToken();

        function submit() {
            vm.isLoading = true;
            AuthenticationService.resetPassword(vm.setupPasswordModel).then(function (response) {
                if (response.success) {
                    AuthenticationService.getToken({
                        "userName": vm.setupPasswordModel.email,
                        "password": vm.setupPasswordModel.password,
                        "rememberMe": true
                    }).then(function (response) {
                        vm.isLoading = false;
                        if (response.success) {
                            if (COMPANY_TYPES.Buyer === response.data.Companies[0].CompanyType) {
                                $state.go('site.home');
                            }
                            else {
                                $state.go('admin.new-offer.upload');
                            }
                        }
                        else {
                            $state.go('auth.login');
                        }
                    });
                }
                else {
                    vm.isLoading = false;
                    vm.hasError = true;
                    vm.error_message = response.error_message;
                }
            });
        }

        function validateField(fieldName) {
            if (vm.setupPasswordForm[fieldName].$invalid && vm.setupPasswordForm[fieldName].$dirty) {
                return true;
            }

            if (vm.setupPasswordForm.$submitted && vm.setupPasswordForm[fieldName].$invalid) {
                return true;
            }

            return false;
        }

        function _validateToken() {
            vm.isCheckingToken = true;
            AuthenticationService.validateToken($stateParams.id,
                $stateParams.code, TokenPurpose.ResetPassword)
                .then(function (response) {
                    vm.isCheckingToken = false;
                    vm.tokenValidationStatus = response.data.type;
                    if (response.success) {
                        vm.setupPasswordModel.email = response.data.email;
                    }
                });
        }
    }
})(window.angular);
