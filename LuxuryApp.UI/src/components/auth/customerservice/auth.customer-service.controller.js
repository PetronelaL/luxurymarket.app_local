﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.customerservice')
        .controller('CustomerServiceController', CustomerServiceController);

    CustomerServiceController.$inject = ['$state', 'AuthenticationService', 'Notification', 'HomeService'];

    function CustomerServiceController($state, AuthenticationService, Notification, HomeService) {
        var vm = this;

        // private vars
        vm.isLoading = false;
        vm.isLoadingRequest = false;
        vm.topicList = [{
            id: null,
            name: 'Loading....'
        }];
        vm.showConfirm = false;
        vm.customerServiceId = null;

        // view models
        vm.customerServiceModel = {};
        vm.customerServiceModel.topicID = null;

        // functions
        vm.addRequest = addRequest;

        // init
        _loadTopics();

        //events captured

        // functions
        function addRequest() {
            if (vm.customerServiceForm.$invalid) {
                return;
            }
            vm.isLoadingRequest = true;
            HomeService.addCustomerServiceRequest(vm.customerServiceModel)
                .then(function (response) {
                    vm.isLoadingRequest = false;
                    if (response.success) {
                        vm.showConfirm = true;
                        vm.customerServiceId = response.data;
                        vm.customerServiceModel = {};
                        vm.customerServiceModel.topicID = null;
                        vm.customerServiceForm.$setPristine();
                        Notification('Customer Service Request was sent with success.');
                    }
                    else {
                        Notification('There was an error. Please try again.');
                    }
                });
        }

        // private functions
        function _loadTopics() {
            vm.isLoading = true;
            HomeService.getTopicList()
               .then(function (response) {
                   vm.isLoading = false;
                   if (response.success) {
                       response.data.splice(0, 0, {
                           topicID: null,
                           name: 'PLEASE SELECT TOPIC*'
                       });
                       vm.topicList = response.data;
                   }
                   else {
                       // display error
                   }
               });
        }
    }
})(window.angular);