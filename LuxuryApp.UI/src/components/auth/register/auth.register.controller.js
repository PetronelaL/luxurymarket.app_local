﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.register')
        .controller('RegisterController', [
          'AuthenticationService',
          'localStorageService',
          '$state',
          'MESSAGE_TYPES',
          'LocationService',
          '$scope',
          '$window',
          '_',
          RegisterController
        ]);


    function RegisterController(AuthenticationService, localStorageService, $state, MESSAGE_TYPES, LocationService, $scope, $window, _) {
        var vm = this;

        // variables

        // view model
        vm.requestModel = {
            companyType: 1,
            contactName: null,
            phoneNumber: null,
            email: null,
            companyName: null,
            streetAddress: null,
            city: null,
            stateId: null,
            zipCode: null,
            countryId: null,
            briefDescription: null,
        };

        // view variables
        vm.isLoading = false;
        vm.hasError = false;
        vm.error_message = null;
        vm.countries = null;
        vm.states = null;
        // functions
        vm.requestNewAccount = requestNewAccount;
        vm.updateStates = updateStates;
        vm.findCountry = findCountry;
        vm.validateField = validateField;

        // init
        _init();
        function findCountry(countries, iso2CodeStr) {
          return _.findWhere(countries, { isO2Code: iso2CodeStr});
        }

        function validateField(fieldName) {
          if (vm.registerForm[fieldName].$invalid && vm.registerForm[fieldName].$dirty) {
            return true;
          }

          if (vm.registerForm.$submitted && vm.registerForm[fieldName].$invalid) {
            return true;
          }

          return false;
        }

        $scope.$on('gPlaceComponents', function (evt, data) {
          vm.requestModel.streetAddress = '';
          vm.requestModel.locality = '';
          vm.requestModel.zipCode = '';
          vm.requestModel.stateId = null;
          vm.requestModel.countryId = null;
          _.each(data.addressComponents, function (component) {
            switch (component.types[0]) {
              case 'street_number':
                vm.requestModel.streetAddress = component.long_name + ', ';
                break;
              case 'route':
                vm.requestModel.streetAddress += component.long_name;
                break;
              case 'locality':
                vm.requestModel.city = component.long_name;
                break;
              case 'postal_code':
                vm.requestModel.zipCode = component.long_name;
                break;
              case 'administrative_area_level_1':
                _.each(vm.countries, function (country) {
                  if (_.has(country, 'states')) {
                    var found = _.findWhere(country.states, { stateName: component.long_name});
                    if (found !== undefined) {
                      vm.states = country.states;
                      vm.requestModel.stateId = found.stateId;
                    }
                  }
                });
                break;
              case 'country':
                var cf = _.findWhere(vm.countries, { isO2Code: component.short_name });
                if (cf && cf.id) {
                  vm.requestModel.countryId = cf.id;
                }
                break;
              default:
                break;

            }
          });

          $scope.$apply();
        });

        function requestNewAccount() {
            if (vm.registerForm.$invalid) {
                return;
            }

            vm.isLoading = true;
            localStorageService.clearAll();
            AuthenticationService.requestNewAccount(vm.requestModel).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    $state.go('auth.message', {
                        type: MESSAGE_TYPES.RequestSent
                    });
                }
                else {
                    vm.hasError = true;
                    vm.error_message = response.error_message;
                }
            });
        }

        function updateStates(s) {
            vm.states = _.findWhere(vm.countries, { id: s}).states;
            vm.requestModel.stateId = null;
        }


        // private functions
        function _init() {
            LocationService.getCountries().then(function (response) {
                if (response.success) {
                  vm.countries = response.data;
                  var defaultCountry = vm.findCountry(vm.countries, 'US');
                  vm.states = defaultCountry.states;
                  vm.requestModel.countryId = defaultCountry.id;
                }
                else {
                    vm.countries = [{
                        id: null,
                        countryName: 'Country'
                    }];
                }
            });
        }
    }
})(window.angular);
