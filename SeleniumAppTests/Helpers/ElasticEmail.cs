﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.PageObjects;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SeleniumAppTests.PageObjects.authentication;

namespace SeleniumAppTests.Helpers
{
    public class ElasticEmail : BasePage
    {
        #region Web Elements Identifiers

        private String _emailID = "eusername";
        private String _passwordID = "epassword";
        private String _signinID = "esigninbutton";
        private String _reportsXpath = "/html/body/div[1]/div/div[2]/nav[1]/ul/li[5]/span";
        private String _emaillogsXpath = "/html/body/div[1]/div/div[2]/nav[1]/ul/li[5]/ul/li[1]/span";
        private String _viewEmailXpath = "/html/body/div[1]/div/div[2]/div/div/div[5]/div/div/div/div[1]/div/table/tbody/tr[1]/td[6]/span/i";
        private String _searchEmailID = "esearchcampaign";
        private String _searchEmailFieldID = "esearchinputcampaign";
        private String _returnedEmailResultsID = "econtactsList";
        private String _emailViewXpath = "//tbody//..[contains(text(), 'Your Request Was Approved')]/..[contains(@class, 'text-center')]/../td//..//i[contains(@class, 'fa fa-eye')]";
        private String _emailDetailClass = "eemaildetail";
        private String _openNewWindowXpath = "//*[contains(text(), 'View in New Window')]";
        private String _setPassXpath = "//*[contains(text(), 'START SHOPPING')]";

        #endregion

        #region Web Elements

        IWebElement email;
        IWebElement password;
        IWebElement signin;

        #endregion

        public ElasticEmail(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_signinID)));
            initWebElements();
        }

        public void loginElastic(RemoteWebDriver driver)
        {
            email.SendKeys("tech@luxurymarket.com");
            password.SendKeys("LuxuryMarket$1");
            signin.Click();

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_reportsXpath)));
        }

        public void reportsClick(RemoteWebDriver driver)
        {
            WebDriverHelper.Driver.FindElementByXPath(_reportsXpath).Click();

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_emaillogsXpath)));
        }

        public void emailLogsClick(RemoteWebDriver driver)
        {
            WebDriverHelper.Driver.FindElementByXPath(_emaillogsXpath).Click();

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_viewEmailXpath)));
        }

        public void searchEmail(string value)
        {
            WebDriverHelper.Driver.FindElementById(_searchEmailID).Click();
            WebDriverHelper.Driver.FindElementById(_searchEmailFieldID).SendKeys(value);
            WebDriverHelper.Driver.FindElementById(_searchEmailFieldID).SendKeys(Keys.Enter);
            WebDriverHelper.Driver.FindElementById(_returnedEmailResultsID).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_emailDetailClass), value));
        }

        public NewAccSetPasswordPage openSetPasswordPage(string value)
        {
            var openemail = WebDriverHelper.Driver.FindElementsByXPath(_emailViewXpath).ToList();
            openemail.Count();

            if (openemail.Count > 0)
            {
                WebDriverHelper.Driver.FindElementByXPath(_emailViewXpath).Click();
            }
            else
            {
                int counts = 0;
                while(true && counts < 7)
                {
                    counts++;
                    WebDriverHelper.Driver.FindElementById(_searchEmailID).Click();
                    WebDriverHelper.Driver.FindElementById(_searchEmailFieldID).SendKeys(value);
                    WebDriverHelper.Driver.FindElementById(_searchEmailFieldID).SendKeys(Keys.Enter);
                    WebDriverHelper.Driver.FindElementById(_returnedEmailResultsID).Click();

                    openemail = WebDriverHelper.Driver.FindElementsByXPath(_emailViewXpath).ToList();
                    openemail.Count();

                    if (openemail.Count > 0)
                    {
                        WebDriverHelper.Driver.FindElementByXPath(_emailViewXpath).Click();
                        break;
                    }

                    Thread.Sleep(10000);
                }
            }

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_openNewWindowXpath)));

            PopupWindowFinder finder = new PopupWindowFinder(WebDriverHelper.Driver);
            string popupWindowHandle = finder.Click(WebDriverHelper.Driver.FindElementByXPath(_openNewWindowXpath));

            WebDriverHelper.Driver.SwitchTo().Window(popupWindowHandle);

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_setPassXpath)));

            WebDriverHelper.Driver.FindElementByXPath(_setPassXpath).Click();

            return new NewAccSetPasswordPage(WebDriverHelper.Driver);
        }

        public void viewEmailsClick(RemoteWebDriver driver)
        {
            WebDriverHelper.Driver.FindElementByXPath(_viewEmailXpath).Click();
            Thread.Sleep(2000);
        }

        public void hiperlinkClick(RemoteWebDriver driver)
        {
            //WebDriverHelper.Driver.FindElementByPartialLinkText(_hiperlinkPartialTxt).Click();

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("submitSetupAccount")));
        }

        public void switchWindow(RemoteWebDriver driver)
        {
            string currentHandle = driver.CurrentWindowHandle;

            PopupWindowFinder finder = new PopupWindowFinder(driver);
            string popupWindowHandle = finder.Click(WebDriverHelper.Driver.FindElementById("emodal-View in New Window"));

            driver.SwitchTo().Window(popupWindowHandle);
        }



        private void initWebElements()
        {
            email = WebDriver.FindElementById(_emailID);
            password = WebDriver.FindElementById(_passwordID);
            signin = WebDriver.FindElementById(_signinID);
        }

    }
}
