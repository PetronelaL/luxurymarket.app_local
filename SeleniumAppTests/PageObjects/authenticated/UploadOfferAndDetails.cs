﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.authenticated.home;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class UploadOfferAndDetails : BasePage
    {
        #region Web Elements Identifiers

        private String _browseFileClass = "drag-and-drop-zone";
        private String _offersInfoClass = "table";
        private String _productTableClass = "details-table";
        private String _startDateID = "startDateDropdown";
        private String _endDateID = "endDateDropdown";
        private String _calendarStartDateClass = "datetimepicker";
        private String _calendarEndDateXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/ui-view/div[2]/div/div[2]/div[1]/form/div[2]/ul/div";
        private String _continueBtnXpath = "//*[contains(text(), 'Continue')]";
        private String _unitsXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/ui-view/div[4]/div/div/table/tbody/tr[1]/td[6]/span";
        private String _totalCostXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/ui-view/div[4]/div/div/table/tbody/tr[1]/td[7]/span";
       
        #endregion

        #region Web Elements

        private IWebElement browseFile;

        #endregion

        public UploadOfferAndDetails(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_browseFileClass)));
            InitWebElements();
        }

        public void uploadOffer()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_browseFileClass))); 

            WebDriverHelper.Driver.FindElementByClassName(_browseFileClass).Click();
            Thread.Sleep(1000);
            SendKeys.SendWait(AppConfig.uploadOffer);
            Thread.Sleep(1000);
            SendKeys.SendWait(@"{Enter}");

            wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_productTableClass)));
        }

        public void setStartDate()
        {
            WebDriverHelper.Driver.FindElementById(_startDateID).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_calendarStartDateClass)));

            var currentDate = DateTime.Now;

            var calendarWebElement = WebDriverHelper.Driver.FindElementByClassName(_calendarStartDateClass);
            var presentdayElement = calendarWebElement.FindElement(By.XPath(".//td[not(contains(@class, 'past')) and text() = " + currentDate.Day + "]"));
            presentdayElement.Click();                     

            //var hoursToSet = currentDate.ToString("h:00 tt");
            //var hourElement = calendarWebElement.FindElement(By.XPath("//span[contains(@class, 'hour') and text() = '" + hoursToSet + "']"));
            //hourElement.Click();

            //var minutesToSet = currentDate.ToString("h:20 tt");
            //var minuteElement = calendarWebElement.FindElement(By.XPath("//span[contains(@class, 'minute') and text() = '" + minutesToSet + "']"));
            //minuteElement.Click();
        }

        public void setEndDate()
        {
            WebDriverHelper.Driver.FindElementById(_endDateID).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_calendarEndDateXpath)));

            var futureDate = DateTime.Today.AddDays(+1);

            var calendarWebElement = WebDriverHelper.Driver.FindElementByXPath(_calendarEndDateXpath);
            var futuredayElement = calendarWebElement.FindElements(By.XPath(".//td[contains(@class, 'future') and text() = " + futureDate.Day + "]")).ToList().Find(e => e.Displayed);
            futuredayElement.Click();
 
            //var hoursToSet = currentDate.ToString("h:00 tt");
            //var hourElement = calendarWebElement.FindElement(By.XPath("//span[contains(@class, 'hour') and text() = '" + hoursToSet + "']"));
            //hourElement.Click();

            //var minutesToSet = currentDate.ToString("h:30 tt");
            //var minuteElement = calendarWebElement.FindElement(By.XPath("//span[contains(@class, 'minute') and text() = '" + minutesToSet + "']"));
            //minuteElement.Click();
        }

        public void getUnitsAndCost(out int units, out double totalcost)
        {
            units = int.Parse(WebDriverHelper.Driver.FindElementByXPath(_unitsXpath).Text);
            totalcost = double.Parse(WebDriverHelper.Driver.FindElementByXPath(_totalCostXpath).Text);
        }

        public SupportingDocsPage continueClick()
        {
            WebDriverHelper.Driver.FindElementByXPath(_continueBtnXpath).Click();

            return new SupportingDocsPage();
        }

        private void InitWebElements()
        {
            browseFile = WebDriverHelper.Driver.FindElementByClassName(_browseFileClass);
        }
    }
}
