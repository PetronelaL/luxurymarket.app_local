﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class BaseSitePage : BasePage
    {

        public SiteHeader Header { get; set; }
        public SiteFooter Footer { get; set; }

        //constructor
        public BaseSitePage(RemoteWebDriver driver) : base(driver)
        {
            //var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            //wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_headerID)));
            initWebElements(driver);
        }

        public void initWebElements(RemoteWebDriver driver)
        {
            Header = new SiteHeader(driver);
        }
    }

}

