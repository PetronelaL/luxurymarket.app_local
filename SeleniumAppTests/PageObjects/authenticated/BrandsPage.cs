﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class BrandsPage
    {
        #region Web Element Identifiers

        private String _chooseBrandXpath = "//*[contains(text(), 'qa brand')]";
        private String _categoriesFilterXpath = "//*[contains(text(), 'Categories')]";
        private String _womensCategoryXpath = "//*[contains(text(), 'Womens')]";

        #endregion

        #region Web Elements

        #endregion

        /// <summary>
        /// Selects a specific brand
        /// </summary>
        /// <returns></returns> Returns an instance of the products page of the selected brand
        public ProductBrandsPage clickSellerBrand()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var brand = WebDriverHelper.Driver.FindElementsByXPath(_chooseBrandXpath).ToList();
            brand.Count();

            try
            {
                if (brand.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_womensCategoryXpath)));
                }
                else
                {
                    WebDriverHelper.Driver.FindElementByXPath(_chooseBrandXpath).Click();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Brands is empty or failed to load" + ex.Message, ex);
            }

            return new ProductBrandsPage();
        }
    }
}
