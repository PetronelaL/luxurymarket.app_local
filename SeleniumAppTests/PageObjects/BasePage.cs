﻿using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects
{
    public class BasePage
    {
        private RemoteWebDriver _webDriver;
        protected RemoteWebDriver WebDriver
        {
            get
            {
                return _webDriver;
            }
        }

        public BasePage(RemoteWebDriver webDriver)
        {
            if (WebDriver == null)
            {
                _webDriver = webDriver;
            }
        }
    }
}
