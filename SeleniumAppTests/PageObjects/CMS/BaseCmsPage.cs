﻿using OpenQA.Selenium.Remote;
using SeleniumAppTests.PageObjects.CMS.authenticated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.CMS
{
    public class BaseCmsPage
    {
        public CmsHeader CmsMenu { get; set; }

        //constructor
        public BaseCmsPage(RemoteWebDriver driver)
        {
            initWebElements(driver);
        }

        public void initWebElements(RemoteWebDriver driver)
        {
            CmsMenu = new CmsHeader(driver);
        }
    }
}
