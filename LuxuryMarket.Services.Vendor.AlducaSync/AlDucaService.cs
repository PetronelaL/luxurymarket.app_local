﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using NLog;
using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace LuxuryMarket.Services.Vendor.AlducaSync
{
    public partial class AlDucaService : ServiceBase
    {
       private readonly IWorker _worker;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public AlDucaService(IWorker worker)
        {
            InitializeComponent();

            _worker = worker;
        }

        protected override void OnStart(string[] args)
        {
            _worker.Start(_cancellationTokenSource.Token);
        }

        protected override void OnStop()
        {
            _cancellationTokenSource.Cancel();
        }
    }
}
