﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Enums;

namespace LuxuryMarket.Services.Vendor.AlducaSync.Implementations
{
    public class SyncWorker : IWorker
    {
        private readonly IWorkerConfig _workerConfig;
        private readonly ISellerProductSyncAgent _sellerProductSyncAgent;
        private readonly ISellerApiSettingService _sellerApiSettingService;
     
        private Task _workerTask = null;

        public SyncWorker(ISellerProductSyncAgent sellerProductSyncAgent,
                          ISellerApiSettingService sellerApiSettingService,
                          IWorkerConfig workerConfig)
        {
            _sellerProductSyncAgent = sellerProductSyncAgent;
            _sellerApiSettingService = sellerApiSettingService;
            _workerConfig = workerConfig;
        }

        public void Start(CancellationToken cancellationToken)
        {
            if (_workerTask != null && _workerTask.Status == TaskStatus.Running)
            {
                return;
            }
            _workerTask = Task.Run(() => Worker(cancellationToken));
        }

        private async Task Worker(CancellationToken cancellationToken)
        {
            var settings = await _sellerApiSettingService.GetSellerApiSettings(SellerAPISettingType.AlDuca);

            for (;;)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    throw new TaskCanceledException();
                }
                try
                {
                    await _sellerProductSyncAgent.ProcessProducts(settings,Guid.NewGuid());
                }
                catch (Exception ex)
                {
                    throw;//todo
                }
                Task.Delay(_workerConfig.DelaySeconds * 1000, cancellationToken).Wait();
            }
        }
    }
}
