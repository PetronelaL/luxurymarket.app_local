﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.BackgroudServices;

namespace Luxury.Ftp.Implementations
{
    public class WorkerConfigAppSettings: IWorkerConfig
    {
        public int DelaySeconds
        {
            get
            {
                int value = 0;
                if (!int.TryParse(ConfigurationManager.AppSettings["WorkerPeriodSeconds"] ?? "", out value))
                {
                    value = 10;
                };
                return value;
            }
        }

    }
}
