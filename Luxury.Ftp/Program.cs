﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Autofac;

namespace Luxury.Ftp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (System.Environment.UserInteractive)
            {
                string parameter = string.Concat(args);
                switch (parameter)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new string[]
                            {Assembly.GetExecutingAssembly().Location});
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new string[]
                            {"/u", Assembly.GetExecutingAssembly().Location});
                        break;
                }
            }
            else
            {
                var container = new DependencyBuilder().GetDependencyContainer();
                ServiceBase[] servicesToRun = new[] { container.Resolve<ServiceFtp>() };
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}
