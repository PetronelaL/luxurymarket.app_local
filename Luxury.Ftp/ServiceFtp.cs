﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LuxuryApp.Contracts.BackgroudServices;
using Luxury.Ftp.Implementations;
using LuxuryApp.Contracts.Agents;
using NLog;

namespace Luxury.Ftp
{
    public partial class ServiceFtp : ServiceBase
    {
        #region Private Vars

        private readonly IWorkerConfig _workerConfig;
        private readonly IFtpUploadAgent _ftpUploadAgent;
        private Thread _workerThread = null;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        #endregion

        public ServiceFtp(IFtpUploadAgent ftpUploadAgent, IWorkerConfig workerConfig)
        {
            InitializeComponent();

            _ftpUploadAgent = ftpUploadAgent;
            _workerConfig = workerConfig;

            // settings
            this.ServiceName = "Luxury Market EDI FTP Transfer";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;
        }

        protected override void OnStart(string[] args)
        {
            _logger.Info("Thread started");
            ThreadStart starter = new ThreadStart(DoWork);
            _workerThread = new Thread(starter);
            _workerThread.Start();
        }

        public void DoWork()
        {
            // Main Loop
            while (true)
            {
                try
                {
                    _logger.Info("Starting upload from queue");
                    _ftpUploadAgent.UploadFromQueue();
                    _logger.Info("Finished upload from queue");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
                
                Thread.Sleep(new TimeSpan(0, 0, _workerConfig.DelaySeconds));
            }
        }
    }
}
