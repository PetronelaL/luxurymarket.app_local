﻿using System.Data;

namespace AlducaServiceProcessor
{
    public class Program
    {
        static void Main(string[] args)
        {
            var result = GetProducts();

        }
        public static DataTable GetProducts()
        {
            var service = new AlducaServiceReference.ServicesSoapClient();
            var result = service.GetSku4Platform("LuxuryMarket", "++Lux=Mrk++");  //set MaxReceivedMessageSize 
            return result;
        }
    }
}
