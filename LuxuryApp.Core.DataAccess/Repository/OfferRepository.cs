﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Models.Offer;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.DataAccess;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class OfferRepository : RepositoryBase,
        IOfferRepository
    {
        public OfferRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public Offer GetOfferById(int offerId)
        {
            Offer result;

            var taskGetOfferHeader = TaskGetOfferHeaderByOfferId(offerId);
            var taskGetOfferProducts = TaskGetOfferProductsByOfferId(offerId);
            var taskGetOfferProductSizes = TaskGetOfferProductSizesByOfferId(offerId);
            result = MapOffer(taskGetOfferHeader.Result, taskGetOfferProducts.Result,
                taskGetOfferProductSizes.Result);

            return result;
        }

        private Task<Offer> TaskGetOfferHeaderByOfferId(int offerId)
        {
            return Task.Run(() => GetOfferHeaderByOfferId(offerId));
        }

        private Offer GetOfferHeaderByOfferId(int offerId)
        {
            const string sql =
                @"select
                    [OfferId] as Id
                    ,[OfferNumber]
                    ,[OfferName]
                    ,[TakeAll]
                    ,[StartDate]
                    ,[EndDate]
                    ,[SellerID]
                    ,[CompanyID] as SellerCompanyId
	                ,[OfferStatusID]
                    ,OfferStatus
                    ,CurrencyId
                    ,CurrencyName as Name
                    ,CurrencyRate as Rate
                        from dbo.[fn_GetOfferHeaderByOfferId] (@offerId)";
            Offer result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<Offer, Currency, Offer>(sql,
                    (offer, currency) =>
                    {
                        offer.Currency = currency;
                        return offer;
                    }, splitOn: "Id, CurrencyId", param: new { offerId })
                    .SingleOrDefault();
                conn.Close();
            }
            return result;
        }

        private Task<OfferProduct[]> TaskGetOfferProductsByOfferId(int offerId)
        {

            return Task.Run(() => GetOfferProductsByOfferId(offerId));
        }

        private OfferProduct[] GetOfferProductsByOfferId(int offerId)
        {
            const string sql =
                @"select
                [OfferProductID] as Id
                  ,[OfferId]
                  ,[ProductID] as ProductId
                  ,[TotalQuantity]
                  ,[OfferCost]
                  ,[Discount]
                  ,[CustomerUnitPrice]
                  ,[LineBuy]
                  ,[CurrencyID] as Id
                  ,CurrencyName as Name
                  ,CurrencyRate as Rate
                        from dbo.fn_GetOfferProductsByOfferId(@offerId)";

            OfferProduct[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<OfferProduct, Currency, OfferProduct>(sql,
                    (offerProduct, currency) =>
                    {
                        offerProduct.Currency = currency;
                        return offerProduct;
                    },
                    new { offerId })
                    .ToArray();
                conn.Close();
            }
            return result;
        }

        private Task<OfferProductSize[]> TaskGetOfferProductSizesByOfferId(int offerId)
        {
            return Task.Run(() => GetOfferProductSizesByOfferId(offerId));
        }

        private OfferProductSize[] GetOfferProductSizesByOfferId(int offerId)
        {

            const string sql =
                @"select  
                OfferProductSizeID as Id
                ,OfferProductId
                ,[Quantity]
                ,SizeName
	            from dbo.[fn_GetOfferProductSizesByOfferId](@offerId)";
            OfferProductSize[] result;

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<OfferProductSize>(sql,new { offerId }).ToArray();
                conn.Close();
            }
            return result;
        }

        private Offer MapOffer(Offer offer, OfferProduct[] offerProducts, OfferProductSize[] offerProductSizes)
        {
            foreach (var op in offerProducts)
            {
                op.OfferProductSizes = offerProductSizes
                    .Where(x => x.OfferProductId == op.Id)
                    .ToArray();
            }
            offer.OfferProducts = offerProducts
                .Where(x => x.OfferId == offer.Id)
                .ToArray();
            return offer;
        }

        public bool IsOfferTakeAll(int offerId)
        {
            return GetOfferHeaderByOfferId(offerId)?.TakeAll ?? false;
        }

        public bool IsOfferTakeAllForOfferProductSizeId(int offerProductSizeId)
        {
            const string sql =
                "select TakeAll " +
                "   from dbo.fn_OfferProductSize_GetBusinessFlagsForCart(@offerProductSizeId)";
            bool result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<bool>(sql, new { offerProductSizeId });
                conn.Close();
            }
            return result;
        }

        public bool IsProductLineBuyForOfferProductSizeId(int offerProductSizeId)
        {
            const string sql =
                "select LineBuy " +
                "   from dbo.fn_OfferProductSize_GetBusinessFlagsForCart(@offerProductSizeId)";
            bool result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<bool>(sql, new { offerProductSizeId });
                conn.Close();
            }
            return result;
        }

        public int GetOfferProductIdForOfferProductSizeId(int offerProductSizeId)
        {
            const string sql =
                "select OfferProductID " +
                "  from dbo.fn_GetOfferProductSizeIdAncestors(@offerProductSizeId)";
            int result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<int>(sql, new { offerProductSizeId });
                conn.Close();
            }
            return result;
        }

        public int GetOfferIdForOfferProductSizeId(int offerProductSizeId)
        {
            const string sql =
                "select OfferID " +
                "  from dbo.fn_GetOfferProductSizeIdAncestors(@offerProductSizeId)";
            int result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<int>(sql, new { offerProductSizeId });
                conn.Close();
            }
            return result;
        }

        public Task<OfferProductSizeIdAncestors[]> TaskGetOfferProductSizeIdsAncestors(int[] offerProductSizeIds)
        {
            return
                Task<OfferProductSizeIdAncestors[]>.Factory.StartNew(() =>
                {
                    const string sql =
                        @"select OfferProductSizeId
                        ,OfferProductId
                        ,OfferId 
                            from dbo.fn_GetOfferProductSizeIdsAncestors(@offerProductSizeIds)";
                    var param = offerProductSizeIds.ToIdList();
                    OfferProductSizeIdAncestors[] result;
                    using (var conn = GetReadOnlyConnection())
                    {
                        conn.Open();
                        result = conn.Query<OfferProductSizeIdAncestors>(sql, new { offerProductSizeIds = param }).ToArray();
                        conn.Close();
                    }
                    return result;
                });
        }

        public Task<OfferProductSizeCartBusinessFlags[]> TaskGetOfferProductSizeCartBusinessFlags(int[] offerProductSizeIds)
        {
            return Task<OfferProductSizeCartBusinessFlags[]>.Factory.StartNew(() =>
            {
                const string sql =
                    @"select 
                        OfferProductSizeId
	                    ,TakeAll
	                    ,LineBuy
                        from dbo.fn_OfferProductSizes_GetBusinessFlagsForCart(@offerProductSizeIds)";
                OfferProductSizeCartBusinessFlags[] result;
                using (var conn = GetReadOnlyConnection())
                {
                    conn.Open();
                    var param = offerProductSizeIds.ToIdList();
                    result = conn.Query<OfferProductSizeCartBusinessFlags>(sql, new { offerProductSizeIds = param }).ToArray();
                    conn.Close();
                }
                return result;
            });
        }

        public int SaveOffer(Offer offer, int createdBy)
        {
            if (offer.Id == 0)
            {
                return InsertOffer(offer, createdBy);
            }
            throw new NotImplementedException();
        }

        public async Task<bool> IsActiveOffer(int offerId)
        {
            var query = $"Select OfferId,IsActiveOffer from  {SqlFunctionName.fn_OfferIsActive}(@OfferId)";
            var isOfferActive = false;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var result = await conn.QueryAsync<Tuple<int, bool>>(query, new { offerId });
                conn.Close();

                isOfferActive = result.Select(x => x.Item2).FirstOrDefault();
            }

            return isOfferActive;
        }

        private int InsertOffer(Offer offer, int createdBy)
        {
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                var tran = conn.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    InsertOffer(conn, offer, createdBy, tran);
                    InsertOfferProducts(conn, offer.Id, offer.OfferProducts, createdBy, tran);

                    tran.Commit();
                }
                catch (Exception)
                {
                    tran.Rollback();
                    throw;
                }
                conn.Close();
            }
            return offer.Id;
        }

        private void InsertOffer(IDbConnection conn,
            Offer offer,
            int createdBy,
            IDbTransaction transaction)
        {
            const string spName = "dbo.Offer_Insert";
            DynamicParameters parameters = new DynamicParameters(new
            {
                offerNumber = offer.OfferNumber,
                offerName = offer.OfferName,
                offerStatusId = (int)offer.OfferStatus,
                takeAll = offer.TakeAll,
                startDate = offer.StartDate,
                endDate = offer.EndDate,
                currencyId = offer.Currency?.CurrencyId,
                sellerCompanyId = offer.SellerCompanyId,
                createdBy,
            });
            parameters.Add("offerId", DbType.Int32, direction: ParameterDirection.ReturnValue);

            conn.Execute(spName, parameters, commandType: CommandType.StoredProcedure, transaction: transaction);

            offer.Id = parameters.Get<int>("offerId");
        }

        private void InsertOfferProducts(IDbConnection conn,
            int offerId,
            OfferProduct[] offerOfferProducts,
            int createdBy,
            IDbTransaction transaction)
        {
            foreach (var offerOfferProduct in offerOfferProducts)
            {
                offerOfferProduct.OfferId = offerId;
                InsertOfferProduct(conn, offerOfferProduct, createdBy, transaction);
            }
        }

        private void InsertOfferProduct(IDbConnection conn,
            OfferProduct offerOfferProduct,
            int createdBy,
            IDbTransaction transaction)
        {
            const string spName =
                "dbo.OfferProduct_Insert";

            var parameters = new DynamicParameters(
                new
                {
                    offerId = offerOfferProduct.OfferId,
                    productId = offerOfferProduct.ProductId,
                    offerCost = offerOfferProduct.OfferCost,
                    lineBuy = offerOfferProduct.LineBuy,
                    currencyId = offerOfferProduct.Currency?.CurrencyId,
                    createdBy,
                });
            parameters.Add("offerProductId", direction: ParameterDirection.ReturnValue);

            conn.Execute(spName, parameters, transaction, commandType: CommandType.StoredProcedure);
            offerOfferProduct.Id = parameters.Get<int>("offerProductId");

            InsertOfferProductSizes(conn, offerOfferProduct.Id, offerOfferProduct.OfferProductSizes, createdBy, transaction);
        }

        private void InsertOfferProductSizes(IDbConnection conn, int offerProductId, OfferProductSize[] offerProductSizes, int createdBy, IDbTransaction transaction)
        {
            foreach (var offerProductSize in offerProductSizes)
            {
                offerProductSize.OfferProductId = offerProductId;
                InsertOfferProductSize(conn, offerProductSize, createdBy, transaction);
            }
        }

        private void InsertOfferProductSize(IDbConnection conn, OfferProductSize offerProductSize, int createdBy, IDbTransaction transaction)
        {
            //const string spName = "dbo.OfferProductSize_Insert";

            //var parameters = new DynamicParameters(new
            //{
            //    offerProductId = offerProductSize.OfferProductId,
            //    quantity = offerProductSize.Quantity,
            //    sizeName = offerProductSize.Size?.Name,
            //    sizeTypeName = offerProductSize.Size?.SizeType?.Name,
            //    createdBy
            //});

            //parameters.Add("offerProductSizeId", direction: ParameterDirection.ReturnValue);

            //conn.Execute(spName, parameters, transaction, commandType: CommandType.StoredProcedure);

            //offerProductSize.Id = parameters.Get<int>("offerProductSizeId");

            //todo size

        }

        public void DeleteOffer(int offerId)
        {
            const string spName = "dbo.Offer_Delete";

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName, new { offerId }, commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }
    }
}

