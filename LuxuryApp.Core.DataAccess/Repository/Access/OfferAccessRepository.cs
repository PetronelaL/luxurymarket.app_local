﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.DataAccess;
using LuxuryApp.Core.DataAccess.Models;
using System.Linq.Expressions;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class OfferAccessRepository :
        RepositoryBase,
        IUserAccessRepository
    {
        public OfferAccessRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public bool CanUserAddOffers(int companyId, int userId)
        {
            var sql = $"SELECT {SqlFunctionName.fn_CanUserAddOffers}(@userId, @companyId)";
            bool result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<bool>(sql, new { userId, companyId });
                conn.Close();
            }
            return result;
        }
    }
}
