﻿using Dapper;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.Home;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.DataAccess;
using LuxuryApp.Processors.Readers;
using System;
using System.Collections.Generic;
using System.Data;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class HomeRepository : RepositoryBase, IHomeRepository
    {
        public HomeRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public List<CustomerServiceTopicsModel> GetServiceTopics(int topicId)
        {
            var parameters = new DynamicParameters();
            if (topicId > 0)
            {
                parameters.Add("TopicID", topicId);
            }

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                try
                {
                    var reader = conn.ExecuteReader(StoredProcedureNames.CustomerServiceTopicsGet,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure);
                    var dataReader = new DataReader { Connection = conn, Reader = reader };

                    if (dataReader == null) return null;

                    return dataReader.ToCustomerServiceTopicList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public int InsertCustomerServiceRequest(CustomerServiceRequestModel model, int userId, DateTimeOffset date)
        {
            var parameters = new DynamicParameters(new
            {
                StatusID = CustomerServiceRequestStatuses.New,
                ContactName = model.ContactName,
                CompanyName = model.CompanyName,
                ContactEmail = model.ContactEmail,
                ContactPhoneNumber = model.ContactPhoneNumber,
                TopicID = model.TopicID,
                QuestionText = model.QuestionText,
                CurrentDate = date
            });
            if (userId > 0)
            {
                parameters.Add("CurrentUser", userId);
            }
            parameters.Add("ID", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                try
                {
                    var reader = conn.ExecuteReader(StoredProcedureNames.CustomerServiceRequest_Insert,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
                return parameters.Get<int>("ID");
            }
        }
    }
}