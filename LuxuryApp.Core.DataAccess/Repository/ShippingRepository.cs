﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.Shipping;
using LuxuryApp.Contracts.Repository;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class ShippingRepository: RepositoryBase,
        IShippingRepository
    {
        public ShippingRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public ShippingTax[] GeShippingTaxesByIds(int[] shippingTaxIds)
        {
            const string sql =
                "select " +
                "Id," +
                "Name," +
                "DisplayName," +
                "Days," +
                "ApparelTax," +
                "NonApparelTax" +
                "  from dbo.fn_GetShippingTaxesByIds(@ids)";
            ShippingTax[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<ShippingTax>(sql,
                    new {ids = shippingTaxIds.ToIdList()})
                    .ToArray();
                conn.Close();
            }
            return result;
        }
    }
}
