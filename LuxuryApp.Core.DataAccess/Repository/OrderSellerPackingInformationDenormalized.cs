﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class OrderSellerPackingInformationDenormalized
    {
        public int Id { get; set; }

        public int OrderSellerId { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactEmail { get; set; }

        public DateTimeOffset StartPickupDate { get; set; }

        public DateTimeOffset EndPickupDate { get; set; }

        public string Notes { get; set; }

        public TimeSpan FromPickupHour { get; set; }

        public TimeSpan ToPickupHour { get; set; }

        public int OrderSellerPackingInformationID { get; set; }

        public double BoxSizeLength { get; set; }

        public double BoxSizeWidth { get; set; }

        public double BoxSizeDepth { get; set; }

        public int Quantity { get; set; }

        public double Weight { get; set; }
    }
}
