﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.DataAccess;
using LuxuryApp.Core.DataAccess.DataReaderExtensions;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class OfferImportRepository : RepositoryBase, IOfferImportRepository
    {
        public OfferImportRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public async Task<int> InsertBatchItems(string fileName, DataTable products, DataTable productSizes, int companyId, int regionId, int userId)
        {
            var createdDate = DateTimeOffset.Now;

            var parameters = new DynamicParameters(
              new
              {
                  fileName = fileName,
                  StatusID = OfferStatus.InProgress,
                  CompanyID = companyId,
                  SizeRegionID = regionId,
                  OfferProducts = products,
                  OfferProductSizes = productSizes,
                  CreatedDate = createdDate,
                  CreatedBy = userId
              });
            parameters.Add("offerImportBatchId", direction: ParameterDirection.ReturnValue);

            var offerImportBatchId = 0;

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                await conn.ExecuteAsync(StoredProcedureNames.OfferImport_Insert,
                                                 param: parameters,
                                                 commandType: CommandType.StoredProcedure);
                offerImportBatchId = parameters.Get<int>("offerImportBatchId");

            }

            return offerImportBatchId;
        }

        public async Task<int> UpdateOfferImportGeneralData(OfferImport item, int userId)
        {
            var returnStatus = -1;

            var parameters = new DynamicParameters(
           new
           {
               OfferImportBatchID = item.Id,
               StartDate = item.StartDate,
               EndDate = item.EndDate,
               StatusID = item.OfferStatus,
               TakeAll = item.TakeAll,
               Active = item.Active,
               Deleted = item.Deleted,
               ModifiedBy = userId,
               ModifiedDate = DateTimeOffset.Now
           });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OfferImportBatch_Update,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }

            return returnStatus;
        }

        public async Task<IEnumerable<OfferImportBachItem>> GetOfferImportProductItems(int? offerImportBatchId, int? offerImportBatchItemId)
        {
            var sql =
               @"select OfferImportBatchItemId,
                   OfferImportBatchID,
                   BrandName,
                   ModelNumber,
                   ColorCode ,
                   MaterialCode,
                   SKU,
                   SizeTypeName,
                   OfferCost,
                   OfferCostValueConverted,
                   OfferTotalPrice,
                   BrandID,
                   ColorID,
                   ProductID,
                   MaterialID,
                   SizeTypeID,
                   CurrencyID as Currency,
                   OfferCostValue,
                   ProductRetailPrice,
                   CustomerDiscount,
                   SellerDiscount,
                   CustomerUnitPrice,
                   CustomerTotalPrice,
                   ActualTotalUnits,
                   Active,
                   LineBuy,
                   MainImageName,
                   ProductName,
                   CategoryName,
                   OriginName,
                   SeasonCode,
                   Dimensions";
            sql = $"{sql} from  {SqlFunctionName.fn_OfferImportBatchItems_GetItems}(@OfferImportBatchID,@OfferImportBatchItemID)";
            IEnumerable<OfferImportBachItem> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OfferImportBachItem>(sql, new { offerImportBatchId, offerImportBatchItemId });
                conn.Close();
            }
            return result;
        }
        public async Task<OfferImportBachItem> GetOfferProductFullData(int offerImportBatchItemId)
        {
            OfferImportBachItem returnItem = null;

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.OfferImportBatchItems_GetItemFullData,
                                                new { offerImportBatchItemId },
                                                 commandType: CommandType.StoredProcedure);
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                returnItem = dataReader.ToOfferImportItemFullData().FirstOrDefault();

                conn.Close();
            }
            return returnItem;
        }

        public async Task UpdateOfferImportItem(OfferImportBachItemEditModel item, int userId, DateTimeOffset datetime, bool mapSizes)
        {
            var parameters = new DynamicParameters(
               new
               {
                   OfferImportBatchItemID = item.Id,
                   BrandID = item.BrandId,
                   ModelNumber = item.ModelNumber,
                   ColorID = item.ColorId,
                   MaterialID = item.MaterialId,
                   Active = item.Active,
                   BusinessID = item.BusinessId,
                   OfferCost = item.OfferCost,
                   CurrencyID = item.Currency > 0 ? (int?)item.Currency : null,
                   LineBuy = item.LineBuy,
                   ModifiedBy = userId,
                   ModifiedDate = datetime,
                   MapSizes = mapSizes
               });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.OfferImportBatchItems_Update,
                                                     param: parameters,
                                                     commandType: CommandType.StoredProcedure);
            }
        }

        public void BatchUpdateOfferImportItems(OfferItemsBatchUpdateModel model, int userId, DateTimeOffset datetime, out string errorMessage)
        {
            errorMessage = String.Empty;

            var parameters = new DynamicParameters(
              new
              {
                  Active = model.Active,
                  OfferImportBatchItemIDs = ListToDataTable(model.OfferImportBatchItemIds)
              });

            using (var conn = GetReadWriteConnection())
            {
                try
                {
                    conn.Open();
                    var reader = conn.Execute(StoredProcedureNames.OfferImportBatchItems_BatchUpdate, parameters, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    errorMessage = string.IsNullOrWhiteSpace(ex.Message) ? "DBError" : ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public async Task<int> AddUpdateOfferImportItemSize(OfferImportBachItemSizeModel item, int userId)
        {
            var totalQuantity = 0;

            var p = new DynamicParameters();
            p.Add("TotalQuantity", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("OfferImportBatchItemID", item.OfferImportBatchItemId);
            p.Add("OfferImportBatchSizeItemID", item.OfferImportBatchSizeItemId);
            p.Add("Quantity", item.Quantity);
            p.Add("ModifiedBy", userId);
            p.Add("ModifiedDate", DateTimeOffset.Now);
            p.Add("Deleted", item.Deleted);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                await conn.ExecuteAsync(StoredProcedureNames.OfferImportBatchSizeItems_Update,
                                                   param: p,
                                                   commandType: CommandType.StoredProcedure
                                                   );
                totalQuantity = p.Get<int>("TotalQuantity");
                conn.Close();
            }

            return totalQuantity;
        }

        public async Task<IEnumerable<OfferImportBatchItemSize>> GetOfferImportItemSizes(int offerImportBatchItemId)
        {
            IEnumerable<OfferImportBatchItemSize> returnItems = null;
            var sql = @"SELECT OfferImportBatchSizeItemID
		                      ,OfferImportBatchItemID
		                      ,Size
		                      ,SizeTypeSizeID
		                      ,SellerSizeMappingID
		                      ,Quantity
		                      ,QuantityValue";
            sql = $"{sql} from  {SqlFunctionName.fn_OfferImportBatchItemGetSizes}(@OfferImportBatchItemID)";
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                returnItems = await conn.QueryAsync<OfferImportBatchItemSize>(sql, new { offerImportBatchItemId });
                conn.Close();
            }
            return returnItems;
        }

        public async Task<int> UpdateOfferItemsLineBuy(OfferImportBatchEditLineBuy item, int userId)
        {
            var returnStatus = -1;
            var ids = item.Ids == null || item.Ids.Length == 0 ? string.Empty : string.Join(",", item.Ids);
            var parameters = new DynamicParameters(
           new
           {
               OfferImportBatchID = item.OfferImportBatchId,
               ModifiedBy = userId,
               ModifiedDate = DateTimeOffset.Now,
               LineBuy = item.LineBuy,
               Ids = ids
           });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OfferImportBatchUpdateLineBuy,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }

            return returnStatus;
        }

        public async Task<IEnumerable<OfferImportBatchItemError>> Validate(int? offerImportBatchId, int? offerImportBatchItemId, bool onlyActive)
        {
            var sql =
               @"select
                   OfferImportBatchItemID,
                   ColumnName,
                   ColumnValue,
                   ErrorType,
                   OfferImportBatchSizeItemID";
            sql = $"{sql} from  {SqlFunctionName.fn_OfferImportBatchValidate}(@OfferImportBatchID,@OfferImportBatchItemID,@OnlyActive)";
            IEnumerable<OfferImportBatchItemError> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OfferImportBatchItemError>(sql, new { offerImportBatchId, offerImportBatchItemId, onlyActive });
                conn.Close();
            }
            result = result.ToList();
            return result;
        }

        public async Task<IEnumerable<OfferImportFile>> GetFiles(int offerImportId)
        {
            IEnumerable<OfferImportFile> returnItems = null;
            var p = new DynamicParameters();
            p.Add("OfferImportBatchID", offerImportId);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.OfferImportFiles_GetByBatchID,
                                                    p,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                if (dataReader != null)
                    returnItems = dataReader.ToOfferImportFileList();

                conn.Close();
            }

            return returnItems;
        }

        public async Task<int> UpdateFile(OfferImportFile file, int userId)
        {
            var returnStatus = -1;

            var parameters = new DynamicParameters(
           new
           {
               OfferImportFileID = file.OfferImportFileId,
               OriginalName = file.OriginalName,
               Deleted = file.Deleted,
               ModifiedBy = userId,
               ModifiedDate = DateTimeOffset.Now
           });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OfferImportFiles_Update,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }

            return returnStatus;
        }

        public int InsertFiles(OfferImportFile file, int userId)
        {
            var returnStatus = -1;
            var p = new DynamicParameters();
            p.Add("OfferImportBatchID", file.OfferImportBatchId);
            p.Add("OriginalName", file.OriginalName);
            p.Add("FileName", file.FileName);
            p.Add("CreatedBy", userId);
            p.Add("CreatedDate", DateTimeOffset.Now);
            p.Add("ID", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = conn.Execute(StoredProcedureNames.OfferImportFiles_Insert,
                                                    param: p,
                                                    commandType: CommandType.StoredProcedure
                                                    );

                returnStatus = p.Get<int>("ID");
                conn.Close();
            }
            return returnStatus;
        }

        public async Task<IEnumerable<string>> GetModelNumberByBrand(ProductSearchMappingCriteria item)
        {
            var returnItems = new List<string>();

            var parameters = new DynamicParameters(
            new
            {
                BrandID = item.BrandId,
                ColorID = item.ColorId,
                MaterialID = item.MaterialId,
                BusinessID = item.BusinessId,
                SizeTypeID = item.SizeTypeId
            });

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.Products_GetModelNumberByBrandID,
                                                param: parameters,
                                                 commandType: CommandType.StoredProcedure);
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                if (dataReader != null)
                    returnItems = dataReader.ToModelNumberList();

                conn.Close();
            }
            return returnItems;
        }

        public async Task<int> InsertOfferImportError(OfferImportReportError model, int userId)
        {
            var returnStatus = -1;

            var parameters = new DynamicParameters(
            new
            {
                OfferImportBatchID = model.OfferImportBatchId,
                OfferImportBatchItemID = model.OfferImportBatchItemId,
                BrandID = model.BrandId,
                ProductID = model.ProductId,
                ErrorDescription = model.ErrorDescription,
                CreatedBy = userId,
                CreatedDate = DateTimeOffset.Now
            });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OfferImportReportedErrors_Insert,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }
            return returnStatus;
        }

        public async Task<OfferImportSummary> GetOfferImportSummary(int offerImportBatchId)
        {
            var sql = @"SELECT OfferImportBatchID as Id,
                               CompanyID,
                               CompanyFee,
                               StartDate,
                               EndDate,
                               TakeAll,
                               StatusID,
                               StatusName,
                               TotalUnits as TotalNumberOfUnits,
                               TotalProducts,
                               TotalCost as TotalCostOffer,
                               SellerTotalPrice,
                               CurrencyID";
            sql = $"{sql} from  {SqlFunctionName.fn_OfferImport_GetSummary}(@OfferImportBatchID)";

            IEnumerable<OfferImportSummary> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OfferImportSummary>(sql, new { offerImportBatchId });
                conn.Close();
            }
            return result.FirstOrDefault();
        }

        private DataTable ListToDataTable(List<int> ids)
        {
            DataTable idsDataTable = null;
            if (ids != null)
            {
                idsDataTable = new DataTable();
                idsDataTable.Columns.Add("ID", typeof(int));
                foreach (var prodId in ids)
                {
                    idsDataTable.Rows.Add(prodId);
                }
            }

            return idsDataTable;
        }
    }
}

