﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.DataReaderExtensions
{
    public static partial class DataReaderExtensions
    {
        public static List<EntityDescription> ToEntityDescriptions(this DataReader reader)
        {
            var list = new List<EntityDescription>();

            while (reader.Read())
            {
                list.Add(new EntityDescription
                {
                    Id = reader.GetInt("ID"),
                    EntityId = reader.GetInt("EntityID"),
                    EntityType = (EntityType)reader.GetInt("EntityTypeID"),
                    Description = reader.GetString("Description"),
                    Notes = reader.GetString("Notes"),
                    Dimensions = reader.GetString("Dimensions")
                });
            }

            return list;
        }

    }
}
