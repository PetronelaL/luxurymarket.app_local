﻿using LuxuryApp.Contracts.Models.Home;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;

namespace LuxuryApp.Processors.Readers
{
    public static class CustomerRequestTopicReader
    {
        public static List<CustomerServiceTopicsModel> ToCustomerServiceTopicList(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<CustomerServiceTopicsModel>();

            while (reader.Read())
            {
                var item = new CustomerServiceTopicsModel();
                item.TopicID = reader.GetInt("TopicID");
                item.Name = reader.GetString("Name");

                items.Add(item);
            }

            reader.Close();

            return items;
        }
    }
}
