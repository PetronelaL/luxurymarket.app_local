﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Resources.Errors;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LuxuryApp.Core.DataAccess.DataReaderExtensions
{
    public static partial class DataReaderExtensions
    {
        public static List<OfferImportBatchItemError> ToOfferImportBatchItemErrorList(this DataReader reader)
        {
            var list = new List<OfferImportBatchItemError>();

            while (reader.Read())
            {
                var error = new OfferImportBatchItemError
                {
                    OfferImportBatchItemId = reader.GetInt("OfferImportBatchItemID"),
                    ColumnName = reader.GetString("ColumnName") ?? string.Empty,
                    ColumnValue = reader.GetString("ColumnValue") ?? string.Empty,
                    ErrorType = reader.GetString("ErrorType") ?? string.Empty,
                    OfferImportBatchSizeItemID = reader.GetIntNullable("OfferImportBatchSizeItemID"),
                };
                list.Add(error);
            }

            list.ForEach(x => x.Message = string.Format(x.ErrorType.ProcessErrorMessage() ?? string.Empty,x.ColumnName, x.ColumnValue));
            return list;
        }

        public static List<OfferImportBachItem> ToOfferImportBachItemList(this DataReader reader)
        {
            var offerImportList = new List<OfferImportBachItem>();

            while (reader.Read())
            {
                var offerImport = new OfferImportBachItem
                {
                    OfferImportBatchItemId = reader.GetInt("OfferImportBatchItemID"),
                    OfferImportBatchId = reader.GetInt("OfferImportBatchID"),
                    BrandName = reader.GetString("Brand"),
                    BusinessName = reader.GetString("Business"),
                    ModelNumber = reader.GetString("ModelNumber"),
                    ColorCode = reader.GetString("ColorCode"),
                    MaterialCode = reader.GetString("MaterialCode"),
                    Sku = reader.GetString("SKU"),
                    SizeTypeName = reader.GetString("SizeTypeName"),
                    OfferCost = reader.GetString("OfferCost"),
                    OfferCostValueConverted = reader.GetDoubleNullable("OfferCostValueConverted"),
                    OfferTotalPrice = reader.GetDoubleNullable("OfferTotalPrice"),

                    BrandId = reader.GetIntNullable("BrandID"),
                    //BusinessId = reader.GetIntNullable("BusinessID"),
                    ColorId = reader.GetIntNullable("ColorID"),
                    ProductId = reader.GetIntNullable("ProductID"),
                    MaterialId = reader.GetIntNullable("MaterialID"),
                    SizeTypeId = reader.GetIntNullable("SizeTypeID"),

                    Currency = (CurrencyType)(reader.GetIntNullable("CurrencyID") ?? 0),
                    OfferCostValue = reader.GetDoubleNullable("OfferCostValue"),
                    ProductRetailPrice = reader.GetDoubleNullable("ProductRetailPrice"),
                    CustomerDiscount = reader.GetDoubleNullable("CustomerDiscount"),
                    SellerDiscount = reader.GetDoubleNullable("SellerDiscount"),
                    CustomerUnitPrice = reader.GetDoubleNullable("CustomerUnitPrice"),
                    CustomerTotalPrice = reader.GetDoubleNullable("CustomerTotalPrice"),
                    ActualTotalUnits = reader.GetIntNullable("ActualTotalUnits"),
                    Active = reader.GetBool("Active"),
                    LineBuy = reader.GetBool("LineBuy"),
                    //product
                    MainImageName = reader.GetString("MainImageName"),
                    ProductName = reader.GetString("ProductName"),
                    CategoryName = reader.GetString("CategoryName"),
                    OriginName = reader.GetString("CategoryName"),
                    SeasonCode = reader.GetString("CategoryName"),
                    Dimensions = reader.GetString("Dimensions")
                };
                offerImportList.Add(offerImport);
            }

            return offerImportList;
        }

        public static List<OfferImportBatchItemSize> ToOfferImportItemSizeList(this DataReader reader)
        {
            var sizes = new List<OfferImportBatchItemSize>();

            while (reader.Read())
            {
                var sizeItem = new OfferImportBatchItemSize
                {
                    OfferImportBatchItemId = reader.GetInt("OfferImportBatchItemID"),
                    OfferImportBatchSizeItemId = reader.GetInt("OfferImportBatchSizeItemID"),
                    Quantity = reader.GetString("Quantity"),
                    QuantityValue = reader.GetIntNullable("QuantityValue"),
                    SizeName = reader.GetString("Size"),
                    SizeTypeSizeId = reader.GetIntNullable("SizeTypeSizeID"),
                    SellerSizeMappingId = reader.GetIntNullable("SellerSizeMappingID")
                };
                sizes.Add(sizeItem);
            }

            return sizes;
        }

        public static List<OfferImportFile> ToOfferImportFileList(this DataReader reader)
        {
            var list = new List<OfferImportFile>();

            while (reader.Read())
            {
                list.Add(new OfferImportFile
                {
                    OfferImportFileId = reader.GetInt("OfferImportFileID"),
                    OfferImportBatchId = reader.GetInt("OfferImportBatchID"),
                    FileName = reader.GetString("FileName"),
                    OriginalName = reader.GetString("OriginalName")
                });
            }

            return list;
        }

        public static List<OfferImportBachItem> ToOfferImportList(this DataReader reader, ref int offerImportBatchId)
        {
            offerImportBatchId = 0;
            while (reader.Read())
            {
                offerImportBatchId = reader.GetInt("OfferImportBatchID");
            }

            var offerImportList = (reader.NextResult()) ? reader.ToOfferImportBachItemList() : new List<OfferImportBachItem>();

            var errorList = (reader.NextResult()) ? reader.ToOfferImportBatchItemErrorList() : new List<OfferImportBatchItemError>();

            offerImportList.ForEach(x => x.Errors = errorList.Where(y => y.OfferImportBatchItemId == x.OfferImportBatchItemId).ToList());

            return offerImportList;
        }

        public static List<OfferImportBachItem> ToOfferImportItemFullData(this DataReader reader)
        {
            var offerImportList = reader.ToOfferImportBachItemList();

            var listDescription = (reader.NextResult()) ? reader.ToEntityDescriptions() : new List<EntityDescription>();

            var listImages = (reader.NextResult()) ? reader.ToEntityImages() : new List<EntityImage>();

            var sizes = (reader.NextResult()) ? reader.ToOfferImportItemSizeList() : new List<OfferImportBatchItemSize>();

            var errors = (reader.NextResult()) ? reader.ToOfferImportBatchItemErrorList() : new List<OfferImportBatchItemError>();

            offerImportList.ForEach(x => x.Descriptions = listDescription.Where(y => x.ProductId != null && y.EntityId == x.ProductId).ToList());
            offerImportList.ForEach(x => x.Images = listImages.Where(y => x.ProductId != null && y.EntityId == x.ProductId).ToList());
            offerImportList.ForEach(x => x.Sizes = sizes.Where(y => y.OfferImportBatchItemId == x.OfferImportBatchItemId).ToList());
            offerImportList.ForEach(x => x.Errors = errors.Where(y => y.OfferImportBatchItemId == x.OfferImportBatchItemId).ToList());

            return offerImportList;
        }

        public static List<string> ToModelNumberList(this DataReader reader)
        {
            var list = new List<string>();
            while (reader.Read())
            {
                list.Add(reader.GetString("ModelNumber"));
            }
            return list;
        }

    }
}
