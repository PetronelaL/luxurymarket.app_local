﻿
namespace LuxuryApp.DataAccess
{
    public static class StoredProcedureNames
    {
        public static string RegisterRequest_Insert = "App.RegistrationRequest_Insert";

        public static string Products_GetByID = "app.usp_ProductGetByID";
        public static string Products_GetBySKU = "App.usp_ProductGetBySku";
        public static string Products_Search = "APP.usp_ProductSearch";

        public static string Countries_Search = "Countries_GetByID";
        public static string States_Search = "States_Search";
        public static string CurrencyGetByID = "usp_CurrencyGetByID";
        public static string CurrencyGetByName = "usp_CurrencyGetByName";

        public static string BrandsGetByID = "usp_BrandsGetByID";
        public static string ColorsSearch = "usp_ColorsSearch";
        public static string BrandsSearch = "usp_BrandsSearch";
        public static string MaterialSearch = "usp_MaterialSearch";


        public static string HierarchiesGet = "Hierachy_Get";
        public static string Products_GetModelNumberByBrandID = "usp_Products_GetModelNumberByBrandID";

        #region OfferImport

        public static string OfferImport_Insert = "usp_OfferImport_Insert";
        public static string OfferImportBatch_Update = "usp_OfferImportBatch_Update";
        public static string OfferImportBatchItems_GetItemFullData = "usp_OfferImportBatchItems_GetItemFullData";
        public static string OfferImportBatchItems_Update = "usp_OfferImportBatchItems_Update";
        public static string OfferImportBatchItems_BatchUpdate = "usp_OfferImportItem_BatchUpdate";
        public static string OfferImportBatchSizeItems_Update = "usp_OfferImportBatchSizeItems_Update";

         public static string OfferImportBatchUpdateLineBuy = "usp_OfferImportBatchUpdateLineBuy";
        public static string OfferImportBatchItem_Validation = "usp_OfferImportBatchItem_Validation";

        public static string OfferImportReportedErrors_Insert = "usp_OfferImportReportedErrors_Insert";
        public static string OfferImportBatch_GetByAppUserID = "usp_OfferImportBatch_GetByAppUserID";

        public static string OrderFiles_Update = "usp_OrderFiles_Update";
        public static string OrderFiles_Insert = "usp_OrderFiles_Insert";

        #endregion

        #region OfferImportFiles

        public static string OfferImportFiles_GetByBatchID = "usp_OfferImportFiles_GetByBatchID";
        public static string OfferImportFiles_Insert = "usp_OfferImportFiles_Insert";
        public static string OfferImportFiles_Update = "usp_OfferImportFiles_Update";

        #endregion

        #region CurrentUser

        public static string CustomerGetByUserID = "usp_CustomerGetByUserID";
        public static string UpdateACcountInfo = "usp_Account_SaveInfo";

        #endregion

        #region Order

        public static string OrderSubmit = "usp_OrderSubmit";
        public static string OrderStatusUpdate = "usp_OrderStatusUpdate";
        public static string OrderSellerConfirmUnits = "usp_OrderSellerConfirmUnits";
        public static string Order_GetSuborders = "usp_Order_GetSuborders";
        #endregion

        public static string FtpProductImagesInsert = "usp_FtpProductImagesInsert";
        public static string FtpProductImagesSchedulesInsert = "usp_FtpProductImagesSchedulesInsert";
        public static string VendorProductImagesInsert = "usp_VendorProductImagesInsert";

        #region order seller packages

        public static string OrderSellerPackagesInsert = "usp_OrderSellerPackagesInsert";
        public static string OrderSellerUpdatePickupDates = "usp_OrderSellerUpdatePickupDates";
        public static string OrderSellerPackingInformationInsert = "usp_OrderSellerPackingInformation_Insert";
        public static string IsSubmittedPackageInformation = "usp_OrderSellerPackingInformation_IsSubmitted";

        #endregion

        #region Customer Service

        public static string CustomerServiceTopicsGet = "usp_CustomerService_GetTopics";
        public static string CustomerServiceRequest_Insert = "usp_CustomerService_Insert";

        #endregion

        #region Company

        public static string AcceptTermsAndConditonsForCompany = "usp_Companies_AcceptTermsAndConditions";

        #endregion

        #region SizeTypes

        public static string usp_SizeTypeGetMappings = "usp_SizeTypeGetMappings";

        #endregion

        public static string Inventory_GetSellerAPISettings = "Inventory_GetSellerAPISettings";
        public static string usp_ProductSyncBatchGetLastSync = "usp_ProductSyncBatchGetLastSync";

    }
}
