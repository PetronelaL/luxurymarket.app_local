﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;

namespace LuxuryApp.Cdn.WebApi
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

         }
    }
}