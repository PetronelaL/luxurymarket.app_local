﻿using LuxuryApp.Cdn.Image.Contracts;
using LuxuryApp.Cdn.Image.Contracts.Model;
using LuxuryApp.Cdn.WebApi.Helpers;
using LuxuryApp.Core.Infrastructure.Providers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LuxuryApp.Cdn.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/imagecdn")]
    public class ImageCdnController : ApiController
    {
        private IImageCdnService _service;

        public ImageCdnController(IImageCdnService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("uploadsingle")]
        public IHttpActionResult UploadSingle(CdnImageUploadParameter uploadParameter)
        {
            return Ok(_service.Upload(uploadParameter));
        }

        [HttpPost]
        [Route("getcontenturl")]
        public IHttpActionResult GetContentUrl(CdnImageUrlQueryParameter queryParameter)
        {
            return Ok(_service.GetContentUrl(queryParameter));
        }

        [HttpPost]
        [Route("removesingle")]
        public IHttpActionResult RemoveSingle(CdnImageUrlQueryParameter queryParameter)
        {
            return Ok(_service.Remove(queryParameter));
        }

        [HttpPost]
        [Route("uploadbatch")]
        public IHttpActionResult UploadBatch([FromBody]IEnumerable<CdnImageUploadParameter> uploadParameters)
        {
            return Ok(_service.Upload(uploadParameters));
        }

        [HttpPost]
        [Route("getcontenturls")]
        public IHttpActionResult GetContentUrlsBatch([FromBody]IEnumerable<CdnImageUrlQueryParameter> queryParameters)
        {
            return Ok(_service.GetContentUrls(queryParameters));
        }

        [HttpPost]
        [Route("removebatch")]
        public IHttpActionResult RemoveBatch([FromBody]IEnumerable<CdnImageUrlQueryParameter> queryParameters)
        {
            return Ok(_service.Remove(queryParameters));
        }

        //test
        [HttpPost]
        [Route("uploadsingleImage")]
        public async Task<IHttpActionResult> UploadImage()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Request!");
                throw new HttpResponseException(response);
            }

            var streamProvider = new ImportProvider(Configs.CdnImageFolder);
            await Request.Content.ReadAsMultipartAsync(streamProvider);

            var data = streamProvider.FileData.FirstOrDefault();
            var name = Path.GetFileNameWithoutExtension(data.LocalFileName);
            var param = new CdnImageUploadParameter
            {
                Key = Path.GetFileName(data.LocalFileName),
                Content = File.ReadAllBytes(data.LocalFileName),
                ImageSizes = CdnImageSizeList.ImageSizes
            };

            return Ok(_service.Upload(param));

        }

    }
}