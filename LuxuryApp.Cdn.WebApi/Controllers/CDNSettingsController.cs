﻿using LuxuryApp.Cdn.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace LuxuryApp.Cdn.WebApi.Controllers
{
    /// <summary>
    /// Controller for getting the CDN settings
    /// </summary>
    [Authorize]
    [RoutePrefix("api/cdnSettings")]
    public class CDNSettingsController : ApiController
    {
        private readonly IEnumerable<ICdnSettings> _allSettings;

        public CDNSettingsController(IEnumerable<ICdnSettings> allSettings)
        {
            _allSettings = allSettings;
        }

        // GET: Test
        [HttpGet]
        public IHttpActionResult Index()
        {
            return Ok(new
            {
                Message = _allSettings.Any() ? "Test succeeded" : "Test failed",
                CurrentConfigurations = _allSettings.Select(x =>
                   new
                   {
                       x.ConfigurationId,
                       Type = x.GetType(),
                   })
            });
        }
    }
}