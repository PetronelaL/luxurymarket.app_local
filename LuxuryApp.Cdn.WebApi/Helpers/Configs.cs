﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace LuxuryApp.Cdn.WebApi.Helpers
{
    public static class Configs
    {
        public static string CdnImageFolder
        {
            get
            {
                var path = ConfigurationManager.AppSettings["CdnUploadFolder"];
                TryCreateDirectory(path);
                return path;
            }
        }

        private static void TryCreateDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception ex)
            {

            }
        }

    }
}