﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Interfaces;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public abstract class RepositoryBase
    {
        private IConnectionStringProvider _connectionStringProvider;

        protected RepositoryBase(IConnectionStringProvider connectionStringProvider)
        {
            _connectionStringProvider = connectionStringProvider;
        }

        protected IDbConnection GetReadOnlyConnection()
        {
            return new SqlConnection(_connectionStringProvider.ReadOnlyConnectionString);
        }

        protected IDbConnection GetReadWriteConnection()
        {
            return new SqlConnection(_connectionStringProvider.ReadWriteConnectionString);
        }
    }
}
