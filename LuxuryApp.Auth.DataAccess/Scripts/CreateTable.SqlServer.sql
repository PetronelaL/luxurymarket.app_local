BEGIN TRAN
CREATE TABLE [app].[Users]
(
   	[Id] [int] (1,1) NOT NULL,
    [UserName]             NVARCHAR (50)            NOT NULL,
    [Email]                NVARCHAR (100)          NULL,
    [EmailConfirmed]       BIT           NOT NULL,
    [PasswordHash]         NVARCHAR (100)         NULL,
    [SecurityStamp]        NVARCHAR (100)         NULL,
    [PhoneNumber]          NVARCHAR (25)         NULL,
    [PhoneNumberConfirmed] BIT           NOT NULL,
    [TwoFactorEnabled]     BIT          NOT NULL,
    [LockoutEndDateUtc]     DATETIMEOFFSET(7)                NULL,
    [LockoutEnabled]       BIT          NOT NULL,
    [AccessFailedCount]    INT                    NOT NULL,
	[CreatedDate]  DATETIMEOFFSET(7)  NOT NULL,
	[ApprovedBy]  INT NULL,
	[ApprovedDate]  DATETIMEOFFSET(7)  NULL,
	[ModifiedDate]  DATETIMEOFFSET(7)  NULL,
	[ModifiedBy] [int] NULL,
	[Active] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
    CONSTRAINT [PK_app.AppUsers_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UK_app.AppUsers_UserName] UNIQUE NONCLUSTERED ([UserName] ASC),
	CONSTRAINT [UK_app.AppUsers_Email] UNIQUE NONCLUSTERED ([Email] ASC)
);

CREATE TABLE [app].[Roles]
(
    [Id]  NVARCHAR(56)          NOT NULL,
    [Name]   NVARCHAR (50)  NOT NULL,

    CONSTRAINT [PK_app.Roles_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UK_app.Rolse_Name] UNIQUE NONCLUSTERED ([Name] ASC)
);

CREATE TABLE [app].[UserRoles]
(
	[Id]  NVARCHAR(56)          NOT NULL,
    [UserId] NVARCHAR(56) NOT NULL,
    [RoleId] NVARCHAR(56) NOT NULL,
	CONSTRAINT [PK_app.UserRoles_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_app.UserRoles_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_app.UserRoles_UserRole] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([Id]) ON DELETE CASCADE
);

CREATE TABLE [app].[UserClaims]
(
	[Id]  NVARCHAR(56)          NOT NULL,
    [UserID]     NVARCHAR(56)                   NOT NULL,
    [ClaimType]  NVARCHAR (MAX)        NULL,
    [ClaimValue] NVARCHAR (MAX)        NULL,

    CONSTRAINT [PK_app.UserClaims_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_app.UserClaims_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE
);

CREATE TABLE [app].[UserLogins]
(
	[Id]  NVARCHAR(56)          NOT NULL,
    [UserID]        NVARCHAR(56)           NOT NULL,
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
	CONSTRAINT [PK_app.UserLogins_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_app.UserLogins_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE
);
CREATE TABLE [app].[RefreshTokens](
	[Id] [nvarchar](128) NOT NULL,
	[Subject] [nvarchar](50) NOT NULL,
	[IssuedUtc]  DATETIMEOFFSET(7)  NOT NULL,
	[ExpiresUtc]  DATETIMEOFFSET(7)  NOT NULL,
	[ProtectedTicket] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_app.RefreshTokens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

IF @@ERROR>0
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
