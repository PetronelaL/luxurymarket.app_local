﻿using Dapper;
using LuxuryApp.Auth.Core.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuxuryApp.Auth.DataAccess
{
    public class UserStore<TUser, TRole> : UserStore<TUser, TRole, int, string>, IUserStore<TUser, int>
    where TUser : User<int>
    where TRole : Role
    {
        public UserStore(DapperIdentityDbContext<TUser, TRole> dbContext)
            : base(dbContext)
        {

        }
        public override async Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var result = await DbContent.UserClaimRepository.FindAsync(c => c.UserId == user.Id);
            IList<System.Security.Claims.Claim> rs = new List<System.Security.Claims.Claim>();
            if (result != null && result.Any())
            {
                foreach (var login in result)
                {
                    rs.Add(new System.Security.Claims.Claim(login.ClaimType, login.ClaimValue));
                }
            }

            return rs;
        }

        public override async Task RemoveClaimAsync(TUser user, System.Security.Claims.Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }
            await DbContent.UserClaimRepository.RemoveAsync(c => c.UserId == user.Id
                && c.ClaimValue == claim.Value
                && c.ClaimType == claim.ValueType);
        }
    }

    public class UserStore<TUser, TRole, TKey, TRoleKey> :
        IUserClaimStore<TUser, TKey>,
        IUserPasswordStore<TUser, TKey>,
        IUserSecurityStampStore<TUser, TKey>,
        IQueryableUserStore<TUser, TKey>,
        IUserEmailStore<TUser, TKey>,
        IUserPhoneNumberStore<TUser, TKey>,
        IUserLockoutStore<TUser, TKey>,
        IUserStore<TUser, TKey>
        where TUser : User<TKey> where TRole : Role<TRoleKey>
    {

        private IdentityDbContext<TUser, TRole, TKey, TRoleKey> _dbContent;
        public IdentityDbContext<TUser, TRole, TKey, TRoleKey> DbContent
        {
            get { return _dbContent; }
        }
        public UserStore(IdentityDbContext<TUser, TRole, TKey, TRoleKey> dbContent)
        {
            if (dbContent == null)
                throw new ArgumentNullException("dbContent is null");
            _dbContent = dbContent;
        }

        public virtual async Task CreateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            await DbContent.UserRepository.InsertAsync(user);
        }

        public virtual async Task DeleteAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            await DbContent.UserRepository.RemoveAsync(user);
        }

        public virtual async Task<TUser> FindByIdAsync(TKey userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId");
            }
            return await DbContent.UserRepository.GetAsync(userId);
        }

        public virtual async Task<TUser> FindByNameAsync(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("userName");
            }
            var users = await DbContent.UserRepository.FindAsync(c => c.UserName == userName);
            return users.FirstOrDefault();
        }

        public virtual async Task UpdateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual void Dispose()
        {
            //if (DbContent.UserRepository != null && DbContent.UserRepository is DapperRepository<TUser>)
            //{
            //    var rr = DbContent.UserRepository as DapperRepository<TUser>;
            //    rr.Dispose();
            //}
            DbContent.Dispose();
        }

        public virtual async Task AddClaimAsync(TUser user, System.Security.Claims.Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }
            var userClaim = new UserClaim<TKey>()
            {
                UserId = user.Id,
                ClaimType = claim.ValueType,
                ClaimValue = claim.Value
            };
            await DbContent.UserClaimRepository.InsertAsync(userClaim);
        }

        public virtual async Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var result = await DbContent.UserClaimRepository.FindAsync(c => (object)c.UserId == (object)user.Id);
            IList<System.Security.Claims.Claim> rs = new List<System.Security.Claims.Claim>();
            if (result != null && result.Any())
            {
                foreach (var login in result)
                {
                    rs.Add(new System.Security.Claims.Claim(login.ClaimType, login.ClaimValue));
                }
            }

            return rs;
        }

        public virtual async Task RemoveClaimAsync(TUser user, System.Security.Claims.Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }
            await DbContent.UserClaimRepository.RemoveAsync(c => (object)c.UserId == (object)user.Id && c.ClaimValue == claim.Value && c.ClaimType == claim.ValueType);
        }

        public virtual async Task<string> GetPasswordHashAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return await Task.FromResult(user.PasswordHash);
            //var u = await DbContent.UserRepository.GetAsync(user.Id);
            //if (u != null)
            //    return u.PasswordHash;
            //return string.Empty;
        }

        public virtual async Task<bool> HasPasswordAsync(TUser user)
        {
            var passwordHash = await GetPasswordHashAsync(user);
            return string.IsNullOrEmpty(passwordHash);
        }

        public virtual async Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.PasswordHash = passwordHash;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual async Task<string> GetSecurityStampAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.SecurityStamp);
        }

        public virtual async Task SetSecurityStampAsync(TUser user, string stamp)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.SecurityStamp = stamp;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual IQueryable<TUser> Users
        {
            get
            {
                var users = GetAllUsers();
                //task.Start();
                //task.Wait();
                //var users=task.Result;
                return users.AsQueryable<TUser>();
            }
        }
        private IEnumerable<TUser> GetAllUsers()
        {
            var users = DbContent.UserRepository.Find(c => c.UserName != " ");
            return users;
        }
        //private async Task<IEnumerable<TUser>> GetAllUsers()
        //{
        //    var users=await DbContent.UserRepository.FindAsync(c => c.UserName != " ");
        //    return users;
        //}

        public virtual async Task<TUser> FindByEmailAsync(string email)
        {
            var users = await DbContent.UserRepository.FindAsync(c => c.Email == email);
            if (users != null && users.Any())
                return users.First();
            return null;
        }

        public virtual async Task<string> GetEmailAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.Email);
        }

        public virtual async Task<bool> GetEmailConfirmedAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.EmailConfirmed);
        }

        public virtual async Task SetEmailAsync(TUser user, string email)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.Email = email;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual async Task SetEmailConfirmedAsync(TUser user, bool confirmed)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.EmailConfirmed = confirmed;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual async Task<string> GetPhoneNumberAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.PhoneNumber);
        }

        public virtual async Task<bool> GetPhoneNumberConfirmedAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.PhoneNumberConfirmed);
        }

        public virtual async Task SetPhoneNumberAsync(TUser user, string phoneNumber)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.PhoneNumber = phoneNumber;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual async Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.PhoneNumberConfirmed = confirmed;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual async Task<int> GetAccessFailedCountAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.AccessFailedCount);
        }

        public virtual async Task<bool> GetLockoutEnabledAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.LockoutEnabled);
        }

        public virtual async Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.LockoutEndDateUtc.HasValue
                    ? user.LockoutEndDateUtc.Value
                    : new DateTimeOffset());
        }

        public virtual async Task<int> IncrementAccessFailedCountAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.AccessFailedCount++;
            await DbContent.UserRepository.UpdateAsync(user);
            return user.AccessFailedCount;
        }

        public virtual async Task ResetAccessFailedCountAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.AccessFailedCount = 0;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual async Task SetLockoutEnabledAsync(TUser user, bool enabled)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.LockoutEnabled = enabled;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        public virtual async Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.LockoutEndDateUtc = lockoutEnd.UtcDateTime;
            await DbContent.UserRepository.UpdateAsync(user);
        }
    }
}
