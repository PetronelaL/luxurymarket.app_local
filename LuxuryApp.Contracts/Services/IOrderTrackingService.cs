﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace LuxuryApp.Contracts.Services
{
    public interface IOrderTrackingService
    {
        ApiResult<Byte[]> GetSellerOrderX2Document(int orderSellerId,out string poNumber);
        ApiResult PushSellerOrderX2Document(int orderSellerId);
    }
}
