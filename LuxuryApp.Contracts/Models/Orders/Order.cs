﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class Order
    {
        public int Id { get; set; }

        public string PONumber { get; set; }

        public int BuyerCompanyId { get; set; }

        public string BuyerAlias { get; set; }

        public string BuyerCompanyName { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public int PaymentMethodId { get; set; }

        public string PaymentMethodName { get; set; }

        public int TotalStyles
        {
            get { return OrderSellerList.SelectMany(x => x.ProductItems.Where(y => y.TotalUnits > 0).Select(y => y.ProductDetails?.ProductId)).Distinct().Count(); }
        }

        public int TotalUnits
        {
            get { return OrderSellerList.SelectMany(x => x.ProductItems).Sum(x => x.TotalUnits); }
        }

        public IEnumerable<OrderSellerBuyerView> OrderSellerList { get; set; }

        public int CurrencyId { get; set; }

        public string CurrencyName { get; set; }
    }
}
