﻿using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class SuborderDataForPDF
    {
        public int OrderId { get; set; }

        public int BuyerCompanyID { get; set; }

        public string BuyerCompanyName { get; set; }

        public int OrderSellerId { get; set; }

        public string OrderSellerPONumber { get; set; }

        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public string BrandName { get; set; }

        public string CategoryName { get; set; }

        public string ModelNumber { get; set; }

        public string ColorCode { get; set; }

        public int StandardColorID { get; set; }

        public string StandardColorName { get; set; }

        public string MaterialCode { get; set; }

        public string SKU { get; set; }

        public decimal ProductRetailPrice { get; set; }

        public int OrderSellerProductID { get; set; }

        public decimal OfferCost { get; set; }

        public decimal CustomerUnitPrice { get; set; }

        public string CurrencyName { get; set; }

        public decimal UnitDiscountPercentage { get; set; }

        public decimal UnitShippingTaxPercentage { get; set; }

        public decimal ShippingCost { get; set; }

        public int Units { get; set; }

        public int UnitsConfirmed { get; set; }

        public string SizeTypeName { get; set; }

        public string SizeName { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public List<Address> Addresses { get; set; }

        public decimal UnitShippingCost { get; set; }

        public decimal UnitLandedCost
        {
            get
            {
                return CustomerUnitPrice + UnitShippingCost;
            }
        }
    }
}
