﻿using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderBuyerManageList
    {
        public OrderBuyerManageList()
        {
            Items = new List<OrderBuyerManageView>();
        }
        public IEnumerable<OrderBuyerManageView> Items { get; set; }

    }

    public abstract class OrderManageDetails
    {
        public DateTimeOffset CreatedDate { get; set; }

        public string PONumber { get; set; }

        public int TotalStyles { get; set; }

        public int TotalUnits { get; set; }

        public double TotalCost { get; set; }
    }

    public class OrderBuyerManageView : OrderManageDetails
    {
        public int OrderId { get; set; }

        public IEnumerable<OrderBuyerSubOrderManageView> SubOrders { get; set; }
    }

    public class OrderBuyerSubOrderManageView
    {
        public int OrderSellerId { get; set; }

        public int SellerCompanyId { get; set; }

        public string SellerAlias { get; set; }

        public string OrderSellerPONumber { get; set; }

        public int StatusId { get; set; }

        public string StatusTypeName { get; set; }
    }
}
