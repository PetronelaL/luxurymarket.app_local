﻿using LuxuryApp.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferSearch
    {
        public int Page { get; set; }

        public int? PageSize { get; set; }

        public int? StatusID { get; set; }

        public string Keyword { get; set; }

        public int CompanyID { get; set; }

        public int SellerID { get; set; }

        public List<OfferStatusResponse> OfferStatuses { get; set; }
    }
}
