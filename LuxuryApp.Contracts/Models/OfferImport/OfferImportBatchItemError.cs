﻿
using LuxuryApp.Contracts.Enums;
using System.Collections;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchItemError
    {
        public int OfferImportBatchItemId { get; set; }

        public int? OfferImportBatchSizeItemID { get; set; }

        public string ColumnName { get; set; }

        public string ColumnValue { get; set; }

        public string ErrorType { get; set; }

        public string Message { get; set; }
    }

    public class OfferImportBatchItemErrorCollection
    {
        public OfferImportBatchItemErrorCollection()
        {
            Items = new List<OfferImportBatchItemError>();
        }
        public IEnumerable<OfferImportBatchItemError> Items { get; set; }
    }
}
