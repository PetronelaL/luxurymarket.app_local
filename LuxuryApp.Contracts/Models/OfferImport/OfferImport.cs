﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SizeMappings;
using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImport
    {
        public int Id { get; set; }

        public string FileName { get; set; }

        public OfferStatus? OfferStatus { get; set; }

        public int CompanyId { get; set; }

        public double CompanyFee { get; set; }

        public string OfferStatusName { get; set; }

        public bool? TakeAll { get; set; }

        public DateTimeOffset? StartDate { get; set; }

        public DateTimeOffset? EndDate { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public bool? Active { get; set; }

        public bool? Deleted { get; set; }

        public int TotalSuccessfulItems { get; set; }

        public int TotalErrorItems { get; set; }

        public int TotalUnits { get; set; }

        public int TotalProducts { get; set; }

        public double TotalCost { get; set; }

        public double SellerTotalPrice { get; set; }

        public SizeRegion SizeRegion { get; set; }

        public List<OfferImportFile> Files { get; set; }

        public IEnumerable<OfferImportBachItem> Items { get; set; }

    }

    public class OfferImportFile
    {
        public int OfferImportFileId { get; set; }

        public int OfferImportBatchId { get; set; }

        public string FileName { get; set; }

        public string OriginalName { get; set; }

        public bool Deleted { get; set; }
    }

    public class OfferImportFileCollection
    {
        public OfferImportFileCollection()
        {
            Items = new List<OfferImportFile>();
        }

        public IEnumerable<OfferImportFile> Items { get; set; }
    }
}
