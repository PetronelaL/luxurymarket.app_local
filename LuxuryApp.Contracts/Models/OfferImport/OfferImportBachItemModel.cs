﻿using LuxuryApp.Contracts.Enums;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBachItemEditModel
    {
        public int Id { get; set; }

        public int? BrandId { get; set; }

        public string ModelNumber { get; set; }

        public int? ProductId { get; set; }

        public int? CategoryId { get; set; }

        public int? BusinessId { get; set; }

        public CurrencyType Currency { get; set; }

        public int? ColorId { get; set; }

        public int? MaterialId { get; set; }

        public int? SizeTypeId { get; set; }

        public double? OfferCost { get; set; }

        public bool? LineBuy { get; set; }

        public bool? Active { get; set; }

        public List<OfferImportBachItemSizeModel> Sizes { get; set; }
    }

    public class OfferImportBachItemSizeModel
    {
        public OfferImportBachItemSizeModel()
        {
            Deleted = false;
        }
        public int OfferImportBatchSizeItemId { get; set; }

        public int OfferImportBatchItemId { get; set; }

        public int Quantity { get; set; }

        public bool Deleted { get; set; }
    }

    public class OfferItemsBatchUpdateModel
    {
        public int OfferImportBatchId { get; set; }

        public List<int> OfferImportBatchItemIds { get; set; }

        public bool Active { get; set; }
    }
}
