﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class ProductFilterModel
    {
        public int CompanyId { get; set; }
        public int[] BusinessIds { get; set; }
        public int[] DivisionIds { get; set; }
        public int[] DepartmentIds { get; set; }
        public int[] CategoriesIds { get; set; }
        public ProductFilterSort Sort { get; set; }

        private int _pageNumber;
        public int PageNumber {
            get { return _pageNumber > 0 ? _pageNumber : 1; } 
            set { _pageNumber = value; }
        }
        public int PageSize { get; set; }

        public ProductFilterModel()
        {
            BusinessIds = new int[0];
            DivisionIds = new int[0];
            DepartmentIds = new int[0];
            CategoriesIds = new int[0];
        }
    }
}
