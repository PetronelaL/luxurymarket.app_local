﻿using System.ComponentModel.DataAnnotations;

namespace LuxuryApp.Contracts.Models.Addresses
{
    public class AddressInfo
    {
        public int AddressId { get; set; }

        public int AddressTypeId { get; set; }

        public string AddressTypeName { get; set; }

        [Required]
        public string Street1 { get; set; }

        public string Street2 { get; set; }

        [Required]
        public string City { get; set; }

        public int StateId { get; set; }

        public string StateName { get; set; }

        [Required]
        public string ZipCode { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int CountryId { get; set; }

        public string CountryName { get; set; }

        public bool IsMain { get; set; }

        public int CompanyId { get; set; }
        public string CountryIso2Code { get; set; }
    }
}
