﻿using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Addresses
{
    public class AddressListing
    {
        public List<AddressInfo> BillingAddress { get; set; }

        public List<AddressInfo> ShippingAddress { get; set; }

        public List<AddressInfo> CompanyAddress { get; set; }
    }
}
