﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync.Coltorti
{
    public class ColtortiAuthenticationResponse
    {
        public string access_token { get; set; }

        public int expires_in { get; set; }
    }
}
