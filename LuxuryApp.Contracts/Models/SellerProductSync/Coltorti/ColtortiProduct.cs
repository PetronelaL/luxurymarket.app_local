﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync.Coltorti
{
    public class ColtortiProduct
    {
        public string product_id { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public double price { get; set; }

        public Dictionary<string, string> brand { get; set; }

        public Dictionary<string, string> season { get; set; }

        // business
        public Dictionary<string, string> family { get; set; }

        // division
        public Dictionary<string, string> line { get; set; }

        // department
        public Dictionary<string, string> ms5_subgroup { get; set; }

        // category
        public Dictionary<string, string> ms5_category { get; set; }

        public List<string> alternative_ids { get; set; }

        // size type
        public Dictionary<string, string> scalar_details { get; set; }

        // color
        public Dictionary<string, string> variant { get; set; }

        // material
        public object attributes { get; set; }

        public List<string> images { get; set; }

        public DateTime updated_at { get; set; }
    }

    public class ColtortiAttribute
    {
        public string description { get; set; }

        public Dictionary<string, string> values { get; set; }
    }
}
