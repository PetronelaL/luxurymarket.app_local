﻿
namespace LuxuryApp.Contracts.Models
{
    public class CurrentUserDataView
    {
        public User User { get; set; }

        public Customer Customer {get;set;}
}
}
