﻿using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class ItemCollection<T> where T : class
    {
        public ItemCollection()
        {
            Items = new List<T>();
        }
        public IEnumerable<T> Items { get; set; }
    }
}
