﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class SaveOfferProductSizesToCartModel
    {
        public int BuyerCompanyId { get; set; }
        public ItemQuantity[] ItemQuantities { get; set; }

        public SaveOfferProductSizesToCartModel(int buyerCompanyId, int offerProductSizeId, int quantity)
        {
            BuyerCompanyId = buyerCompanyId;
            ItemQuantities = new ItemQuantity[]
            {
                new ItemQuantity
                {
                    OfferProductSizeId = offerProductSizeId,
                    Quantity = quantity,
                }
            };
        }

        public class ItemQuantity
        {
            public int OfferProductSizeId { get; set; }
            public int Quantity { get; set; }
        }
    }

    public class AddEntireOfferToCartModel
    {
        public int BuyerCompanyId { get; set; }
        public int OfferId { get; set; }

        public AddEntireOfferToCartModel(int buyerCompanyId, int offerId)
        {
            BuyerCompanyId = buyerCompanyId;
            OfferId = offerId;
        }
    }

    public class AddProductsToCartModel
    {
        public int BuyerCompanyId { get; set; }
        public List<int> ProductId { get; set; }

        public AddProductsToCartModel(int buyerCompanyId, List<int> productId)
        {
            BuyerCompanyId = buyerCompanyId;
            ProductId = productId;
        }
    }

    public class AddAllProductToCartModel
    {
        public int BuyerCompanyId { get; set; }
        public ProductSearchItems Search { get; set; }

        public AddAllProductToCartModel(int buyerCompanyId, ProductSearchItems search)
        {
            BuyerCompanyId = buyerCompanyId;
            Search = search;
        }
    }
}
