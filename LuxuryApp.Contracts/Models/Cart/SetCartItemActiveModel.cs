﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class SetCartItemActiveModel
    {
        public CartItemSellerUnitIdentifier CartItemSellerUnitIdentifier { get; set; }
        public bool ActiveFlag { get; set; }
    }
}
