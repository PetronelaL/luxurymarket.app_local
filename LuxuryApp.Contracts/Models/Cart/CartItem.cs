using System.Configuration;
using System.Linq;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartItem
    {
        public int CartId { get; set; }
        public int OfferProductId { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public int BrandId { get; set; }
        public string BrandName { get; set; }

        public string CategoryName { get; set; }

        public string ProductSku { get; set; }

        public string ProductMainImageName { get; set; }

        public string ProductMainImageLink
        {
            get
            {
                return ConfigurationManager.AppSettings["S3BucketLink50"] + ProductMainImageName;
            }
        }

        public int MaterialId { get; set; }
        public string MaterialCode { get; set; }

        public int ColorId { get; set; }
        public string ColorName { get; set; }

        public int StandardColorId { get; set; }
        public string StandardColorName { get; set; }

        public int StandardMaterialId { get; set; }
        public string StandardMaterialName { get; set; }

        public int SeasonId { get; set; }
        public string SeasonName { get; set; }

        public decimal AverageUnitCost { get; set; }

        public decimal AverageUnitDiscountPercentage { get; set; }

        public decimal TotalCustomerCost { get; set; }

        public decimal TotalShippingCost { get; set; }

        public CartItemSellerUnit[] SellerUnits { get; set; }

        public decimal TotalLandedCost { get; set; }

        public CartItem()
        {
            SellerUnits = new CartItemSellerUnit[0];
        }
    }
}