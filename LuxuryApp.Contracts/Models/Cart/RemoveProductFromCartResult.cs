﻿namespace LuxuryApp.Contracts.Models.Cart
{
    public class RemoveProductFromCartResult
    {
        public CartSummary CartSummary { get; set; }

        public int[] RemovedProductIds { get; set; }

        public RemoveProductFromCartResult()
        {
            RemovedProductIds = new int[0];
        }
    }
}