﻿using LuxuryApp.Contracts.Enums;
using System.Collections.Generic;
using System.Data;

namespace LuxuryApp.Contracts.Models
{
    public class ProductSearchItems
    {
        public int Page { get; set; }

        public int? BusinessID { get; set; }

        public int? DivisionID { get; set; }

        public int? DepartmentID { get; set; }

        public List<int> CategoryIDs { get; set; }

        public List<int> BrandIDs { get; set; }

        public List<int> ColorIDs { get; set; }

        public string Keyword { get; set; }

        public int? PageSize { get; set; }

        public SearchSortType Sort { get; set; }

        public int? FeaturedEventID { get; set; }

        public int? SellerCompanyID { get; set; }

        public int? OfferID { get; set; }

        public int CompanyID { get; set; }
    }

    public enum SearchSortType
    {
        Newest = 0,
        PriceLowHigh = 1,
        PriceHighLow = 2,
        DiscountLowHigh = 3,
        DiscountHighLow = 4
    }
}