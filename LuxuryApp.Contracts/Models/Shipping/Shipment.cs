﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Configuration;

namespace LuxuryApp.Contracts.Models.Shipment
{
    public class Shipping
    {
        public int CartId { get; set; }

        public Shipment[] Shipments { get; set; }

        public int ShipmentsCount => Shipments.Length;
        public decimal TotalShippingCost { get; set; }
        public Currency Currency { get; set; }
    }

    public class Shipment
    {
        public int CartId { get; set; }
        public int ShippingRegionId { get; set; }

        public string ShipsFromRegion { get; set; }
        public string ShipingIterval { get; set; }

        public int TotalStylesCount { get; set; }

        public decimal ApparelCost { get; set; }
        public decimal NonApparelCost { get; set; }
        public decimal TotalShippingCost { get; set; }
        
        public Currency Currency { get; set; }

        public ShipmentItem[] ShipmentItems { get; set; }

        public Shipment()
        {
            ShipmentItems = new ShipmentItem[0];
        }
    }

    public class ShipmentItem
    {
        public int CartId { get; set; }
        public int ShippingTaxId { get; set; }

        [JsonIgnore]
        public int ProductId { get; set; }

        public string ProductName { get; set; }
        public string BrandName { get; set; }

        public string ProductMainImageName { get; set; }

        public string ProductMainImageLink
        {
            get
            {
                return ConfigurationManager.AppSettings["S3BucketLink50"] + ProductMainImageName;
            }
        }

        public int TotalUnits { get; set; }
        public decimal TotalCost { get; set; }
        public decimal ShippingCost { get; set; }
        public decimal TotalLandedCost => TotalCost + ShippingCost;
        public Currency Currency { get; set; }

        public bool IsApparel { get; set; }

        public ShipmentItemBySeller[] ShipmentItemByBySellers { get; set; }

        public ShipmentItem()
        {
            ShipmentItemByBySellers = new ShipmentItemBySeller[0];
        }
    }

    public class ShipmentItemBySeller
    {
        public string SellerAlias { get; set; }
        public int UnitsCount { get; set; }
        public decimal UnitOfferCost { get; set; }
        public decimal TotalOfferCost { get; set; }
        public Currency Currency { get; set; }
    }
}



