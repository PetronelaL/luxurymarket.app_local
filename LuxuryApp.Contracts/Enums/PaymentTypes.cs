﻿namespace LuxuryApp.Contracts.Enums
{
    public enum PaymentTypes
    {
        PayAtTerm = 1,

        Prepayment = 2
    }
}
