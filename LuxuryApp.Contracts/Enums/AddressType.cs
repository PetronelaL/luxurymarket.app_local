﻿
namespace LuxuryApp.Contracts.Enums
{
    public enum AddressType
    {
        None = 0,

        Billing = 1,

        Shipping = 2,

        Company = 3
    }
}
