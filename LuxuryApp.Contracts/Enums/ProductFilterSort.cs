﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Enums
{
    public enum ProductFilterSort
    {
        Newest = 0,
        PriceLowHigh = 1,
        PriceHighLow = 2
    }
}
