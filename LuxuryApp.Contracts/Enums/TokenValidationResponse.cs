﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Enums
{
    public enum TokenValidationResponseType
    {
        Valid = 1,

        InvalidOrExpiredToken = 2,

        InvalidUser = 3
    }
}
