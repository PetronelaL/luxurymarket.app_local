﻿using LuxuryApp.Contracts.Models.Home;
using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Repository
{
    public interface IHomeRepository
    {
        List<CustomerServiceTopicsModel> GetServiceTopics(int topicId);

        int InsertCustomerServiceRequest(CustomerServiceRequestModel model, int userId, DateTimeOffset date);
    }
}
