﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models.Shipping;

namespace LuxuryApp.Contracts.Repository
{
    public interface IShippingRepository
    {
        ShippingTax[] GeShippingTaxesByIds(int[] shippingTaxIds);
    }
}
