﻿using LuxuryApp.Contracts.Models.Ftp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface IVendorImageRepository
    {
        Task<IEnumerable<VendorImageItem>> GetUnprocessedImagesAsync();

        Task<int> UpdateProcessedImageAsync(VendorImageItem item, int createdBy);
    }
}