﻿using LuxuryApp.Contracts.Models.Cart;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface ICartRepository
    {
        void PhysicalDeleteCart(int cartId);
        int GetOrCreateCartId(int userId, int buyerCompanyId, int currencyId);
        Cart GetCart(int cartId);
        CartSummary GetCartSummary(int cartId);
        void UpdateCartTotalsByCartId(int cartId);
        CartItem[] GetCartItems(int cartId);

        void AddEntireOfferToCart(int cartId, int offerId);
        void AddEntireOffersToCart(int cartId, int[] offerIds);

        void AddEntireOfferProductToCart(int cartId, int offerProductId);
        void AddEntireOfferProductsToCart(int cartId, int[] offerProductIds);

        void AddOfferProductSizeToCart(int cartId, int offerProductSizeId, int quantity);
        void AddOfferProductSizesToCart(int cartId, SaveOfferProductSizesToCartModel.ItemQuantity[] itemQuantities);

        void AddProductsToCart(int cartId, int[] productIds);

        int GetCartItemSellerUnitSizeIdByOfferProductSizeId(int cartId, int offerProductSizeId);
        CartItemIdentifier[] GetCartItemIdentifiersByCartItemSellerUnitIdentifier(CartItemSellerUnitIdentifier cartItemSellerUnitIdentifier);
        CartItem[] GetCartItemByCartItemSellerUnitIdentifier(CartItemSellerUnitIdentifier cartItemSellerUnitIdentifier);

        void SetActiveFlagForEntireOffersUnderCart(int cartId, int[] offerIds, bool activeFlag);
        void SetActiveFlagForEntireOfferProductsUnderCart(int cartId, int[] offerProductIds, bool activeFlag);
        void SetActiveFlagForOfferProductSizesUnderCart(int cartId, int[] offerProductSizeIds, bool activeFlag);

        void RemoveCartItemsWithShippingTaxId(int cartId, int shippingRegionId);
        void RemoveCartItems(int cartId, int[] itemsIds);
        void EmptyCart(int cartId);

        CartTotalSummary GetTotalSummary(int cartId, bool onlyValidItems, int userId);
        bool IsUserAssociatedToCart(int cartId, int userId);

        OfferHasReservedItem HasReservedItemsByOtherUsers(int? offerId, int? offerProductId, int? cartId);
        OfferHasAvailableQuantity OfferProductSizeHasAvailableQuantity(int offerProductSizeId, int quantity, int? cartId);

        CartItemValidatelCollection GetInvalidItems(int cartId);
        List<CartItemUpdatedQuantity> RelockCartProduct(int cartId, int offerProductId);
        List<CartItemReserved> GetCartTimes(int cartId);
        CartSplitPreview GetCartSplitPreview(int cartId);
     }
}
