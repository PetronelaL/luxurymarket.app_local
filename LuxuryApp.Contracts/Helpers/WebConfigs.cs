﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Helpers
{
    public static class WebConfigs
    {
        public static int ProductsPageSize
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ProductsPageSize"]);
            }
        }

        public static int SellerProductsPageSize => int.Parse(ConfigurationManager.AppSettings["SellerProductsPageSize"]);

        public static string OfferImportFolder
        {
            get
            {
                var path = ConfigurationManager.AppSettings["OfferImportUploadFolder"];
                TryCreateDirectory(path);
                return path;
            }

        }

        public static string OfferDocumentsFolder
        {
            get
            {
                var path = ConfigurationManager.AppSettings["OfferDocumentsFolder"];
                TryCreateDirectory(path);
                return path;
            }
        }

        public static string OrderDocumentsFolder
        {
            get
            {
                var path = ConfigurationManager.AppSettings["OrderDocumentsFolder"];
                TryCreateDirectory(path);
                return path;
            }
        }

        public static Uri LuxuryMarketCDNUrl
        {
            get
            {
                return new Uri(ConfigurationManager.AppSettings["LuxuryMarketCDNUrl"]);
            }
        }

        public static string AuthUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["LuxuryMarketAuthUrl"];
            }
        }

        public static int BestSellersNumberOfItems
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["BestSellersNumberOfItems"]);
            }
        }

        public static string AWSBucketLink
        {
            get { return ConfigurationManager.AppSettings["AWSBucketLink"]; }
        }

        public static string LogoUrl
        {
            get
            {
                return AWSBucketLink + "logo.png";
            }
        }

        public static string S3BucketLinkExportCart
        {
            get { return ConfigurationManager.AppSettings["S3BucketLinkExportCart"]; }
        }

        public static string ExportCartLogoUrl
        {
            get
            {
                return S3BucketLinkExportCart + "export-cart-logo.png";
            }
        }

        public static string OrderGeneratedFolder
        {
            get
            {
                var path = ConfigurationManager.AppSettings["OrderGeneratedFolder"];
                TryCreateDirectory(path);
                return path;
            }

        }

        public static string FtpUploadFailedNotificationEmail
        {
            get { return ConfigurationManager.AppSettings["FtpUploadFailedNotificationEmail"]; }
        }

        #region UploadImages

        public static string FtpUploadImageFrom
        {
            get
            {
                var path = ConfigurationManager.AppSettings["FtpUploadImageFrom"];
                TryCreateDirectory(path);
                return path;
            }
        }

        public static string FtpUploadImageTo
        {
            get
            {
                var path = ConfigurationManager.AppSettings["FtpUploadImageTo"];
                TryCreateDirectory(path);
                return path;
            }
        }
        #endregion

        private static void TryCreateDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
