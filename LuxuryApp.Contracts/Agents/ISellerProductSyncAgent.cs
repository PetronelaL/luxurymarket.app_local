﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using System;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface ISellerProductSyncAgent
    {
        //  Task<IEnumerable<ProductSyncItem>> GetProducts(APISettings settings, Guid mainBatchID);

        Task<int> ProcessProducts(APISettings settings, Guid mainBatchID);
    }
}
