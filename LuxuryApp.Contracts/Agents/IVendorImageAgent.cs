﻿using System;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface IVendorImageAgent
    {
        Task<double> UploadImagesAsync();
    }
}
