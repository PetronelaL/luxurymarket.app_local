﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface ISellerApiSettingService
    {
        Task<APISettings> GetSellerApiSettings(SellerAPISettingType sellerAPIType);
    }
}
