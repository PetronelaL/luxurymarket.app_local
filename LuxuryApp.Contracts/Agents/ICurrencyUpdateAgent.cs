﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface ICurrencyUpdateAgent
    {
        void UpdateCurrencies();
    }
}
